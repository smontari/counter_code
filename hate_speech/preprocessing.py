import pandas as pd
import pickle
import glob
from html import unescape
import re, os
import numpy as np
import argparse
import torch
import numpy as np
import pandas as pd
import numpy as np
import pickle
import argparse
import os, codecs
#from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, f1_score
from sklearn.utils.class_weight import compute_class_weight
from sklearn.model_selection import train_test_split
import ipdb
torch.manual_seed(42)
import wordsegment as ws
ws.load() # load vocab for word segmentation

import preprocessing_utils


if __name__== '__main__':
    # load raw data, and create 6 files: dev-train-test, for both binary and multiclass setting.
    path = '/home/smontari/scratch/data/hate_speech/Data/'
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--dataset_name", type=str, choices=['davidson2017', 'founta2018', 'hateval', 'evalita', 'ami', 'fr_sex', 'ar_rel'], default = 'davidson2017',
                    help="Name of dataset to user")
    args = parser.parse_args()

    dataset_name = args.dataset_name
    print(dataset_name)

    if dataset_name == 'founta2018':
        dataset = pd.read_csv(path + 'raw_training_data' + '/founta2018.csv', index_col=None, names=['text', 'label', 'count_label_votes'], delimiter='\t')
        dataset = preprocessing_utils.clean_dataset(dataset)
        save_path = path + 'clean_training_data/' + dataset_name +'/multiclass'
        preprocessing_utils.split_and_save(dataset, save_path)
        # to binary: hateful (1) and non-hateful (0)
        dataset.label.replace({'hateful': 1, "abusive": 0, "normal": 0, "spam": 0}, inplace = True)
        save_path = path + 'clean_training_data/' + dataset_name  +'/binary'
        preprocessing_utils.split_and_save(dataset, save_path)


    elif dataset_name == 'davidson2017':
        dataset = pd.read_csv(path + 'raw_training_data' + '/davidson2017.csv', index_col=0)
        dataset.rename(columns={"class": "label", "tweet": "text"}, inplace=True, errors='ignore')
        dataset = preprocessing_utils.clean_dataset(dataset)
        # give multiclass labels string names for clarity
        dataset.label.replace({0: "hateful", 1: "offensive", 2: "neither"}, inplace = True)
        save_path = path + 'clean_training_data/' + dataset_name +'/multiclass'
        preprocessing_utils.split_and_save(dataset, save_path)
        # to binary: hateful (1) and non-hateful (0)
        dataset.label.replace({'hateful': 1, 'offensive': 0, 'neither': 0}, inplace = True)
        save_path = path + 'clean_training_data/' + dataset_name  +'/binary'
        preprocessing_utils.split_and_save(dataset, save_path)

    
    elif dataset_name == 'hateval':
        # divide by target: the first part is about immigrants, the second about women. Numbers taken from Nozza 2021 paper. Need to check manually.
        nb_immigrants_dict = {
            'hateval2019_en_dev.csv': 500,
            'hateval2019_en_test.csv': 1499,
            'hateval2019_en_train.csv': 4500,
            'hateval2019_es_dev.csv':  173,
            'hateval2019_es_test.csv': 800,
            'hateval2019_es_train.csv': 1618
        } # todo check that this is the right partition of data
        for file in nb_immigrants_dict:
            dataset = pd.read_csv(path + 'raw_training_data' + '/hateval2019/' + file, delimiter=',',encoding='utf-8')
            dataset.drop(['id'], axis=1, inplace=True)
            dataset.rename(columns={"HS": "label"}, inplace=True, errors='ignore')
            imm_df = dataset.head(nb_immigrants_dict[file])
            wom_df = dataset.tail(1-nb_immigrants_dict[file])
            for topic, df in zip(['immigrants', 'women'], [imm_df, wom_df]):
                df = preprocessing_utils.clean_dataset(df)
                save_path = path + 'clean_training_data/' + topic + '/' + file.split('_')[1] + '/'
                if not os.path.exists(save_path):
                    os.makedirs(save_path)
                print(file, len(df), df.groupby('label').id.count(), '\n')
                print(df.head(2))
                pickle.dump(df, open(save_path  + file.split('_')[-1][:-4] + '.pkl','wb'))
    
    elif dataset_name == 'evalita':
        path_evalita = path + 'raw_training_data' + '/Evalita-HaSpeeDe/TW-folder/'
        files = ['TW-reference/haspeede_TW-reference.tsv', 'TW-train/haspeede_TW-train.tsv']
        save_path = path + 'clean_training_data/immigrants/it/'
        if not os.path.exists(save_path):
                os.makedirs(save_path)
        for file in files:
            colnames = ['id','text','label']
            dataset = pd.read_csv(path_evalita + file, delimiter='\t',encoding='utf-8', names = colnames)
            dataset = preprocessing_utils.clean_dataset(dataset)
            if 'reference' in file:
                pickle.dump(dataset, open(save_path  + 'test.pkl','wb'))
            else:
                train, dev = train_test_split(dataset, test_size=0.25, random_state=42)
                for df, splitname in zip([train, dev],['train', 'dev']) :
                    pickle.dump(df, open(save_path  + splitname + '.pkl','wb'))

    elif dataset_name == 'ami':
        path_ami = path + 'raw_training_data' + '/AMI@EVALITA2018/'
        files = glob.glob(path_ami + '*it*.tsv')
        print(files)
        save_path = path + 'clean_training_data/women/it/'
        if not os.path.exists(save_path):
                os.makedirs(save_path)
        for file in files:
            dataset = pd.read_csv(file, delimiter='\t',encoding='utf-8')
            dataset.drop(['id'], axis=1, inplace=True)
            dataset.rename(columns={"misogynous": "label"}, inplace=True, errors='ignore')
            dataset = preprocessing_utils.clean_dataset(dataset)
            print(file, len(dataset), dataset.groupby('label').id.count(), '\n')
            print(dataset.head(2))
            if 'testing' in file:
                pickle.dump(dataset, open(save_path  + 'test.pkl','wb'))
            else:
                train, dev = train_test_split(dataset, test_size=0.25, random_state=42)
                for df, splitname in zip([train, dev],['train', 'dev']) :
                    pickle.dump(df, open(save_path  + splitname + '.pkl','wb'))

    elif dataset_name == 'fr_sex':
        path_repo = '/home/smontari/scratch/data/hate_speech/git_repositories/An-Annotated-Corpus-for-Sexism-Detection-in-French-Tweets/'
        annotations = pd.read_csv(path_repo + 'corpus_SexistContent.csv', header = None, sep = '\t')
        annotations_dict = dict(zip([str(a) for a in annotations[0]], annotations[1]))
        labels = []
        tweets = []
        with codecs.open(path_repo + 'hydrated_tweets.jsonl', 'r', 'utf-8', errors='ignore') as f:
            for jfile in preprocessing_utils.load_json_multiple(f):
                for tweet_dict in jfile['data']:
                    text = tweet_dict["text"]
                    id = tweet_dict["id"]
                    if id in annotations_dict:
                        label = annotations_dict[id]
                        labels.append(label)
                        tweets.append(text)

        save_path = path + 'clean_training_data/women/fr/'
        if not os.path.exists(save_path):
                os.makedirs(save_path)

        dataset = pd.DataFrame(list(zip(labels, tweets)), columns=['label','text'])
        dataset = preprocessing_utils.clean_dataset(dataset)
        preprocessing_utils.split_and_save(dataset, save_path)

    elif dataset_name == 'ar_rel':
        path_repo = '/home/smontari/scratch/data/hate_speech/git_repositories/Arabic_hatespeech/'
        save_path = path + 'clean_training_data/religion/ar/'
        if not os.path.exists(save_path):
                os.makedirs(save_path)

        files_dict={}
        files_dict['train'] = ['train.csv', 'hydrated_tweets_train.jsonl']
        files_dict['test'] = ['test.csv', 'hydrated_tweets_test.jsonl']
        for split in files_dict:
            afile, tfile = files_dict[split]
            annotations = pd.read_csv(path_repo + afile, sep = ',', encoding='utf-8', error_bad_lines=False)
            annotations_dict = dict(zip([str(a) for a in annotations['id']], annotations['hate']))
            labels = []
            tweets = []
            with codecs.open(path_repo + tfile, 'r', 'utf-8', errors='ignore') as f:
                for jfile in preprocessing_utils.load_json_multiple(f):
                    for tweet_dict in jfile['data']:
                        text = tweet_dict["text"]
                        id = tweet_dict["id"]
                        if id in annotations_dict:
                            label = annotations_dict[id]
                            labels.append(label)
                            tweets.append(text)
            dataset = pd.DataFrame(list(zip(labels, tweets)), columns=['label','text'])
            dataset = preprocessing_utils.clean_dataset(dataset)
            if split == 'test':
                pickle.dump(dataset, open(save_path  + 'test.pkl','wb'))
            else:
                train, dev = train_test_split(dataset, test_size=0.25, random_state=42)
                for df, splitname in zip([train, dev],['train', 'dev']) :
                    pickle.dump(df, open(save_path  + splitname + '.pkl','wb'))
