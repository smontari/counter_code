import pickle
from glob import glob
import pandas as pd
import ipdb

data_path = '/home/smontari/scratch/data/hate_speech/Data/'
dirs = 'clean_training_data/', 'comparable_clean_training_data/'
langs = ['it', 'en', 'es']
topics = ['immigrants', 'women']
for dir in dirs:
    for topic in topics:
        for lang in langs:
            splits = glob(data_path + dir + topic + '/' + lang + '/*.pkl')
            for s in splits:
                print(s)
                training_data = pd.read_pickle(s)
                newfile = s.replace('pkl', 'txt')
                data = training_data[['text','label']]
                data.to_csv(newfile, sep='\t', index=False, header = None)
