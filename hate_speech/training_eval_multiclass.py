
from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification, AutoModel
import torch.nn as nn
import torch
import torch.nn.functional as F
import numpy as np
import pandas as pd
import numpy as np
import pickle
import argparse
import os
#from sklearn.model_selection import train_test_split
from transformers import Trainer, TrainingArguments
from sklearn.metrics import classification_report, f1_score
import ipdb

class HateDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)

# 3 possibilities: 
# use already finetuned hate speech detection model
# finetune MLM for text classification on our corpus
# finetune hate speech detection model on our corpus

model_name = "bert-base-uncased"

# Load datasets
path = '/home/smontari/scratch/data/hate_speech/hatecheck-experiments/Data/'
parser = argparse.ArgumentParser()
parser.add_argument("-d", "--dataset_name", type=str, choices=['davidson2017', 'founta2018'], default = 'davidson2017',
                help="Name of dataset to user")
parser.add_argument("-t", "--do_train", type=bool, default = 'True',
                help="Wether to train the LM for classification")
args = parser.parse_args()
dataset_name = args.dataset_name
print(dataset_name)

data_path = path + 'clean_training_data/' + dataset_name 
training_data = pd.read_pickle(data_path + '/training_data_multiclass.pkl')
save_path = '/home/smontari/scratch/data/hate_speech/hatecheck-experiments/Models/' + dataset_name + '/'
if not os.path.exists(save_path):
    os.makedirs(save_path)


# write to dict
dataset = training_data.copy() #.sample(n=10000, random_state=123)
# for multiclass: convert string label names into integer IDs

if dataset_name == 'founta2018':
    dataset.label.replace({"hateful": 0, "abusive": 1, "normal": 2, "spam": 3}, inplace = True)

    num_labels = 4
elif dataset_name == 'davidson2017':
    dataset.label.replace({"hateful": 0, "offensive": 1, "neither": 2}, inplace = True)

    num_labels = 3

print(dataset.groupby('label').text.count())

df_train, df_valtest, df_val, df_test = {}, {}, {}, {}

# import tokenizer
tokenizer = AutoTokenizer.from_pretrained(model_name)

# add special tokens for URLs, emojis and mentions (--> see pre-processing)
special_tokens_dict = {'additional_special_tokens': ['[USER]','[URL]']}
num_added_toks = tokenizer.add_special_tokens(special_tokens_dict)

texts = dataset.text.astype("string").tolist()
labels = dataset.label.tolist()
encodings = tokenizer(texts, truncation=True, padding=True)
hate_dataset = HateDataset(encodings, labels)
# random split
train_split = int(len(texts) * 0.8)
test_split = int(len(texts) * 0.1)
valid_split = len(texts) - train_split - test_split
train, valid, test = torch.utils.data.random_split(hate_dataset, [train_split, valid_split, test_split])

# check CUDA availability
print(torch.cuda.is_available())
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Define training arguments
training_args = TrainingArguments(
    save_steps = 2500,
    output_dir=save_path + '/BERT_multiclass/Checkpoints', # output directory
    num_train_epochs=3,              # total number of training epochs
    per_device_train_batch_size=16,  # batch size per device during training
    per_device_eval_batch_size=64,   # batch size for evaluation
    evaluation_strategy = 'epoch',
    warmup_steps=500,                # number of warmup steps for learning rate scheduler
    weight_decay=0.01,               # strength of weight decay
    learning_rate = 5e-5,
    seed = 123
)

def model_init():
    model = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=num_labels)
    # resize to match tokenizer length with special tokens added above
    model.resize_token_embeddings(len(tokenizer))
    return model

# Instantiate trainer objects for each dataset
trainer = Trainer(
    args=training_args,                  
    train_dataset=train,         
    eval_dataset=valid,            
    model_init = model_init
)

trainer.train()

trainer.save_model(save_path + 'BERT_multiclass/Final')
tokenizer.save_pretrained(save_path + 'BERT_multiclass/Final')

# load fine-tuned models
model = AutoModelForSequenceClassification.from_pretrained(save_path + 'BERT_multiclass/Final')

trainer = Trainer(
    model=model,         
    args=TrainingArguments(
        output_dir=save_path + 'BERT_multiclass/Test',
        per_device_eval_batch_size = 64)
)

print('Evaluating')
results = trainer.predict(test)
for metric in results.metrics:
    print(metric, results.metrics['{}'.format(metric)])
print()

# write predictions to series
preds=[]
for row in results[0]:
    preds.append(int(np.argmax(row)))

pred_labels = pd.Series(preds)
test_labels = [x['labels'] for x in test]

# print classification reports for each model
print(classification_report(test_labels,pred_labels))

# f1 scores
for average in ['micro', 'macro', 'weighted']:
    print('{} F1 score: {:.2%}'.format(average, f1_score(test_labels,pred_labels, average=average)))
print(pred_labels.value_counts())
