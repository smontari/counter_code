import pandas as pd
import pickle
import glob
from html import unescape
import re, os
import numpy as np
import argparse
from scipy.sparse import data
import torch
import numpy as np
import pandas as pd
import numpy as np
import pickle
import argparse
import os, codecs
#from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, f1_score
from sklearn.utils.class_weight import compute_class_weight
from sklearn.model_selection import train_test_split
import ipdb
torch.manual_seed(42)
import wordsegment as ws
ws.load() # load vocab for word segmentation

import preprocessing_utils


if __name__== '__main__':
    # load raw data, and create 6 files: dev-train-test, for both binary and multiclass setting.
    path = '/home/smontari/scratch/data/hate_speech/Data/'
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--dataset_name", type=str, choices=['hateval', 'evalita', 'ami'], default = 'davidson2017',
                    help="Name of dataset to user")
    args = parser.parse_args()

    dataset_name = args.dataset_name
    print(dataset_name)

    split_size = {
        'train': 1618,
        'dev': 173,
        'test': 800,
    }

    
    if dataset_name == 'hateval':
        # divide by target: the first part is about immigrants, the second about women. Numbers taken from Nozza 2021 paper. Need to check manually.
        nb_immigrants_dict = {
            'hateval2019_en_dev.csv': 500,
            'hateval2019_en_test.csv': 1499,
            'hateval2019_en_train.csv': 4500,
            'hateval2019_es_dev.csv':  173,
            'hateval2019_es_test.csv': 800,
            'hateval2019_es_train.csv': 1618
        } # todo check that this is the right partition of data
        for file in nb_immigrants_dict:
            dataset = pd.read_csv(path + 'raw_training_data' + '/hateval2019/' + file, delimiter=',',encoding='utf-8')
            print(file, len(dataset))

            dataset.drop(['id'], axis=1, inplace=True)
            dataset.rename(columns={"HS": "label"}, inplace=True, errors='ignore')
            imm_df = dataset.head(nb_immigrants_dict[file])
            wom_df = dataset.tail(1-nb_immigrants_dict[file])
            print(len(imm_df), len(wom_df))
            for topic, df in zip(['immigrants', 'women'], [imm_df, wom_df]):
                df = preprocessing_utils.clean_dataset(df)
                save_path = path + 'comparable_clean_training_data/' + topic + '/' + file.split('_')[1] + '/'
                if not os.path.exists(save_path):
                    os.makedirs(save_path)
                split = file.split('_')[-1][:-4]
                split_size[split]
                print("doing sampling")
                dataset = preprocessing_utils.do_split(dataset, split_size[split])
                print(split, len(dataset))
                if split == "test":
                    test, blind = train_test_split(dataset, test_size=0.5, random_state=42)
                    pickle.dump(test, open(save_path  + 'test' + '.pkl','wb'))
                    pickle.dump(blind, open(save_path  + 'blind' + '.pkl','wb'))
                else:
                    pickle.dump(dataset, open(save_path  + split + '.pkl','wb'))
    
    elif dataset_name == 'evalita':
        path_evalita = path + 'raw_training_data' + '/Evalita-HaSpeeDe/TW-folder/'
        files = ['TW-reference/haspeede_TW-reference.tsv', 'TW-train/haspeede_TW-train.tsv']
        save_path = path + 'comparable_clean_training_data/immigrants/it/'
        if not os.path.exists(save_path):
                os.makedirs(save_path)
        for file in files:
            colnames = ['id','text','label']
            dataset = pd.read_csv(path_evalita + file, delimiter='\t',encoding='utf-8', names = colnames)
            dataset = preprocessing_utils.clean_dataset(dataset)
            if 'train' in file:
                dataset = preprocessing_utils.do_split(dataset, split_size['train'])
                print('train', len(dataset))
                pickle.dump(dataset, open(save_path  + 'train.pkl','wb'))
            else:
                print(len(dataset))
                test, dev = train_test_split(dataset, test_size=0.20, random_state=42)
                print(len(test), len(dev))
                dev_sampled = preprocessing_utils.do_split(dev, split_size['dev'])
                test_sampled = preprocessing_utils.do_split(test, split_size['test'])
                test, blind = train_test_split(test_sampled, test_size=0.5, random_state=42)
                for df, splitname in zip([dev_sampled, test, blind],['dev', 'test', 'blind']) :
                    pickle.dump(df, open(save_path  + splitname + '.pkl','wb'))

    elif dataset_name == 'ami':
        path_ami = path + 'raw_training_data' + '/AMI@EVALITA2018/'
        files = glob.glob(path_ami + '*it*.tsv')
        print(files)
        save_path = path + 'comparable_clean_training_data/women/it/'
        if not os.path.exists(save_path):
                os.makedirs(save_path)
        for file in files:
            dataset = pd.read_csv(file, delimiter='\t',encoding='utf-8')
            dataset.drop(['id'], axis=1, inplace=True)
            dataset.rename(columns={"misogynous": "label"}, inplace=True, errors='ignore')
            dataset = preprocessing_utils.clean_dataset(dataset)

            if 'testing' in file:
                test, dev = train_test_split(dataset, test_size=0.20, random_state=42)
                dev_sampled = preprocessing_utils.do_split(dev, split_size['dev'])
                test_sampled = preprocessing_utils.do_split(test, split_size['test'])
                test, blind = train_test_split(test_sampled, test_size=0.5, random_state=42)
                for df, splitname in zip([dev_sampled, test, blind],['dev', 'test', 'blind']) :
                    pickle.dump(df, open(save_path  + splitname + '.pkl','wb'))
            else:
                dataset = preprocessing_utils.do_split(dataset, split_size['train'])
                pickle.dump(dataset, open(save_path  + 'train.pkl','wb'))
