# inspired from https://github.com/hate-alert/Tutorial-ICWSM-2021

from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification, AutoModel
import torch.nn as nn
import torch
import torch.nn.functional as F
import numpy as np
import pandas as pd
import numpy as np
import pickle
import glob
import argparse
import os
#from sklearn.model_selection import train_test_split
from transformers import Trainer, TrainingArguments
from sklearn.metrics import classification_report, f1_score
from sklearn.utils.class_weight import compute_class_weight
import ipdb
from ray import tune
torch.manual_seed(42)
from tqdm.auto import tqdm
import training_utils
import logging

os.environ["WANDB_DISABLED"] = "true"
os.environ["TOKENIZERS_PARALLELISM"] = "false"
logging.disable(logging.INFO) # disable INFO and DEBUG logging everywhere
# or 
#logging.disable(logging.WARNING) # disable WARNING, INFO and DEBUG logging everywhere

class WeightedTrainer(Trainer):
        def compute_loss(self, model, inputs, return_outputs=False):
            labels = inputs.pop("labels")
            outputs = model(**inputs)
            logits = outputs[0]
            weighted_loss = torch.nn.CrossEntropyLoss(weight=torch.FloatTensor(class_weights)).to(device)
            return (weighted_loss(logits, labels), outputs) if return_outputs else weighted_loss(logits, labels)



# Always start from bert-base-multilingual.
# 1) zeroshot, cross-lingual: training on one language and testing on unseen languages; 
# 2) monolingual: training and testing on the same language;
# 3) few-shot, cross-lingual: training on one language and a small percentage of samples from the test language
#and testing on the test language
# 4) augmented cross-lingual: training on several languages and testing on a language included in the training.

# training path:
# 1) tune hyperparameters
# 2) use several seeds for best hyperparameters. For each seed, get best epoch. 
# 3) average best epoch results of all seeds

stilt_dict = {}
stilt_dict['parsing'] = {
    'mono-it':'bert-base-italian-uncased-it_isdt',
    'mono-en':  'bert-base-uncased-ewt',
    'mono-es':  'RuPERTa-base_es-es_gsd',
    'multi-it':  'mbert-it_isdt',
    'multi-en':  'mbert-ewt',
    'multi-es':  'mbert-es_gsd',
    'multi-all':  'mbert-es_gsd+ewt+it_isdr'
}

# Load datasets
path = '/home/smontari/scratch/data/hate_speech/'
parser = argparse.ArgumentParser()
parser.add_argument("-t", "--do_train", action='store_true',
                    help="Wether to train the LM for classification")
parser.add_argument("--tune", action='store_true',
                help="Wether to tune de classifier hyperparameters")
parser.add_argument("-m", "--model",type=str, default='mono',
                    help="Which model to use")
parser.add_argument("-s", "--stilt",type=str, default='',
                    help="Which finetuning to use")
parser.add_argument("-c", "--comparable_data", action='store_true',
                    help="Wether to use to balanced dataset or not")
parser.add_argument("-e", "--epochs", type= int, default = 1,
                    help="Number of epochs to use")
parser.add_argument("-a", "--average",  action='store_true',
                    help="Wether to run multiple models and average them")

args = parser.parse_args()

if args.comparable_data: 
    data_path = path + 'Data/comparable_clean_training_data/'
else:
    data_path = path + 'Data/clean_training_data/'
models_path = path + 'Models/monolingual/'
if args.comparable_data: 
    models_path = models_path + 'comparable_data/'
else:
    models_path = models_path + 'full_data/'
models_path = models_path + args.model
if args.stilt:
    models_path = models_path  + '-' + args.stilt
print("saving models to ", models_path)

models_dict = {
    'en':"bert-base-uncased",
    'it':"dbmdz/bert-base-italian-uncased",
    'es':"mrm8488/RuPERTa-base",
    'multi':"bert-base-multilingual-uncased",
    'char':"helboukkouri/character-bert",
    'parsing': path + 'Models/machamp_models/transformers_logs/'
}


# check CUDA availability
print("GPU:", torch.cuda.is_available())
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

langs = ['it', 'en', 'es']
topics = ['immigrants', 'women']
datasets = {}

# create datasets dict
print("Preparing data...")
for topic in topics:
    datasets[topic]={}
    for lang in langs:
        print(topic, lang)
        datasets[topic][lang]={}
        splits = glob.glob(data_path + topic + '/' + lang + '/*.pkl')
        for s in splits:
            split_name = s.split('/')[-1][:-4]
            training_data = pd.read_pickle(s)
            texts = training_data.text.astype("string").tolist()
            labels = training_data.label.tolist()

            modelname = models_dict[lang]
            tokenizer = AutoTokenizer.from_pretrained(modelname)
            if args.model != 'char':
                # add special tokens for URLs and mentions (cf preprocess_df.py)
                special_tokens_dict = {'additional_special_tokens': ['[USER]','[URL]']}
                num_added_toks = tokenizer.add_special_tokens(special_tokens_dict)

            encodings = tokenizer(texts, truncation=True, padding=True)
            hate_dataset = training_utils.HateDataset(encodings, labels)
            datasets[topic][lang][split_name] = hate_dataset
        #print(datasets[topic][lang].keys())
        #print([len(datasets[topic][lang][s]) for s in datasets[topic][lang]])
        """
        immigrants it
        dict_keys(['dev', 'test', 'train'])
        [750, 1000, 2250]
        immigrants en
        dict_keys(['dev', 'test', 'train'])
        [500, 1499, 4500]
        immigrants es
        dict_keys(['dev', 'test', 'train'])
        [173, 800, 1618]
        women it
        dict_keys(['dev', 'test', 'train'])
        [1000, 1000, 3000]
        women en
        dict_keys(['dev', 'test', 'train'])
        [501, 1502, 4501]
        women es
        dict_keys(['dev', 'test', 'train'])
        [328, 801, 2883]
        COMPARABLE: (all identical)
        dict_keys(['dev', 'test', 'train', 'blind'])
        [173, 400, 1618, 400]
        """


if args.do_train:
    print("Training...")
    # training the model on each language
    for topic in topics:
        for lang in langs:
            print(topic + ' + ' + lang)
            save_path = models_path + topic + '/' + lang
            if not os.path.exists(save_path):
                os.makedirs(save_path)
            # when using stilt, provide as dataset name 'stilt' + task
            if args.stilt:
                modelname = models_dict[args.stilt] + stilt_dict[args.stilt][args.model+'-'+lang]
            else:
                modelname = models_dict[lang]


            split_dict = datasets[topic][lang]
            train_labels = [int(x['labels']) for x in split_dict['train']]
            # compute class weights based on training data label distribution
            class_weights = compute_class_weight('balanced', classes = np.unique(train_labels), y = train_labels)

            # Define training arguments
            training_args = TrainingArguments(
                save_steps = 2500,
                output_dir=save_path +'/Checkpoints', # output directory
                num_train_epochs=args.epochs,              # total number of training epochs
                per_device_train_batch_size=16,  # batch size per device during training
                per_device_eval_batch_size=64,   # batch size for evaluation
                evaluation_strategy = 'epoch',
                warmup_steps=500,                # number of warmup steps for learning rate scheduler
                weight_decay=0.01,               # strength of weight decay
                learning_rate = 5e-5,
                seed = 123
            )
        
            tokenizer = AutoTokenizer.from_pretrained(modelname)
            if args.model != 'char':
                # add special tokens for URLs and mentions (cf preprocess_df.py)
                special_tokens_dict = {'additional_special_tokens': ['[USER]','[URL]']}
                num_added_toks = tokenizer.add_special_tokens(special_tokens_dict)
            def model_init():
                model = AutoModelForSequenceClassification.from_pretrained(modelname)
                # resize to match tokenizer length with special tokens added above
                if args.model != 'char':
                    model.resize_token_embeddings(len(tokenizer))
                return model

            trainer = WeightedTrainer(                       
                    args=training_args,                  
                    train_dataset=split_dict['train'],         
                    eval_dataset=split_dict['dev'],            
                    model_init = model_init
                )
            # tune params
            if args.tune:
                best_run = trainer.hyperparameter_search(
                    backend = 'ray',
                    hp_space = training_utils.custom_hp_space,
                    direction = 'minimize',
                    n_trials = 1
                    )
                print(best_run)
                param_path = save_path + '/Final/'
                if not os.path.exists(param_path):
                    os.makedirs(param_path)
                pickle.dump(best_run.hyperparameters, open(param_path + 'best_parameters_dict.pkl', 'wb'))
                for n, v in best_run.hyperparameters.items():
                    setattr(trainer.args, n, v)
            else:
                # use the same hyperparameters as for models without parsing
                best_parameters_dict = pickle.load(open(path + 'Models/monolingual/comparable_data/' + topic + '/' + lang + '/Final/best_parameters_dict.pkl', 'rb'))
                for n, v in best_parameters_dict.items():
                    setattr(trainer.args, n, v)
            # do training
            if args.average:
                # do several runs avec average results
                for seed in range(5):
                    print("Run nb: ", seed)
                    setattr(trainer.args, 'eval_dataset', split_dict['test'])
                    setattr(trainer.args, 'seed', seed)
                    setattr(trainer.args,'load_best_model_at_end', True)
                    trainer.train()
                    trainer.save_model(save_path + '/Checkpoints/multiple_seeds/'+str(seed))

            else:
                setattr(trainer.args,'load_best_model_at_end', True)
                trainer.train()

            trainer.save_model(save_path + '/Final')
            tokenizer.save_pretrained(save_path + '/Final')
            print("saved model at ", save_path + '/Final')


# load fine-tuned models
print("Evaluation:")
final_path = path + 'Models/results/monolingual_f1_macro_' + args.model + '_' + args.stilt + '_' + str(args.epochs) + 'epochs'

if args.comparable_data:
    final_path = final_path + '_COMPARABLE'
if args.tune:
    final_path = final_path + '_TUNED'
if args.average:
    final_path = final_path + '_AVERAGED_TUNED'


if os.path.isdir(final_path + '.csv'):
    accuracy_table = pd.read_csv(final_path + '.csv')
    accuracy_table.loc[args.model + args.stilt] = [0]*len(accuracy_table.columns)
else:
    accuracy_table = pd.DataFrame(index=pd.Index([args.model + args.stilt], name='model'),columns=pd.Index([t + '-' + l for t in topics for l in langs], name='data'))

print(accuracy_table)
for topic in topics:
    for lang in langs:
        print(topic, lang)
        save_path = models_path + topic + '/' + lang
        test_data = datasets[topic][lang]['test']
        if args.average:
            f1_list = []
            for seed in range(5):
                model = AutoModelForSequenceClassification.from_pretrained(models_path + topic + '/' + lang + '/Checkpoints/multiple_seeds/'+str(seed))
                f1_list.append(training_utils.evaluate(model, test_data, save_path))
            f1 = np.mean(f1_list)
        else:
            model = AutoModelForSequenceClassification.from_pretrained(models_path + topic + '/' + lang + '/Final')
            f1 = training_utils.evaluate(model, test_data, save_path)

        accuracy_table.loc[args.model + args.stilt, topic + '-' + lang] = f1 = '{:0.04}'.format(f1)

print(accuracy_table)
accuracy_table.to_csv(final_path + '.csv')

