# activate machamp environment !!!

from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification, AutoModel

import os, sys
import glob
import torch
import tarfile
import json, ipdb
import numpy as np
from allennlp.models import load_archive
from allennlp.data.tokenizers import Tokenizer, PretrainedTransformerTokenizer
from allennlp.models import load_archive
from allennlp.modules.token_embedders import PretrainedTransformerEmbedder
from allennlp.common import Params
from allennlp.common.plugins import import_plugins

sys.path.insert(0, "/home/smontari/scratch/code/machamp")
sys.path.insert(0, "/home/smontari/scratch/code/")


from machamp.dataset_readers.machamp_universal_reader import MachampUniversalReader
from machamp.modules import pretrained_transformer_mixmatched_indexer
from machamp.models.machamp_model import MachampModel
from machamp.models.sentence_decoder import MachampClassifier
from machamp.models.tag_decoder import MachampTagger
from machamp.models.dependency_decoder import MachampBiaffineDependencyParser
from machamp.models.mlm_decoder import MachampMaskedLanguageModel
from machamp.models.multiseq_decoder import MachampMultiTagger
from machamp.models.crf_decoder import MachampCrfTagger
from machamp.models.seq2seq_decoder import MachampSeq2SeqDecoder

import_plugins()
path = '/home/smontari/scratch/data/hate_speech/Models'
machamp_path = path + '/machamp_models/logs/'

for model_path in glob.glob(machamp_path+'*/'):
    print(model_path)
    dir_path = glob.glob(model_path+'*/')[-1]

    # Load the archive from the serialization directory, which contains the trained model and the params.

    archive = load_archive(dir_path + "model.tar.gz")
    # Pull out just the PretrainedTransformerEmbedder part of the model.
    transformer_embedder: PretrainedTransformerEmbedder = archive.model._text_field_embedder.token_embedder_tokens._matched_embedder

    tokenizer: PretrainedTransformerTokenizer = Tokenizer.from_params(Params(archive.config.as_dict()["dataset_reader"]["tokenizer"]))


    transformer_dir = model_path.replace("logs", "transformers_logs")

    transformer_embedder.transformer_model.save_pretrained(transformer_dir)
    tokenizer.tokenizer.save_pretrained(transformer_dir)

    #model = AutoModelForSequenceClassification.from_pretrained(transformer_dir)

