# from https://github.com/hate-alert/Tutorial-ICWSM-2021

from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification, AutoModel
import torch.nn as nn
import torch
import torch.nn.functional as F
import numpy as np
import pandas as pd
import numpy as np
import pickle
import argparse
import os
#from sklearn.model_selection import train_test_split
from transformers import Trainer, TrainingArguments
from sklearn.metrics import classification_report, f1_score
from sklearn.utils.class_weight import compute_class_weight
import ipdb
from ray import tune
torch.manual_seed(42)
# todo
# this is an attempt to do either binary or multiclass in one file. Try both for both datasets.


class HateDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)

# 3 possibilities: 
# use already finetuned hate speech detection model
# finetune MLM for text classification on our corpus
# finetune hate speech detection model on our corpus

model_name = "bert-base-uncased"

# Load datasets
path = '/home/smontari/scratch/data/hate_speech/hatecheck-experiments/Data/'
parser = argparse.ArgumentParser()
parser.add_argument("-d", "--dataset_name", type=str, choices=['davidson2017', 'founta2018'], default = 'davidson2017',
                help="Name of dataset to user")
parser.add_argument("-ct", "--classif_type", type=str, choices=['binary', 'multi'], default = 'multi',
                help="Name of dataset to user")
parser.add_argument("-t", "--do_train", action='store_true',
                    help="Wether to train the LM for classification")
parser.add_argument("--tune", type=bool, default = False,
                help="Wether to tune de classifier hyperparameters")

args = parser.parse_args()
dataset_name = args.dataset_name
print(dataset_name)


data_path = path + 'clean_training_data/' + dataset_name 
save_path = '/home/smontari/scratch/data/hate_speech/hatecheck-experiments/Models/' + dataset_name
# check CUDA availability
print("GPU:", torch.cuda.is_available())
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

if args.classif_type == 'multi':
    save_path = save_path + '/BERT_multiclass/'
    training_data = pd.read_pickle(data_path + '/training_data_multiclass.pkl')
    dataset = training_data.copy()
    # for multiclass: convert string label names into integer IDs
    if dataset_name == 'founta2018':
        dataset.label.replace({"hateful": 0, "abusive": 1, "normal": 2, "spam": 3}, inplace = True)
        num_labels = 4
    elif dataset_name == 'davidson2017':
        dataset.label.replace({"hateful": 0, "offensive": 1, "neither": 2}, inplace = True)
        num_labels = 3
else:
    save_path = save_path + '/BERT_binary'
    training_data = pd.read_pickle(data_path + '/training_data_binary.pkl')
    dataset = training_data.copy()
    num_labels = 2

print(dataset.groupby('label').text.count())
if not os.path.exists(save_path):
        os.makedirs(save_path)

# import tokenizer
tokenizer = AutoTokenizer.from_pretrained(model_name)

# add special tokens for URLs, emojis and mentions (--> see pre-processing)
special_tokens_dict = {'additional_special_tokens': ['[USER]','[URL]']}
num_added_toks = tokenizer.add_special_tokens(special_tokens_dict)

texts = dataset.text.astype("string").tolist()
labels = dataset.label.tolist()
encodings = tokenizer(texts, truncation=True, padding=True)
hate_dataset = HateDataset(encodings, labels)
# random split
train_split = int(len(texts) * 0.8)
test_split = int(len(texts) * 0.1)
valid_split = len(texts) - train_split - test_split
train, valid, test = torch.utils.data.random_split(hate_dataset, [train_split, valid_split, test_split], generator=torch.Generator().manual_seed(42))

if args.do_train:
    print("Training...")
    train_labels = [int(x['labels']) for x in train]
    if args.classif_type == 'binary':
        # compute class weights based on training data label distribution
        class_weights = compute_class_weight('balanced', classes = np.unique(train_labels), y = train_labels)
        print(class_weights)

    # Define training arguments
    training_args = TrainingArguments(
        save_steps = 2500,
        output_dir=save_path +'/Checkpoints', # output directory
        num_train_epochs=3,              # total number of training epochs
        per_device_train_batch_size=16,  # batch size per device during training
        per_device_eval_batch_size=64,   # batch size for evaluation
        evaluation_strategy = 'epoch',
        warmup_steps=500,                # number of warmup steps for learning rate scheduler
        weight_decay=0.01,               # strength of weight decay
        learning_rate = 5e-5,
        seed = 123
    )

    def model_init():
        model = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=num_labels)
        # resize to match tokenizer length with special tokens added above
        model.resize_token_embeddings(len(tokenizer))
        return model

    if args.classif_type == 'multi':
        # Instantiate trainer objects
        trainer = Trainer(
            args=training_args,                  
            train_dataset=train,         
            eval_dataset=valid,            
            model_init = model_init
        )
    else:
        class WeightedTrainer(Trainer):
            def compute_loss(self, model, inputs):
                labels = inputs.pop("labels")
                outputs = model(**inputs)
                logits = outputs[0]
                weighted_loss = torch.nn.CrossEntropyLoss(weight=torch.FloatTensor(class_weights)).to(device)
                return weighted_loss(logits, labels)
        trainer = WeightedTrainer(                       
                args=training_args,                  
                train_dataset=train,         
                eval_dataset=valid,            
                model_init = model_init
            )


    # perform hyperparameter tuning using grid search
    def custom_hp_space(trial):
        return {
            "learning_rate": tune.grid_search([2e-5, 3e-5, 5e-5]),
            "num_train_epochs": tune.grid_search([2, 3, 4]),
            "per_device_train_batch_size": tune.grid_search([16,32])
        }


    if args.tune:
        best_run = trainer.hyperparameter_search(
        backend = 'ray',
        hp_space = custom_hp_space,
        direction = 'minimize',
        n_trials = 1
        )
        print(best_run)
        for n, v in best_run.hyperparameters.items():
            setattr(trainer.args, n, v)
        trainer.train()
    else:
        trainer.train()

    trainer.save_model(save_path + '/Final')
    tokenizer.save_pretrained(save_path + '/Final')
    print("saved model at ", save_path + '/Final')


# load fine-tuned models
print("Evaluation:")
model = AutoModelForSequenceClassification.from_pretrained(save_path + '/Final')

trainer = Trainer(
    model=model,         
    args=TrainingArguments(
        output_dir=save_path + '/Test',
        per_device_eval_batch_size = 64)
)

with open(save_path + '/Test/metrics.txt', 'w+') as f:
    print('Evaluating')
    f.write(dataset_name + '\n')

    results = trainer.predict(test)
    for metric in results.metrics:
        f.write(metric, results.metrics['{}'.format(metric)])
        print(metric, results.metrics['{}'.format(metric)])
    print()

    # write predictions to series
    preds=[]
    for row in results[0]:
        preds.append(int(np.argmax(row)))

    pred_labels = pd.Series(preds)
    test_labels = [x['labels'] for x in test]

    # print classification reports for each model
    print(classification_report(test_labels,pred_labels))
    f.write(classification_report(test_labels,pred_labels) + '\n')

    # f1 scores
    for average in ['micro', 'macro', 'weighted']:
        print('{} F1 score: {:.2%}'.format(average, f1_score(test_labels,pred_labels, average=average)))
        f.write('{} F1 score: {:.2%}'.format(average, f1_score(test_labels,pred_labels, average=average)) + '\n')
    print(pred_labels.value_counts())
    f.write(pred_labels.value_counts())