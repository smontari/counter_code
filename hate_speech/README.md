# Intermediary fine-tuning for hate speech detection

Data taken from, and code inspired from:
https://github.com/paul-rottger/hatecheck-experiments
Two widely-used hate speech datasets in English:

Davidson et al. (2017) 
Code and paper:
https://github.com/t-davidson/hate-speech-and-offensive-language
https://aaai.org/ocs/index.php/ICWSM/ICWSM17/paper/view/15665
24,783 tweets: 1,430 tweets (5.8%) labelled hateful, 19,190 (77.4%) offensive and 4,163 (16.8%) neither.

Founta et al. (2018)
Code and paper:
https://github.com/ENCASEH2020/hatespeech-twitter
https://arxiv.org/pdf/1802.00393.pdf
99,996 tweets: 4,965 tweets (5.0%) labelled hateful, 27,150 (27.2%) abusive, 14,030 (14.0%) spam and 53,851 (53.9%) normal

Binary classification: collapse labels other than hateful into a single non-hateful label.


***********************
Data from Nozza 2021: https://aclanthology.org/2021.acl-short.114.pdf
All saved on CLEPS, at /home/smontari/scratch/data/hate_speech/Data/raw_training_data/
Languages: english, spanish, italian.

1) Data from Hateval shared task:
Ask for it from here: https://github.com/msang/hateval
Download link: https://datacloud.di.unito.it/index.php/s/eMZdFYq6yRP3zeL
Password: 2019hateval

2) Data from EVALITA:
AMi 2018: https://github.com/MIND-Lab/ami2018
https://drive.google.com/open?id=1YpB8fADUNvwhKNlDvlQAtgeERMqmc2W0
English Training Set:
- File name: en_training.zip
- Password: !AMI_en_training_2018?
Italian Training Set:
- File name: it_training.zip
- Password: ?AMI_it_training_2018!
English Test Set:
- File name: en_testing.zip
- Password: !AMI_en_testing_2018?
Italian Test Set:
- File name: it_testing.zip
- Password: ?AMI_it_testing_2018!

HaSpeeDe: 
https://github.com/msang/haspeede
HaSpeeDe 2018 - Facebook dataset: https://drive.google.com/drive/folders/1JwijjimNSeh_zO1cC51CZe8jpeH-cAW7
HaSpeeDe 2018 - Twitter dataset: https://drive.google.com/drive/folders/1TEMHcWc96mIQe6LmyPLpC0XTBRKHE0aP

*********************
/home/smontari/scratch/data/hate_speech/git_repositories

Sexism:
French: https://github.com/patriChiril/An-Annotated-Corpus-for-Sexism-Detection-in-French-Tweets
TO REHYDRATE
Get all tweet ids: cat corpus_SexistContent.csv | cut -d $'\t' -f 1 > twids.txt
hydrate them: twarc2 hydrate twids.txt hydrated_tweets.jsonl
 ISSUE: too few tweets are retrieved !!!
--> demander ! TODO

Religious:
Arabic: https://github.com/nuhaalbadi/Arabic_hatespeech
TO REHYDRATE
cat train.csv | cut -d \, -f 1 > twids_train.txt
tail -n +2 twids_train.txt > twids_train.txt.tmp && mv twids_train.txt.tmp twids_train.txt
cat test.csv | cut -d \, -f 1 > twids_test.txt
tail -n +2 twids_test.txt > twids_test.txt.tmp && mv twids_test.txt.tmp twids_test.txt
twarc2 hydrate twids_train.txt hydrated_tweets_train.jsonl
twarc2 hydrate twids_test.txt hydrated_tweets_test.jsonl
 ISSUE: too few tweets are retrieved !!!

Multilingual, diverse domains: https://github.com/HKUST-KnowComp/MLMA_hate_speech
EN, FR, AR
For cross-lingual transfert ! (not cross-domain)


#####################################

use preprocessing.py to prepare datasets, selecting the dataset from: ['davidson2017', 'founta2018', 'hateval', 'evalita', 'ami', 'fr_sex', 'ar_rel']

OR use create_equivalent_datasets to prepare one of Nozza's datasets ('hateval', 'evalita', 'ami') with sampling to same size in all languages, same sentence length distribution and same proportion of hate speech after sampling, and splitting to train-dev-test-blind.

Use zeroshot-baseline-nozza to train binary weighted hate speech classifier. 

Use load_macham_models to save each multitask trained model with macham to transformers library format.