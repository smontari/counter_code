import pandas as pd
import pickle
from html import unescape
import re, os
import numpy as np
import wordsegment as ws
from sklearn.model_selection import train_test_split
import torch
import numpy as np
import pandas as pd
from scipy.stats import ks_2samp
import numpy as np
import pickle, json
import os
#from sklearn.model_selection import train_test_split
import ipdb
torch.manual_seed(42)
ws.load() # load vocab for word segmentation


def load_json_multiple(segments):
  chunk = ""
  for segment in segments:
    chunk += segment
    try:
      yield json.loads(chunk)
      chunk = ""
    except ValueError:
      pass


def regex_match_segmentation(match):
    """
    helper function for segmenting hashtags found through regex"""
    return ' '.join(ws.segment(match.group(0)))

def clean_text(text):
    # convert HTML codes, lowercase
    text = unescape(text).lower()
    # replace mentions, URLs and emojis with special token
    text = re.sub(r"@[A-Za-z0-9_-]+",'[USER]',text)
    text = re.sub(r"http\S+",'[URL]',text)
    # find and split hashtags into words
    text = re.sub(r"#[A-Za-z0-9]+", regex_match_segmentation, text)
    # remove punctuation at beginning of string (quirk in Davidson data)
    text = text.lstrip("!")
    # remove newline and tab characters
    text = text.replace("\r\n\'",' ').replace('\n',' ').replace('\t',' ')
    text = text.replace('&amp;', '&')
    return text


def clean_dataset(dataset):
    # create index column and rename to ID
    dataset.reset_index(inplace=True)
    dataset.rename(columns={'index': 'id'}, inplace=True, errors='ignore')
    # drop unneccessary columns
    dataset = dataset[['id','text','label']]
    # tidy up column types
    dataset = dataset.convert_dtypes()
    dataset['text']=dataset.text.apply(clean_text)
    return dataset

def split_and_save(dataset, save_path):
    train, devtest = train_test_split(dataset, test_size=0.2, random_state=42)
    dev, test = train_test_split(devtest, test_size=0.2, random_state=42)
    for df, splitname in zip([train, dev, test],['train', 'dev', 'test']) :
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        pickle.dump(df, open(save_path  + splitname + '.pkl','wb'))

def compute_len(text):
  return len(text.split())

def compute_metrics(dataset):
    return np.mean(dataset.text.apply(compute_len)), np.var(dataset.text.apply(compute_len)), sum(dataset.label)/len(dataset)

def compute_quantiles(dataset, q1=0.4, q2=0.6):
    return [np.quantile(dataset.text.apply(compute_len), q1), np.quantile(dataset.text.apply(compute_len), q2)]

def do_split(dataset, split_size):
    avg_len, var_len, percent_hate = compute_metrics(dataset)
    print("variance of sentence length: ", var_len)
    #interval = compute_quantiles(dataset, q1=0.4, q2=0.6)
    #while np.mean(new_dataset.text.apply(compute_len)) not in interval and :
    new_dataset = dataset.sample(split_size)
    rep = 0
    while (ks_2samp(new_dataset.text.apply(compute_len), dataset.text.apply(compute_len))[1] < 0.95) and (sum(new_dataset.label)/len(new_dataset) not in [percent_hate-0.05, percent_hate+0.05]):    
        print('re-sampling')
        new_dataset = dataset.sample(split_size)
        rep +=1
    print("nb of rep: ", rep)
    return new_dataset
