

from transformers import AutoModelForSequenceClassification
import torch.nn as nn
import torch
import torch.nn.functional as F
import numpy as np
from transformers import Trainer, TrainingArguments
import pandas as pd
from sklearn.metrics import classification_report, f1_score
import ipdb
from ray import tune
torch.manual_seed(42)
from tqdm.auto import tqdm

class HateDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)

def custom_hp_space(trial):
    """
    perform hyperparameter tuning using grid search
    """
    return {
        "learning_rate": tune.grid_search([2e-5, 3e-5, 5e-5]),
        #"num_train_epochs": tune.grid_search([1, 3, 5]),
        "per_device_train_batch_size": tune.grid_search([16,32])
    }


def evaluate(model, test_data, save_path):
    trainer = Trainer(
        model=model,         
        args=TrainingArguments(
            output_dir=save_path + '/Test',
            per_device_eval_batch_size = 64)
    )
    results = trainer.predict(test_data)
    # write predictions to series
    preds=[]
    for row in results[0]:
        preds.append(int(np.argmax(row)))
    pred_labels = pd.Series(preds)
    test_labels = [x['labels'] for x in test_data]
    f1 = f1_score(test_labels,pred_labels, average='macro')
    return f1