
import transformers
from transformers import AutoTokenizer, AutoModel, AutoConfig, CamembertTokenizer, CamembertForMaskedLM, CamembertConfig, CamembertModel
import torch
import time
import pandas as pd
import datetime
import random
import pickle


def format_time(elapsed):
    '''
    Takes a time in seconds and returns a string hh:mm:ss
    '''
    # Round to the nearest second.
    elapsed_rounded = int(round((elapsed)))
    
    # Format as hh:mm:ss
    return str(datetime.timedelta(seconds=elapsed_rounded))


def make_smart_batches(text_samples, idxs, batch_size):
    '''
    This function combines all of the required steps to prepare batches.
    '''

    print('Creating Smart Batches from {:,} examples with batch size {:,}...\n'.format(len(text_samples), batch_size))

    # =========================
    #   Tokenize & Truncate
    # =========================

    full_input_ids = []

    # Tokenize all training examples
    print('Tokenizing {:,} samples...'.format(len(idxs)))
    # For each training example...
    for text in text_samples:
        
        # Report progress.
        if ((len(full_input_ids) % 10000) == 0):
            print('  Tokenized {:,} samples.'.format(len(full_input_ids)))

        # Tokenize the sample.
        input_ids = tokenizer.encode(text=text,              # Text to encode.
                                    add_special_tokens=True, # Do add specials.
                                    truncation=True,         # Do Truncate!
                                    padding=False)           # DO NOT pad.
                                    
        # Add the tokenized result to our list.
        full_input_ids.append(input_ids)
        
    print('DONE.')
    print('{:>10,} samples\n'.format(len(full_input_ids)))

    # =========================
    #      Select Batches
    # =========================    

    # Sort the two lists together by the length of the input sequence.
    samples = sorted(zip(full_input_ids, idxs), key=lambda x: len(x[0]))

    print('{:>10,} samples after sorting\n'.format(len(samples)))


    # List of batches that we'll construct.
    batch_ordered_sentences = []
    batch_ordered_idx = []

    print('Creating batches of size {:}...'.format(batch_size))
    
    # Loop over all of the input samples...    
    while len(samples) > 0:
        
        # Report progress.
        if ((len(batch_ordered_sentences) % 1000) == 0 \
            and not len(batch_ordered_sentences) == 0):
            print('  Selected {:,} batches.'.format(len(batch_ordered_sentences)))

        # `to_take` is our actual batch size. It will be `batch_size` until 
        # we get to the last batch, which may be smaller. 
        to_take = min(batch_size, len(samples))

        # Pick a random index in the list of remaining samples to start
        # our batch at.
        select = random.randint(0, len(samples) - to_take)

        # Select a contiguous batch of samples starting at `select`.
        #print("Selecting batch from {:} to {:}".format(select, select+to_take))
        batch = samples[select:(select + to_take)]

        #print("Batch length:", len(batch))

        # Each sample is a tuple--split them apart to create a separate list of 
        # sequences and a list of idx for this batch.
        batch_ordered_sentences.append([s[0] for s in batch])
        batch_ordered_idx.append([s[1] for s in batch])

        # Remove these samples from the list.
        del samples[select:select + to_take]

    print('\n  DONE - Selected {:,} batches.\n'.format(len(batch_ordered_sentences)))

    # =========================
    #        Add Padding
    # =========================    

    print('Padding out sequences within each batch...')

    py_inputs = []
    py_attn_masks = []
    py_idx = []

    # For each batch...
    for (batch_inputs, batch_idx) in zip(batch_ordered_sentences, batch_ordered_idx):

        # New version of the batch, this time with padded sequences and now with
        # attention masks defined.
        batch_padded_inputs = []
        batch_attn_masks = []
        
        # First, find the longest sample in the batch. 
        # Note that the sequences do currently include the special tokens!
        max_size = max([len(sen) for sen in batch_inputs])

        # For each input in this batch...
        for sen in batch_inputs:
            
            # How many pad tokens do we need to add?
            num_pads = max_size - len(sen)

            # Add `num_pads` padding tokens to the end of the sequence.
            padded_input = sen + [tokenizer.pad_token_id]*num_pads

            # Define the attention mask--it's just a `1` for every real token
            # and a `0` for every padding token.
            attn_mask = [1] * len(sen) + [0] * num_pads

            # Add the padded results to the batch.
            batch_padded_inputs.append(padded_input)
            batch_attn_masks.append(attn_mask)

        # Our batch has been padded, so we need to save this updated batch.
        # We also need the inputs to be PyTorch tensors, so we'll do that here.
        # Todo - Michael's code specified "dtype=torch.long"
        py_inputs.append(torch.tensor(batch_padded_inputs))
        py_attn_masks.append(torch.tensor(batch_attn_masks))
        py_idx.append(torch.tensor(batch_idx))
    
    print('  DONE.')

    # Return the smart-batched dataset!
    return (py_inputs, py_attn_masks, py_idx)

model_path = '/home/smontari/scratch/data/twitter-scraping/antipass/models/test-mlm/'

#tokenizer = CamembertTokenizer.from_pretrained("camembert-base", do_lower_case=True)
#model = CamembertForMaskedLM.from_pretrained("camembert-base")

config = CamembertConfig.from_pretrained(model_path + 'config.json')
tokenizer = CamembertTokenizer.from_pretrained(model_path, do_lower_case=True)
model = CamembertModel.from_pretrained(model_path, config=config)
with torch.no_grad():
    #config = AutoConfig.from_pretrained("camembert-base", output_hidden_states=True)
    #model = AutoModel.from_pretrained("camembert-base", config=config)
    #embeddings, _, all_layer_embeddings = model(encoded_sentence)


    # encoded_sentence = tokenizer.encode('Voici ma phrace @ala #hihi .')
    # encoded_sentence = torch.tensor(encoded_sentence).unsqueeze(0)
    # output= model(encoded_sentence)
    # sentence_embedding = torch.mean(output[0].detach(), dim=1)

    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model.to(device)
    model.eval()

    # raw_dataset=['Voici ma phrace @ala #hihi .', 'Voici ma .', 'Voici ma ffsg sergsth dtrheyts phrace @ala #hihi .']
    # tokenized_dataset = tokenizer(raw_dataset, padding=True, truncation=True, return_tensors="pt")
    # tokenized_dataset = {k: v.to(device) for k, v in tokenized_dataset.items()}
    # outputs = model(**tokenized_dataset)
    # sentence_embedding = torch.mean(outputs[0].detach(), dim=1)
    # print(sentence_embedding.shape)

    ############
    #emb_dict = {}

    #dump_path = '/home/smontari/scratch/data/twitter-scraping/antipass/emb_dict_finetuned_camembert_multiload.pkl'
    #with open(dump_path, 'wb') as file:
    #    pickle.dump(emb_dict, file)

    dump_path = '/home/smontari/scratch/data/twitter-scraping/antipass/emb_ids_list_finetuned_camembert.pkl'

    all_tweets_df = pd.read_csv('/home/smontari/scratch/data/twitter-scraping/antipass/preprocessed/bert_all.csv', sep=",")
    all_tweets = all_tweets_df['text']
    tweet_ids = all_tweets_df['idx']
    batch_size = 100
    print("Number of examples: ", len(tweet_ids))


    (py_inputs, py_attn_masks, py_idx) = make_smart_batches(all_tweets, tweet_ids, batch_size)
    embeddings_tensor = torch.empty((len(tweet_ids), 768), dtype = torch.float32)
    ids_tensor = torch.empty((len(tweet_ids),), dtype = torch.int64)
    
    print("Extracting embeddings...")

    t0 = time.time()
    for step in range(0, len(py_inputs)):

        # Progress update every 100 batches.
        if step % 50 == 0 and not step == 0:
            # Calculate elapsed time in minutes.
            elapsed = format_time(time.time() - t0)
            
            # Calculate the time remaining based on our progress.
            steps_per_sec = (time.time() - t0) / step
            remaining_sec = steps_per_sec * (len(py_inputs) - step)
            remaining = format_time(remaining_sec)

            # Report progress.
            print('  Batch {:>7,}  of  {:>7,}.    Elapsed: {:}.  Remaining: {:}'.format(step, len(py_inputs), elapsed, remaining))
            #with open(dump_path, 'ab') as file:
            #    pickle.dump(emb_dict, file)
            #    emb_dict = {}

        # Copy the batch to the GPU.
        b_input_ids = py_inputs[step].to(device)
        b_input_mask = py_attn_masks[step].to(device)
        b_ids = py_idx[step]
        outputs = model(b_input_ids, token_type_ids=None, 
                            attention_mask=b_input_mask)

        embeddings = outputs[0].detach().cpu()
        sent_embs = torch.mean(embeddings, dim=1)
        batch_ids = b_ids

        embeddings_tensor[step*batch_size:(step+1)*batch_size] = sent_embs
        ids_tensor[step*batch_size:(step+1)*batch_size] = batch_ids
    
        #for emb, id in zip(sent_embs, batch_ids):
        #    emb_dict[id.item()]= emb
        
    with open(dump_path, 'wb') as file:
        pickle.dump((embeddings_tensor, ids_tensor), file)
        #emb_dict = {}
