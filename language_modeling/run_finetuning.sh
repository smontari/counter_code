#!/bin/bash

#SBATCH --job-name=finetuning    # create a short name for your job
#SBATCH --nodes=1                # node count
#SBATCH --ntasks=1               # total number of tasks across all nodes
#SBATCH --cpus-per-task=10       # cpu-cores per task (>1 if multi-threaded tasks)
#SBATCH --partition=gpu          # Name of the partition
#SBATCH --gres=gpu:rtx6000:1     # GPU nodes are only available in gpu partition
#SBATCH --mem=16G                # Total memory allocated
#SBATCH --hint=multithread       # we get physical cores not logical
#SBATCH --time=48:00:00          # total run time limit (HH:MM:SS)
#SBATCH --output=finetuning%j.out   # output file name
#SBATCH --error=finetuning%j.err    # error file name

echo "### Running $SLURM_JOB_NAME ###"

set -x
cd ${SLURM_SUBMIT_DIR}

module purge
module load cuda/11.1

# Set your conda environment
#source /home/$USER/.bashrc
source activate transformers

#python -u ./finetune_mlm.py --model_name_or_path camembert-base --train_file /home/smontari/scratch/data/twitter-scraping/antipass/preprocessed/bert_all.csv --do_train --do_eval --output_dir /home/smontari/scratch/data/twitter-scraping/antipass/models/test-mlm --line_by_line

python3 -u ./finetune_mlm.py --model_name_or_path camembert-base \
    --train_file /home/seddah/scratch/gjawahar/projects/data/sosweet_lyon/tokenized/tweet_text/zip_stopwords_oui/master/ \
    --do_train --do_eval --output_dir /home/smontari/scratch/objects/social_word_emb/full_v2/camembert-finetuned/ --line_by_line 


#python finetune_mlm.py --model_name_or_path bert-base-uncase  --train_file data/kptimes/kptimes_train_forLM.txt --do_train --do_eval --output_dir model/