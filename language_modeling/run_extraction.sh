#!/bin/bash

#SBATCH --job-name=finetuning    # create a short name for your job
#SBATCH --nodes=1                # node count
#SBATCH --ntasks=1               # total number of tasks across all nodes
#SBATCH --cpus-per-task=20       # cpu-cores per task (>1 if multi-threaded tasks)
#SBATCH --partition=gpu          # Name of the partition
#SBATCH --gres=gpu:rtx6000:1     # GPU nodes are only available in gpu partition
#SBATCH --mem=100G                # Total memory allocated
#SBATCH --hint=multithread       # we get physical cores not logical
#SBATCH --time=24:00:00          # total run time limit (HH:MM:SS)
#SBATCH --output=finetuning%j.out   # output file name
#SBATCH --error=finetuning%j.err    # error file name

echo "### Running $SLURM_JOB_NAME ###"

set -x
cd ${SLURM_SUBMIT_DIR}

#module purge
#module load cuda/11.1

# Set your conda environment
source activate transformers

python -u sentence_embedding.py