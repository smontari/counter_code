

** Disclaimer **

/!\ This is not production-ready code /!\


This repository include code produced for the CounteR project between 01/04/2021 and 01/11/2021 by the INRIA Almanach team.  

In particular, it includes code for:

- Scraping and formatting tweets using various public scraping tools.  

- Preprocessing tweets text and metadata, extracting features and building datasets.  

- Descriptive analysis and exploration of the created datasets.  

- Selection of tweets and users given a set of metrics and tool for manual annotation.

- Language model fine-tuning and sentence embedding extraction, to obtain textual features.  

- Creating interaction graphs from Twitter datasets, modeling static and temporal graph neural networks (TGN) for learning user embedding.  

- Clustering Twitter users given all the user-specific features extracted or learned, and doing visualisation relying on dimensionality reduction.

- Baseline for radicalisation detection and hate speech detection using pre-trained language models.

- Multi-task framework to perform zero-shot cross-lingual hate speech detection using several complementary tasks (sentiment analysis, dependency parsing...).