
import pandas as pd
import numpy as np
from tqdm import tqdm
import torch
from sklearn.model_selection import train_test_split
import re
import random
import matplotlib.pyplot as plt
import collections
import string
import time
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_score
import warnings
warnings.filterwarnings('ignore')
from transformers import AutoModelForTokenClassification, AutoTokenizer, AutoModel


start_time = time.time()

# load data
with open("/home/smontari/data/radicalisation_detection/isis-related-tweets/AboutIsis.csv") as f:
   print(f)

about_isis = pd.read_csv("/home/smontari/data/radicalisation_detection/isis-related-tweets/AboutIsis.csv", encoding='latin1')
isis_fanboy = pd.read_csv("/home/smontari/data/radicalisation_detection/isis-related-tweets/IsisFanboy.csv", encoding='latin1')
reduced_aboutisis = random.sample(list(about_isis['tweets']), len(isis_fanboy))
df = pd.DataFrame()
df['text'] = list(isis_fanboy['tweets']) + reduced_aboutisis
df['labels'] = len(isis_fanboy['tweets'])*[1] + len(reduced_aboutisis)*[0]
df = df.dropna()
print(df.head)

print(len(df))
MODEL_NAME = 'distilbert-base-multilingual-cased'

# We need to create the model and tokenizer
model = AutoModel.from_pretrained(MODEL_NAME)
tokenizer = AutoTokenizer.from_pretrained(MODEL_NAME)

# tokenize the sentences = divide them into wordpieces.

tokenized = df['text'].apply((lambda x: tokenizer.encode(x, add_special_tokens=True)))
print(tokenized[0])
# pad all sequences to the same size, to feed the model with a matrix instead of a list of lists (much faster).
max_len = 0
for i in tokenized.values:
    if len(i) > max_len:
        max_len = len(i)

padded = np.array([i + [0]*(max_len-len(i)) for i in tokenized.values])

# mask to ignore the padded section of the sequences
attention_mask = np.where(padded != 0, 1, 0)
attention_mask.shape

print("start extraction")
# Feed all sequences to the model. Return the weights of the last hidden layer (but we could use any combination of layers)
input_ids = torch.tensor(padded)  
attention_mask = torch.tensor(attention_mask)
last_hidden_states = []

with torch.no_grad():
    for seq, mask in zip(tqdm(input_ids), attention_mask):
        last_hidden_states.append(model(seq[None,:], attention_mask=mask[None,:]).last_hidden_state[0,0])
        #last_hidden_states = [model(input_ids, attention_mask=attention_mask).last_hidden_state[0,0]]

print(last_hidden_states.shape())
#split our datset into a training set and testing set
labels = df['labels']
train_features, test_features, train_labels, test_labels = train_test_split(last_hidden_states, labels, shuffle=True, test_size = 0.2, random_state = 42)

lr_clf = LogisticRegression()
lr_clf.fit(train_features, train_labels)

print(lr_clf.score(test_features, test_labels))