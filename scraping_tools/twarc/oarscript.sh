#!/bin/bash
#
# Comments starting with #OAR are used by the resource manager if using "oarsub -S"
# The job is submitted to the default queue
#OAR -q default
#
module load conda
source activate twint
#
d="2021-07-18"
until [[ $d > 2021-08-18 ]]; do 
    echo "$d"
    d=$(date -I -d "$d + 1 day")
    twarc2 search  --start-time $d --end-time $(date -I -d "$d + 1 day") "lang:fr (#antivax OR #Antivaccin OR #PassSanitaire OR #antivaxx OR #antipass OR #AntiPassSanitaire OR #antivaxxers OR #passdelahonte OR #manifs7aout OR #manif31juillet OR #passsanitairedelahonte OR #manif7aout OR #manif14aout OR #vaccination OR #nonaupassdelahonte OR #vaccinezvous OR #manifestation31juillet OR #vaccinationobligatoire OR #manifs31juillet OR #dictaturesanitaire OR #vaccinobligatoire OR #nonaupasssanitaire OR #NonAuVaccinObligatoire OR #TouchezPasAuxEnfants OR #manif21aout OR  #NonAuPasseportSanitaire OR #VaccinationCovid OR #VaccinezvousBordel)" > /data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/$d.json
done



