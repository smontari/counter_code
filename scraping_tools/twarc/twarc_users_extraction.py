import os
import networkx as nx
import subprocess
from tqdm import tqdm
import hvplot.networkx as hvnx


#os.system("twarc friends zehavoc > /home/smontari/code/data_processing/tweets-analysis/data/zehavoc_twarc_friends.txt")
#os.system("twarc followers zehavoc > /home/smontari/code/data_processing/tweets-analysis/data/zehavoc_twarc_followers.txt")

#os.system("twarc users /home/smontari/code/data_processing/tweets-analysis/data/zehavoc_twarc_followers.txt > /home/smontari/code/data_processing/tweets-analysis/data/zehavoc_twarc_followers.jsonl")

#print("done")

# get followers of each follower, but don't save it immediatly:
with open("/home/smontari/code/data_processing/tweets-analysis/data/zehavoc_twarc_followers.txt") as f:
    followers_list = f.readlines()

fol_dict={}
for fol in tqdm(followers_list[:2]):
    fol_list = subprocess.check_output(["twarc", "followers", "fol"])
    fol_dict[fol]=fol_list

print(len(fol_dict))


# build a graph of all followers
MAX_FOLLOWERS = 1000 

Gf = nx.Graph()
for screen_name, followers in fol_dict.items():
    some_followers = list(followers)[:MAX_FOLLOWERS]
    Gf.add_node(screen_name)
    Gf.add_nodes_from(some_followers)
    for follower in some_followers:
        Gf.add_edge(follower, screen_name)

# shave off followers who are follow nobody in our inner network, so NetworkX
# can plot a sane number of people at the edge of the graph
MAX_WITH_0_FOLLOWERS = 40
for screen_name, followers in fol_dict.items():
    nbr_with_no_followers = 0
    for follower in followers:
        edges_of_connected_node = Gf.edges(follower)
        if len(edges_of_connected_node) == 1:
            if nbr_with_no_followers == MAX_WITH_0_FOLLOWERS:
                Gf.remove_node(follower)
            else:
                nbr_with_no_followers += 1
    print("Capping: ", screen_name, nbr_with_no_followers)


# Use graphviz
prog = "neato"  # neato is default layout engine in GraphViz
pos = nx.nx_agraph.graphviz_layout(Gf, prog=prog, root=None, args="")

labels = {}
for node in Gf.nodes():
    labels[node] = ""
    if len(Gf.edges(node)) > 10:
        labels[node] = node
print("done")
hvnx.draw(Gf, pos, with_labels=True, alpha=0.2, labels=labels, font_size=20, font_family='sans-serif')
