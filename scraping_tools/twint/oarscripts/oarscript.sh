#!/bin/bash
#
# Comments starting with #OAR are used by the resource manager if using "oarsub -S"
# The job is submitted to the default queue
#OAR -q default
#
module load conda
source activate twint
#
python  -u ../extraction_script_nef.py