import twint
from time import sleep
import os, sys
import aiohttp
#import nest_asyncio
#nest_asyncio.apply()


def columne_names():
  return twint.output.panda.Tweets_df.columns

def twint_to_pd(columns):
  return twint.output.panda.Tweets_df[columns]


def save_csv(c, name):
    c.Store_csv = True
    c.Output = "/home/smontari/scratch/data/twitter-scraping/" + name + '_' + c.Search +"_test.csv"
    c.Custom["tweet"] = ["id", "date", "time", "username", "tweet", "replies_count", "retweets_count", "likes_count"]

    scrap_loop(c)

def save_json(c, name):
    c.Store_json = True
    output_dir = "/home/smontari/code/data_processing/tweets-analysis/data/" + name + '_' + c.Search + '/'
    if not os.path.exists(output_dir):
         os.makedirs(output_dir)
    c.Output = output_dir
    #c.Custom["tweet"] = ["id", "date", "time", "username", "tweet", "replies_count", "retweets_count", "likes_count", "urls", "client", "language", "mentions"]

    scrap_loop(c)

def scrap_loop(c):
    # problem: c.Resume doesn't work sometimes, and the scraping starts again from current time. 
    # Try to add the "until" statement. But wouldn't work if the scraping re-starts without this statement being called.
    # The time element on Until doesn't work, only the date is used. So always re-start at the beginning of the day.
    # Debug: The format is defined in datelock.py. It is called in run.py. All OK. Then run.py call RequestUrl in get.py which call Search in url.py --> here, config.until doesnt have time anymore!
    # modified function _formatDate in url.py, but still doesnt work. Assume this function does not work from the twitter side. 
    # Possible problem: using the tweet id does not work ! need to use object "cursor" from "response" so I print it in the logfile and set c.Debug to True (in fct Feed in run.py).
    # doesn't work, still resume from current time. Problem with scroll ? Printed it everywhere, the right resume scroll is fed into "request" but it has no effect on the response
    # second problem: when the access token is refreshed, strats from scratch again. It happens when TokenExpiryException (run.py, token.py)
    # 
    c.Resume = "twint-last-request.log" #c.Output
    # if os.path.exists(c.Output):
    #     with open(c.Output, 'r') as rFile:
    #         endline = rFile.readlines()[-1].split(',')
    #         untildate = endline[1] +' ' + endline[2]
    #     print("resume from " + untildate)
    #    c.Until = untildate
    try:
        twint.run.Search(c)
    except aiohttp.client_exceptions.ClientPayloadError:
        print('loop Payload')
        sleep(2)
        scrap_loop(c)
    except aiohttp.client_exceptions.ClientConnectorError:
        print('loop Connector')
        sleep(5)
        scrap_loop(c)
    except aiohttp.client_exceptions.ClientOSError:
        print('loop OS')
        sleep(10)
        scrap_loop(c)
    except ConnectionResetError:
        print('loop Reset')
        sleep(5)
        scrap_loop(c)


def main():
    c = twint.Config()
    c.Debug = True
    #c.Username = "zehavoc"
    #c.Limit = 20
    c.Lang= "fr"
    #c.Store_object = True
    c.Pandas = True
    c.Hide_output = True
    keyword = "jerusalem OR israel OR gaza OR palestin"
    #"cergy joseph"
    #"cergy OR joseph "
    #"youssoupha"

    # Separate objects are necessary.

    f = twint.Config()
    f.Username = "zehavoc"
    #f.Limit = 20
    f.Custom["user"] = ["bio"]
    f.User_full = False
    f.Store_csv = True
    f.Output = "/Users/smontari/Projects/tweets-analysis/data/lookup_" + f.Username +"_test.csv"
    f.Debug = True
    f.Images = True
    f.Hide_output = True
    c.Since = "2021-05-05"
    c.Until = "2021-05-11"

    runs = {"profile": twint.run.Profile,
        "search": twint.run.Search, 
        "following": twint.run.Following, # not working CRITICAL:root:twint.feed:Follow:IndexError
        "followers": twint.run.Followers, # not working CRITICAL:root:twint.feed:Follow:IndexError
        "favorites": twint.run.Favorites, # not working: list index out of range [x] feed.MobileFav, [!] Twitter does not return more data, scrape stops here.
        "lookup": twint.run.Lookup # not working - CRITICAL:root:twint.get:User:'NoneType' object is not subscriptable
    }


    # Something breaks if we don't split these up
    names = list(runs.keys())
    names = ["search"]
    for run_name in names:
        print(run_name)
        if runs[run_name] == twint.run.Search:
            
            c.Search = keyword
            
            #twint.run.Search(c)
            #print(columne_names())
            #data = twint.output.panda.Tweets_df[columne_names()]
            #print(data.head())
            
            save_json(c, run_name)
        elif run_name[:6]=="follow" or run_name == "lookup":
            scrap_loop(f)
        else:
            c.Since = ""
            c.Until = ""
            save_csv(c, runs[run_name], run_name)

        


if __name__ == '__main__':
    main()
