# -*- coding: utf-8 -*-

import tweepy
import csv
import json
import preprocessor as p
import string
from nltk.corpus import stopwords
import pandas as pd
from nltk.stem.snowball import FrenchStemmer
from datetime import date
from datetime import datetime
import time

class ExtractTweets:
    """
    Access Twitter API and get tweets for dynamic word embeddings analysis
    """
    def __init__(self, args):
        """
        Constructor
        """
        #cred_path=os.path.join(os.getcwd(),'twitter_credentials.json')
        cred_path='twitter_credentials.json'

        try:
            with open(cred_path) as cred_data:
                info = json.load(cred_data)
                self.consumer_key = info['CONSUMER_KEY']
                self.consumer_secret = info['CONSUMER_SECRET']
                self.access_key = info['ACCESS_KEY']
                self.access_secret = info['ACCESS_SECRET']
        except (FileNotFoundError, IOError):
            print("[IOError] Twitter API Keys file not found.\nExiting...")
            exit(1)
        except KeyError:
            print("[KeyError] Appropriate Twitter API Keys not found in keys file.\nExiting...")
            exit(1)
        except Exception as exp:
            print("[ERROR] %s" % exp)
            exit(1)

        self.query = args.query
        self.query_type = args.query_type
        if args.max_nb == "max":
            self.max = True
        else:
            self.max = False
            self.max_nb_tweets = int(args.max_nb)
        self.output_dir = args.output_dir
        self.preprocess = args.preprocess
        self.overwrite = args.overwrite
        print(args)

    def limit_handled(self, cursor, list_name):
        while True:
            try:
                yield cursor.next()
            except tweepy.RateLimitError:
                print("\nCurrent number of data points in list = " + str(len(list_name)))
                print('Hit Twitter API rate limit.')
                for i in range(3, 0, -1):
                    print("Wait for {} mins.".format(i * 5))
                    time.sleep(15 * 60)
                print('Hit Twitter API rate limit.')
                for i in range(3, 0, -1):
                    print("Wait for {} mins.".format(i * 5))
                    time.sleep(5 * 60)
            except tweepy.error.TweepError:
                print('\nCaught TweepError exception' )

    def preprocess_tweet(self, text):
        """
        Utility function to clean tweet text by removing links and
        special characters using simple regex statements.
        Preprocessing tweets tool :
        https://pypi.org/project/tweet-preprocessor/
        """
        p.set_options(p.OPT.URL) #, p.OPT.EMOJI,p.OPT.SMILEY, p.OPT.MENTION, p.OPT.NUMBER, p.OPT.RESERVED)
        cleaned_text=p.clean(text)
        if self.preprocess:
            tokenized_text = p.tokenize(cleaned_text)

            tokenized_text1=[ch.replace('-', ' ') for ch in tokenized_text]
            tokenized_text2 = ''.join([ch.replace("’", ' ') for ch in tokenized_text1])
            tokenized_text3 = ''.join([ch.replace("'", ' ') for ch in tokenized_text2])

            exclude = set(string.punctuation)
            exclude.update(("«","»" ))
            stop = set(stopwords.words('french'))
            stop.update(("RT", "FAV"))

            stop_free = " ".join([i for i in tokenized_text3.lower().split() if ((i not in stop))])
            punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
            no_lone=' '.join( [w for w in punc_free.split() if len(w)>1] )
            
            stemmer = FrenchStemmer()
            stemmed_text = " ".join([stemmer.stem(word) for word in no_lone.split()])
            cleaned_text = no_lone
        return cleaned_text
        
            

    def query_api_user(self):
        print("Authorizing app...")
        # Create OAuthHandler object
        auth = tweepy.OAuthHandler(self.consumer_key,
                            self.consumer_secret)
        # Set access token and secret
        auth.set_access_token(self.access_key,
                              self.access_secret)

        # Create tweepy API object
        api = tweepy.API(auth, wait_on_rate_limit=True)
        # user personal info
        user_infos = {}
        user = api.get_user(screen_name	=self.query)
        keys = ["screen_name", "name", "description", "listed_count", "followers_count", "statuses_count", "friends_count", "favourites_count", "url", "id_str",\
             "location", "verified", "created_at", "profile_image_url_https", "default_profile", "default_profile_image"]
        user_data = [user.screen_name, user.name, user.description, user.listed_count, user.followers_count, user.statuses_count, user.friends_count, user.favourites_count, user.url, user.id_str,\
             user.location, user.verified, str(user.created_at), user.profile_image_url_https, user.default_profile, user.default_profile_image]
        user_infos["info"]=dict(zip(keys, user_data))
        # user lists
        lists = api.get_lists(screen_name=self.query)
        lists_dict = {}
        list_keys = ["name", "created_at","subscriber_count", "id_str", "member_count", "description"]
        for liste in lists:
            list_data = [liste.name, str(liste.created_at), liste.subscriber_count, liste.id_str, liste.member_count , liste.description]
            lists_dict[liste.name] = dict(zip(list_keys, list_data))
        user_infos["lists"]= lists_dict
        # user followers and friends

        followers = []
        # debug
        # issue with get_followers: tweepy.errors.TweepyException: This method does not perform pagination
        # But api.get_followers.pagination_mode = 'cursor'
        # 
        #print(len(api.get_followers(screen_name=self.query)[0]))
        #for i, page in enumerate(self.limit_handled(tweepy.Cursor(api.get_followers(screen_name=self.query).items()), followers)):
        #    followers += page
        followers =api.get_followers(screen_name=self.query)
        user_infos["followers"] = [[user.screen_name, user.description, user.followers_count, user.id_str, user.location] for user in followers]
        friends = api.get_friends(screen_name	=self.query)
        user_infos["friends"] = [[user.screen_name, user.description, user.followers_count, user.id_str, user.location] for user in friends]

        output_path=(self.output_dir + 'user_info_' + self.query + '.json')
        with open(output_path, 'w', encoding='utf-8') as f:
            json.dump(user_infos, f, indent=4, ensure_ascii=False)
        """
        cursor = tweepy.Cursor(api.followers, count=200).pages()
        friends = []
        for i, page in enumerate(self.limit_handled(tweepy.Cursor(api.friends, count=200).pages(), friends)):
            print("\r"+"Loading"+ i % 5 *".", end='')
            friends += page
        """

    def query_api(self, oldest_id):
        print("Authorizing app...")
        # Create OAuthHandler object
        auth = tweepy.OAuthHandler(self.consumer_key,
                            self.consumer_secret)
        # Set access token and secret
        auth.set_access_token(self.access_key,
                              self.access_secret)

        # Create tweepy API object to fetch tweets
        api = tweepy.API(auth, wait_on_rate_limit=True)
        if oldest_id is not None:
            oldest_id = oldest_id - 1

        if self.query_type == 'userid':
                tweets = api.user_timeline(screen_name=self.query, 
                            count=100,
                            include_rts = False,
                            tweet_mode = 'extended',
                            max_id = oldest_id,
                            lang='fr',
                            since="2018-11-01",
                            )

        elif self.query_type == 'hashtag': 
            tweets = tweepy.Cursor(api.search,
                                q=self.query,
                                count=100,
                                lang='fr',
                                include_rts = False,
                                since="2018-11-01",
                                tweet_mode='extended',
                                max_id = oldest_id)
        else:
            print("query_type not recognized")
        return tweets


    def get_tweets(self):
        """
        Gather and store tweets in csv file
        """

        output_path=(self.output_dir + 'tweets_' + self.query_type + '_' + self.query + '_no_RT.csv')
        if self.overwrite:
            csvFile = open(output_path, 'w+', encoding='utf-8')
            csvWriter = csv.writer(csvFile, delimiter=';')
            csvFile.writerow(["id","created_at","text", "lang", "username","likes","retweets", "in reply to"])
        else:
            csvFile = open(output_path, 'a', encoding='utf-8')
            csvWriter = csv.writer(csvFile, delimiter=';')
        nb_tweets = 0
        oldest_id = None
        #if self.max:
            # todo make it work for as much tweets as possible
        while nb_tweets < self.max_nb_tweets:
            tweets = self.query_api(oldest_id)
            if len(tweets) == 0:
                break
            oldest_id = tweets[-1].id
            for tweet in tweets:
                if (not tweet.retweeted) and ('RT @' not in tweet.full_text):
                    nb_tweets = nb_tweets +1
                    #text = self.preprocess_tweet(tweet.full_text)
                    csvWriter.writerow([tweet.id_str, tweet.created_at, tweet.text, tweet.lang, tweet.user.screen_name,\
                         tweet.favorite_count, tweet.retweet_count, tweet.in_reply_to_screen_name])
                    if (nb_tweets%100 == 0):
                        print("extracted tweets : "+ str(nb_tweets))

        print('Extracted ' + str(nb_tweets) \
              + ' tweets with ' + self.query_type + ' ' + self.query)


    def run(self):
        if self.query_type == 'userinfo':
            return self.query_api_user()
        elif self.query_type in ['userid', 'hashtag']:
            return self.get_tweets()






# list of hashtags : #giletjaune #GiletsJaunes #GILETS_JAUNES #giletsjaunes #GiletJaune

#tweets_df = pd.read_csv('/home/montariol/Projets/Dynamic_word_embeddings/Data/Tweets/tweets_with_hashtag_#giletjaune.csv',encoding='utf-8',delimiter=";",header=None)

