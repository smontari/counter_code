from datetime import time
import time as strtime
import datetime
import os
import sys
import pprint
import numpy as np
import argparse

start = time()

import extract_tweets


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--query_type', '-qt', type=str, default='hashtag', help='''
            Query type: either hashtag, userid, userinfo''')
    parser.add_argument('--query', '-q', type=str, default='gilet-jaune', help='''
            If True : train backward from most recent time step''')
    parser.add_argument('--max_nb', '-nb', type=str, default='100', help='''
            Max number of tweets you want to extract. Use "max" to get as much as possible.''')
    parser.add_argument('--preprocess', '-pp', type=bool, default=False, help='''
            Wether we want the tweet text cleaned (punctuation, stopwords, etc) or not.''')
    parser.add_argument('--overwrite', '-ov', type=bool, default=True, help='''
            OverWrite any tweets previously extracted using the same query, instead of appending to the file.''')
    parser.add_argument('--save', type=str, default=None, help='''
                Metadata from tweets to save''')
    parser.add_argument('--output_dir', type = str, default = '/home/smontari/code/data_processing/tweets-analysis/data/', help='''
        Path to the output directory.''') 

    args = parser.parse_args()
    return args

args = parse_args()
extractor = extract_tweets.ExtractTweets(args)
extractor.run()
