# -*- coding: utf-8 -*-
"""
Created on Fri Jan  4 15:56:51 2019

@author: montariol
"""

import pandas as pd

tweets_df = pd.read_csv('/people/montariol/data/Twitter/tweets_with_hashtag_#giletjaune_no_RT.csv',encoding='utf-8',delimiter=";",header=None)
tweets_df.head

tweets_df.columns = ['date','id','text']
tweets_df['date'] = pd.to_datetime(tweets_df['date'], infer_datetime_format=True)

tokenized_corpus= [str(tweet).split() for tweet in tweets_df['text'].tolist()]

wc = {'<UNK>': 1}
for sentence in tokenized_corpus:
    for word in sentence:
        wc[word] = wc.get(word, 0) + 1

wc=sorted(wc, key=wc.__getitem__)




test=tweets_df['text'].iloc[2]




from nltk.stem.snowball import FrenchStemmer
stemmer = FrenchStemmer()
stemmed_text = " ".join([stemmer.stem(word) for word in test.split()])



