import json
import numpy as np
import pandas as pd
from pathlib import Path
import argparse
import pickle
import ipdb
import torch


def preprocess(data_name):
  u_list, i_list, ts_list, label_list = [], [], [], []
  feat_l = []
  idx_list = []

  with open(data_name) as f:
    s = next(f)
    for idx, line in enumerate(f):
      e = line.strip().split(',')
      u = int(e[0])
      i = int(e[1])

      ts = float(e[2])
      label = float(e[3])  # int(e[3])

      feat = np.array([int(x) for x in e[4:]])

      u_list.append(u)
      i_list.append(i)
      ts_list.append(ts)
      label_list.append(label)
      idx_list.append(idx)

      feat_l.append(feat)
  return pd.DataFrame({'u': u_list,
                       'i': i_list,
                       'ts': ts_list,
                       'label': label_list,
                       'idx': idx_list}), np.array(feat_l)


def reindex(df, bipartite=True):
  new_df = df.copy()
  if bipartite:
    assert (df.u.max() - df.u.min() + 1 == len(df.u.unique()))
    assert (df.i.max() - df.i.min() + 1 == len(df.i.unique()))

    upper_u = df.u.max() + 1
    new_i = df.i + upper_u

    new_df.i = new_i
    new_df.u += 1
    new_df.i += 1
    new_df.idx += 1
  else:
    new_df.u += 1
    new_df.i += 1
    new_df.idx += 1

  return new_df


def run(data_name, datapath, node_features_filename, edge_features_path, random_proj_dim, bipartite=True):
  PATH = Path(datapath) / '{}.csv'.format(data_name)
  if edge_features_path:
    if random_proj_dim:
      data_name = data_name+"_"+str(random_proj_dim)
    else:
      data_name = data_name+"_768"

  datapath_save = Path(datapath) / "objects_{}".format(data_name)
  datapath_save.mkdir(parents=True, exist_ok=True)
  
  OUT_DF = datapath_save / 'ml_{}.csv'.format(data_name)
  OUT_FEAT = datapath_save / 'ml_{}.npy'.format(data_name)
  OUT_NODE_FEAT = datapath_save / 'ml_{}_node.npy'.format(data_name)

  df, feat = preprocess(PATH)

  # todo changed this for testing
  new_df = reindex(df, bipartite)
  #new_df = df.copy()
  new_df = new_df.sort_values(by=['ts'], ascending = True)

  if edge_features_path:
    #edge_features_path = '/home/smontari/code/tweets-analysis/code/analysis/objects/emb_ids_list_finetuned_camembert.pkl'

    with open(edge_features_path, 'rb') as file:
        embeddings_tensor, ids_tensor = pickle.load(file)
    if random_proj_dim:
      proj_mat = torch.randn(768, random_proj_dim)
      embeddings_tensor_small = torch.einsum("nb,bd->nd", embeddings_tensor, proj_mat).numpy()
    else:
      embeddings_tensor_small = embeddings_tensor.numpy()
    # create matrix of all embeddings in the graph
    twid2idx = {int(twid):i for i, twid in enumerate(ids_tensor)}
    print(feat.shape)
    event_id_list = feat
    print(len(set(int(i) for i in event_id_list)&set(twid2idx.keys())))
    all_embs=embeddings_tensor_small[[twid2idx[int(i)] for i in event_id_list]]
    print("feat: ", np.array(all_embs).shape)
    feat = np.array(all_embs)

  empty = np.zeros(feat.shape[1])[np.newaxis, :]
  feat = np.vstack([empty, feat])
  print(feat.shape,feat[0].dtype)
  # todo for test
  feat*=0
  print(feat.shape,feat[0].dtype)

  if node_features_filename:
    NODEPATH = Path(datapath) / '{}.csv'.format(node_features_filename)
    u_list = []
    feat_l = []
    with open(NODEPATH) as f:
      for line in f:
        e = line.strip().split(',')
        u = int(e[0])
        nfeat = np.array([float(x) for x in e[1:]])
        u_list.append(u)
        feat_l.append(nfeat)
    node_feat = np.array(feat_l)
  
  else:
    max_idx = max(new_df.u.max(), new_df.i.max())
    print("nb of users: ", max_idx)
    node_feat = np.zeros((max_idx+ 1, 172))

  new_df.to_csv(OUT_DF)
  np.save(OUT_FEAT, feat)
  np.save(OUT_NODE_FEAT, node_feat)
  print("Preprocessing done!", OUT_DF)

parser = argparse.ArgumentParser('Interface for TGN data preprocessing')
parser.add_argument('--data', type=str, help='Dataset name (eg. wikipedia or reddit)',
                    default='wikipedia')
parser.add_argument('--bipartite', action='store_true', help='Whether the graph is bipartite')

parser.add_argument('--node_features_filename', type=str, default = '',
                    help='Path to node features, if they are included in the input data')
parser.add_argument('--edge-features-path', type=str, default = '', #'/home/smontari/code/tweets-analysis/code/analysis/objects/emb_ids_list_finetuned_camembert.pkl',
                    help='Path to edge features, if they are in a separate pickle file')
parser.add_argument('--random-proj-dim', type=int, default = '', 
                    help='If specified, do random projection of the edge features of the given dimention')

parser.add_argument('--datapath', type=str, help='Dataset directory',
                    default='/data/almanach/user/smontari/scratch/graphs/')

args = parser.parse_args()

run(args.data, args.datapath, node_features_filename=args.node_features_filename, edge_features_path = args.edge_features_path, random_proj_dim = args.random_proj_dim, bipartite=args.bipartite)