#!/bin/bash
#
#
# Comments starting with #OAR are used by the resource manager if using "oarsub -S"
#
# Note : quoting style of parameters matters, follow the example
#
# The job is submitted to the default queue

#OAR -q default
# Path to the binary to run

#oarsub -I -p  "gpu='YES' and gpucapability>='4.0'" -l /gpunum=1 -I
#oarsub -p "gpu='YES' and gpucapability>='6.1' and host='nefgpu49.inria.fr' "  
#      -l /nodes=1/gpunum=2,walltime=10:00:00 -t besteffort ./script_conllu.sh 
#oarsub -l /nodes=1,walltime=10:00:00 ./run_preprocessing.sh
#oarsub -l walltime=10:00:00 ./run_preprocessing.sh
#oarsub -p "mem > 100000 and gpu='YES' and gpucapability>='6.1' " -l /nodes=1/core=3/gpunum=1,walltime=10:00:00 -t besteffort ./script_conllu.sh 
#oarsub -p "gpu='YES'" -l /nodes=1/gpunum=1,walltime=10:00:00 -t besteffort ./run_model.sh

module load conda
source activate pytorch

#python  -u ../tgn_nodewise/utils/preprocess_data.py --data twitter_twarc_small --datapath /data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/ --edge-features-path /data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/emb_ids_list_finetuned_camembert.pkl

#python -u ../tgn_vanilla/utils/preprocess_data.py --data twitter_twarc_small --datapath /data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/ --edge-features-path /data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/emb_ids_list_finetuned_camembert.pkl --random-proj-dim 512

#python -u ../tgn_vanilla/train_self_supervised.py -d twitter_twarc_small_768 --use_memory --prefix tgn-attn --n_runs 1 --datapath /data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/ --n_epoch 1 --randomize_features --bs 100

#python -u ../tgn_nodewise/train_self_supervised.py -d twitter_twarc_small --use_memory --prefix tgn-attn-nodewise --n_runs 1 --datapath /data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/ --n_epoch 1 --randomize_features
# done, see 11905688

#python -u ../tgn_vanilla/train_self_supervised.py -d twitter_twarc_small --use_memory --prefix tgn-attn --n_runs 1 --datapath /data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/ --n_epoch 1 --randomize_features
# done, see 11905672

#python -u ../tgn_vanilla/train_self_supervised.py -d twitter_twarc_small_300 --use_memory --prefix tgn-attn --n_runs 1 --datapath /data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/ --n_epoch 1 --randomize_features --bs 200
# done, see 11905810

python -u ../tgn_nodewise/train_self_supervised.py -d twitter_twarc_small_300 --use_memory --prefix tgn-attn-nodewise --n_runs 1 --datapath /data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/ --n_epoch 1 --randomize_features --bs 100
# done, see 11905812

#python -u ../tgn_vanilla/train_self_supervised.py -d twitter_twarc_small_512 --use_memory --prefix tgn-attn --n_runs 1 --datapath /data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/ --n_epoch 1 --randomize_features --bs 200


