#!/bin/bash
#
#
# Comments starting with #OAR are used by the resource manager if using "oarsub -S"
#
# Note : quoting style of parameters matters, follow the example
#
# The job is submitted to the default queue

#OAR -q default
# Path to the binary to run


#oarsub -I -p  "gpu='YES' and gpucapability>='4.0'" -l /gpunum=1 -I
#oarsub -p "gpu='YES' and gpucapability>='6.1' and host='nefgpu49.inria.fr' "  
#      -l /nodes=1/gpunum=2,walltime=10:00:00 -t besteffort ./script_conllu.sh 

module purge
module load conda

source activate ndependencychar
module load cuda/10.2
module load gcc/7.3.0
module load cudnn/7.6-cuda-10.2


NAME=camembert-oscar-scalar-mix--all_layers
CONFIGFILE="scripts_oar/scripts-french/script_conllu.sh"
TREEBANK= ""

python -u -m npdependency.graph_parser  --train_file $NARABIZI/train.NArabizi_treebank.conllu --dev_file $NARABIZI/dev.NArabizi_treebank.conllu --out_dir output/$MODEL --overwrite $CONFIGFILE \\

