import torch
import numpy as np
import pickle
import pandas as pd
import json
import ipdb
from tqdm import tqdm
from spacy.lang.fr import French
#nlp = French()
#tokenizer = nlp.tokenizer
from sklearn.metrics.pairwise import cosine_similarity
import argparse
from transformers import AutoTokenizer, AutoModel
 
def get_word_idx(sent, encoded, target, tokenizer):
    idx = None
    for wid in set(encoded.word_ids()):
        if wid is not None:
            s,e = encoded.word_to_tokens(wid)
            word = tokenizer.decode(encoded['input_ids'][0][s:e])
            if word == target:
                idx = wid
    return idx
            
    # todo solve issue of word tokenization: finding right word id correspondence

def get_hidden_states(encoded, token_ids_word, model, layers):
    """Push input IDs through model. Stack and sum `layers` (last four by default).
    Select only those subword token outputs that belong to our word of interest
    and average them."""
    with torch.no_grad():
        output = model(**encoded)
    # Get all hidden states
    states = output.hidden_states
    # Stack and sum all requested layers
    output = torch.stack([states[i] for i in layers]).sum(0).squeeze()
    # Only select the tokens that constitute the requested word
    word_tokens_output = output[token_ids_word]
    return word_tokens_output.mean(dim=0)


def get_targets_list(input_path):
    with open(input_path, 'r') as f:
        hasths_count = json.load(f)
    hashts = [k for k,v in hasths_count.items() if v > 200]
    return hashts


def get_embeddings(embeddings_path, datasets, tokenizer, model, targets, layers):
    user_hashtags_embs = {}
    count2sents = {}

    for user, tweets_list in tqdm(datasets.items()):
        user_hashtags_embs[user] = {}
        count2sents[user] = {}
        for sent in tweets_list:
            sent = sent.replace('#',' ').lower()
            encoded = tokenizer.encode_plus(sent.replace('\'', ' \' '), return_tensors="pt").to(device)
            for target in targets:
                user_hashtags_embs[user][target] = np.zeros((768))
                count2sents[user][target] = 0
                #tokens = [w.text for w in tokenizer(sent)]
                idx = get_word_idx(sent, encoded, target, tokenizer)
                if idx is not None:
                    token_ids_word = np.where(np.array(encoded.word_ids()) == idx)
                    word_embedding = get_hidden_states(encoded, token_ids_word, model, layers)
                    user_hashtags_embs[user][target] += word_embedding.detach().cpu().numpy()
                    count2sents[user][target] += 1
        print(user, sum(count2sents[user].values()))

    print("store embeddings to: ", embeddings_path)
    with open(embeddings_path.split('.')[0] + '.pkl', 'wb') as f:
        pickle.dump([user_hashtags_embs, count2sents], f, protocol=pickle.HIGHEST_PROTOCOL)

if __name__ == '__main__':


    parser = argparse.ArgumentParser()
    parser.add_argument("--objects_path",
                        default='/home/smontari/scratch/objects/bataclan_tweets/',
                        type=str,
                        help="Paths were data are stored")
    parser.add_argument("--data_path", default='french_top10_user_tweets_dict_preprocessed.pkl', type=str,
                        help="Path to target file")
    parser.add_argument("--target_path", default='french_hastags_count.txt', type=str,
                        help="Path to data file")
    parser.add_argument("--lang", default='french', const='all', nargs='?',
                        help="Choose a language", choices=['french', 'english', 'arabic'])
    parser.add_argument("--path_to_fine_tuned_model", default='', type=str,
                        help="Path to fine-tuned model. If empty, pretrained model is used")
    parser.add_argument("--embeddings_path", default='embeddings/hashtags_bataclan_french.pkl', type=str,
                        help="Path to output pickle file containing embeddings.")
    args = parser.parse_args()

    data_path = args.objects_path + args.data_path
    embeddings_path = args.objects_path + args.embeddings_path
    
    datasets =  pickle.load(open(data_path, 'rb'))
    target_list = get_targets_list(args.objects_path +  args.target_path)
    print("number of target words: ", len(target_list))
    print("number of users: ", len(datasets))

    layers = [-4, -3, -2, -1]

    if len(args.path_to_fine_tuned_model) > 0:
        fine_tuned = True
        state_dict =  torch.load(args.path_to_fine_tuned_model)
    else:
        fine_tuned = False

    if args.lang == 'french':
        tokenizer = AutoTokenizer.from_pretrained('Yanzhu/bertweetfr-base', do_lower_case=True)
        if fine_tuned:
            state_dict = torch.load(args.path_to_fine_tuned_model)
            model = AutoModel.from_pretrained('camembert-base', state_dict=state_dict, output_hidden_states=True)
        else:
            model = AutoModel.from_pretrained('Yanzhu/bertweetfr-base', output_hidden_states=True)

    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    model.to(device)
    model.eval()

    get_embeddings(embeddings_path, datasets, tokenizer, model, target_list, layers)



