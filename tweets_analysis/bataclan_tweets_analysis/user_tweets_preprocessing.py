import random, re
import json
#from emoji import demojize
import glob
import codecs
from tqdm import tqdm
from nltk.tokenize import TweetTokenizer
from collections import Counter
import matplotlib.pyplot as plt
import ipdb
import pickle


def load_json_multiple(segments):
  chunk = ""
  for segment in segments:
    chunk += segment
    try:
      yield json.loads(chunk)
      chunk = ""
    except ValueError:
      pass

def normalizeTweet(tokens):
    normTweet = " ".join([normalizeToken(token) for token in tokens])    
    normTweet = re.sub(r",([0-9]{2,4}) , ([0-9]{2,4})", r",\1,\2", normTweet)
    normTweet = re.sub(r"([0-9]{1,3}) / ([0-9]{2,4})", r"\1/\2", normTweet)
    normTweet = re.sub(r"([0-9]{1,3})- ([0-9]{2,4})", r"\1-\2", normTweet)
    return " ".join(normTweet.split())

def normalizeToken(token):
    lowercased_token = token.lower()
    if token.startswith("@"):
        return "@USER"
    elif lowercased_token.startswith("http") or lowercased_token.startswith("www"):
        return "HTTPURL"
    #elif len(token) == 1:
    #    return demojize(token)
    else:
        return token

def extract_users_count(lang):
    user_tweets_count = dict()
    source_dir = datapath + lang + '_corpus/'
    source_files = glob.glob(source_dir  + '*.raw.json')
    print(source_dir)
    print("nb of files: ", len(source_files))
    for i in tqdm(range(len(source_files))):
        dfile = source_files[i]
        with codecs.open(dfile, 'r', 'utf-8', errors='ignore') as f:
            for jfile in load_json_multiple(f): 
                text = jfile['text']
                if not text.startswith('RT'):
                    user = jfile['user']['id']
                    if user not in user_tweets_count:
                        user_tweets_count[user]=1
                    else:
                        user_tweets_count[user]+=1
    user_tweets_count = dict(sorted(user_tweets_count.items(), key =lambda kv:(kv[1], kv[0]), reverse=True))
    plt.hist(user_tweets_count.values(), bins=50)
    plt.savefig(objects_path + lang + '_user_count_original_tweets.png')

    with open(objects_path + lang + '_user_count_dict.txt', 'w') as f:
        f.write(json.dumps(dict(user_tweets_count), indent=4))

def extract_top_users_tweets(lang, topn):
    tokenizer = TweetTokenizer()
    with open(objects_path + lang + '_user_count_dict.txt', 'r') as f:
        user_tweets_count = json.load(f)
    users_list = [k for k, v in user_tweets_count.items() if v>topn]
    print("nb of users kept: ", len(users_list)) # 1188
    user_tweets_dict = {}
    source_dir = datapath + lang + '_corpus/'
    source_files = glob.glob(source_dir  + '*.raw.json')
    for i in tqdm(range(len(source_files))):
        dfile = source_files[i]
        with codecs.open(dfile, 'r', 'utf-8', errors='ignore') as f:
            for jfile in load_json_multiple(f): 
                text = jfile['text']
                user = str(jfile['user']['id'])
                if user in users_list and not text.startswith('RT'):
                    if user not in user_tweets_dict:
                        user_tweets_dict[user]=[]
                    tokens = tokenizer.tokenize(text.replace("’", "'").replace("…", "..."))
                    normtext = normalizeTweet(tokens)
                    user_tweets_dict[user].append(normtext)
    pickle.dump(user_tweets_dict, open(objects_path + lang + '_top' + str(topn) + '_user_tweets_dict_preprocessed.pkl', 'wb'))

if __name__ == "__main__":
    languages = ['arabic', 'french', 'english']
    objects_path = "/home/smontari/scratch/objects/bataclan_tweets/"
    datapath = "/home/seddah/scratch/CounteR/CorpusCompBataclan/"
    lang = 'french'
    #extract_users_count(lang)
    extract_top_users_tweets(lang, topn = 10)
