from math import nextafter
import torch
import transformers
from transformers import AutoModel, AutoTokenizer 
from sentence_transformers import SentenceTransformer
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
from tqdm import tqdm
import pickle
import json, re
from nltk.tokenize import TweetTokenizer
import glob
from collections import Counter
import ipdb

def normalizeTweet(tokens):
    normTweet = " ".join([normalizeToken(token) for token in tokens])    
    normTweet = re.sub(r",([0-9]{2,4}) , ([0-9]{2,4})", r",\1,\2", normTweet)
    normTweet = re.sub(r"([0-9]{1,3}) / ([0-9]{2,4})", r"\1/\2", normTweet)
    normTweet = re.sub(r"([0-9]{1,3})- ([0-9]{2,4})", r"\1-\2", normTweet)
    return " ".join(normTweet.split())

def normalizeToken(token):
    lowercased_token = token.lower()
    if token.startswith("@"):
        return "@USER"
    elif lowercased_token.startswith("http") or lowercased_token.startswith("www"):
        return "HTTPURL"
    #elif len(token) == 1:
    #    return demojize(token)
    else:
        return token

def extracthashtag(text):
  hasht=[]
  for word in text.split():
    if word[0]=='#':
        h = word[1:].lower().replace('.','').replace(':','')
        if '…' not in h and len(h)>1:
            hasht.append(h)
  return hasht

def extract_sentence():
    source_dir = datapath + 'english' + '_corpus/'
    source_files = glob.glob(source_dir  + '*.raw.json.raw.txt')
    counter = 0
    pro_sent = []
    anti_sent = []
    for fname in tqdm(source_files):
        with open(fname, 'r') as f:
            for tweet in f.readlines():
                if not tweet.startswith('RT'):
                    tokens = basictokenizer.tokenize(tweet.replace("’", "'").replace("…", "...").replace("#", ""))
                    normtext = normalizeTweet(tokens).lower()
                    if any(h in normtext for h in anti_hasht):
                        nohasht_tweet = normtext
                        for h in anti_hasht:
                            nohasht_tweet = nohasht_tweet.replace(h, "")
                        if len(nohasht_tweet)>3:
                            anti_sent.append(nohasht_tweet)
                    if any(h in normtext for h in pro_hasht):
                        nohasht_tweet = normtext
                        for h in pro_hasht:
                            nohasht_tweet = nohasht_tweet.replace(h, "")
                        if len(nohasht_tweet)>3:
                            pro_sent.append(nohasht_tweet)
    return pro_sent, anti_sent


objects_path = '/home/smontari/scratch/objects/bataclan_tweets/'
datapath = "/home/seddah/scratch/CounteR/CorpusCompBataclan/"

basictokenizer = TweetTokenizer()

tokenizer = AutoTokenizer.from_pretrained('vinai/bertweet-base')
model = AutoModel.from_pretrained('vinai/bertweet-base')
sent_model = SentenceTransformer('sentence-transformers/paraphrase-MiniLM-L6-v2')
model.eval()

anti_hasht = ['islamthetruth4u', 'islamistheproblem', 'banislam',  'stopislam', 'radicalislam']
pro_hasht = ['notinmyname', 'muslimsstandwithparis', 'terrorismhasnoreligion', 'muslimsarenotterorist', 'muslimsarenotterrorist']


pro_sent, anti_sent = extract_sentence()
print(len(pro_sent), len(anti_sent))
# todo sample as many positive sentence as the nb of negative ones (otherwise, takes too long)
sent = pro_sent + anti_sent

# sentence-BERT
sent_enc = [sent_model.encode(s).reshape(1, -1) for s in sent]
cos_sentemb = np.array([ cosine_similarity(sent_enc[i],sent_enc[j]).item() for i in range(len(sent_enc)) for j in range(len(sent_enc))])
# BERT-avg
sent_bertall = [model(**tokenizer(s, return_tensors='pt'))[0].detach() for s in sent]
sent_bertavg = [torch.mean(emb, dim=1) for emb in sent_bertall]
cos_bertavg = np.array([ cosine_similarity(sent_bertavg[i],sent_bertavg[j]).item() for i in range(len(sent_bertavg)) for j in range(len(sent_bertavg))])



n = len(pro_sent)
row_format =" {:>15}" * 4

print(row_format.format("", *["intra-pro", "intra-anti", "inter"]))
for sentenclist, methodName in zip([sent_enc, sent_bertavg],["SentenceBERT", "BERTavg"]):

    sent_enc_pro = sentenclist[:n-1]
    sent_enc_anti = sentenclist[n:]

    cos_sentemb_intrapro = np.array([ cosine_similarity(sent_enc_pro[i],sent_enc_pro[j]).item() for i in range(len(sent_enc_pro)) for j in range(len(sent_enc_pro[i:]))])
    cos_sentemb_intraanti = np.array([ cosine_similarity(sent_enc_anti[i],sent_enc_anti[j]).item() for i in range(len(sent_enc_anti)) for j in range(len(sent_enc_anti[i:]))])
    cos_sentemb_inter = np.array([ cosine_similarity(sent_enc_pro[i],sent_enc_anti[j]).item() for i in range(len(sent_enc_pro)) for j in range(len(sent_enc_anti))])
    print(row_format.format(*[methodName, np.mean(cos_sentemb_intrapro),np.mean(cos_sentemb_intraanti),np.mean(cos_sentemb_inter)]))

