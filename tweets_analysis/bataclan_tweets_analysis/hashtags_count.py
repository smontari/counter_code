import json
import glob
import codecs
from collections import Counter

def countHashtags(lang):
    source_dir = datapath + lang + '_corpus/'
    source_files = glob.glob(source_dir  + '*.raw.json.raw.txt')
    print(source_dir)
    print("nb of files: ", len(source_files))
    counter = 0
    hashts_all = []
    for fname in source_files:
        with open(fname, 'r') as f:
            for tweet in f.readlines():
                counter +=1
                if counter%100000==0:
                    print(counter)
                if not tweet.startswith('RT'):
                    hashts_all.extend(extracthashtag(tweet))
    hasths_count = Counter(hashts_all).most_common()
    with open(objects_path + lang + '_hastags_count.txt', 'w') as f:
        f.write(json.dumps(dict(hasths_count), indent=4))

def extracthashtag(text):
  hasht=[]
  for word in text.split():
    if word[0]=='#':
        h = word[1:].lower().replace('.','').replace(':','')
        if '…' not in h and len(h)>1:
            hasht.append(h)
  return hasht

if __name__ == "__main__":
    languages = ['arabic', 'french', 'english']
    objects_path = "/home/smontari/scratch/objects/bataclan_tweets/"
    datapath = "/home/seddah/scratch/CounteR/CorpusCompBataclan/"

    for lang in languages:
        countHashtags(lang)