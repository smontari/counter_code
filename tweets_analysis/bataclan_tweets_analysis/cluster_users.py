import torch
from sklearn import metrics
import sklearn.cluster
from sklearn.manifold import TSNE
import pandas as pd
import json
import pickle
import numpy as np
#import seaborn as sns
import matplotlib.pyplot as plt
import ipdb
from collections import Counter
from sklearn import decomposition
np.random.seed(5)


def cluster_hasht(hasht, k):
    all_embs = []
    users = []
    users_no_emb = []
    count = 0
    #ipdb.set_trace()
    for user in user_hashtags_embs:
        if user_hashtags_embs[user][hasht].sum() != 0:
            users.append(user)
            all_embs.append(user_hashtags_embs[user][hasht])
        else:
            users_no_emb.append(user)
        count+= count2sents[user][hasht]
    print(hasht, len(all_embs), count)
    km = sklearn.cluster.KMeans(k)
    km.fit(all_embs)
    clusters = list(km.predict(all_embs))
    users.extend(users_no_emb)
    clusters.extend([k]*len(users_no_emb))
    return users, clusters

def get_targets_list(input_path):
    with open(input_path, 'r') as f:
        hasths_count = json.load(f)
    hashts = [k for k,v in hasths_count.items() if v > 200]
    return hashts


def plot_tsne_groundtruth():
    categories_full = [9]*len(node_embs)
    for user, categ in zip(annotated_idxs, categories):
        categories_full[user]=categ
    tsne = TSNE(n_components=2, verbose=1, random_state=123)
    z = tsne.fit_transform(node_embs) 
    df = pd.DataFrame()
    #df["y"] = y
    df["comp-1"] = z[:,0]
    df["comp-2"] = z[:,1]
    catnames = ["neutral neutral", "neutral anti-pass", "neutral pro-pass", "anti-vax neutral", "anti-vax anti-pass", "anti-vax pro-pass", "pro-vax neutral", " pro-vax anti-pass", "pro-vax pro-pass", "unlabelled"]
    formatter = plt.FuncFormatter(lambda val, loc: catnames[val])
    plt.scatter(df["comp-1"] , df["comp-2"], c = categories_full, s=1.0+(np.array(categories_full)!=9)*20, cmap="tab10")
    plt.colorbar(ticks=list(range(10)), format=formatter)
    plt.savefig('tsne_annotated.png')

def extracthashtag(text):
  hasht=[]
  for word in text.split():
    if word[0]=='#':
        h = word[1:].lower().replace('.','').replace(':','')
        if '…' not in h and len(h)>1:
            hasht.append(h)
  return hasht

def countHashtags():
    source_dir = objects_path + 'french_top10_user_tweets_dict_preprocessed.pkl'
    datasets =  pickle.load(open(source_dir, 'rb'))
    counter = 0
    hashts_all = []
    for user in datasets:
        for tweet in datasets[user]:
            counter +=1
            if counter%100000==0:
                print(counter)
            hashts_all.extend(extracthashtag(tweet))
    hasths_count = Counter(hashts_all).most_common()
    with open(objects_path + 'french_hastags_count_reduced_users.txt', 'w') as f:
        f.write(json.dumps(dict(hasths_count), indent=4))

if __name__ == '__main__':

    objects_path = '/home/smontari/scratch/objects/bataclan_tweets/'
    with open(objects_path + 'embeddings/hashtags_bataclan_french.pkl', 'rb') as f:
        user_hashtags_embs, count2sents = pickle.load(f)
    print("number of users: ", len(user_hashtags_embs))

    #countHashtags()

    target_list = get_targets_list(objects_path +  'french_hastags_count.txt')

    user_hashtags_clusters = {}

    for user in user_hashtags_embs:
        user_hashtags_clusters[user] = {}

    for hasht in target_list:
        users, clusters = cluster_hasht(hasht, k=5)
        for u, c in zip(users, clusters):
            user_hashtags_clusters[u][hasht] = c