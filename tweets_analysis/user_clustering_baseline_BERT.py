import torch
from sklearn import metrics
import sklearn.cluster
from sklearn.manifold import TSNE
import pandas as pd
import pickle
from tqdm import tqdm
import numpy as np
#import seaborn as sns
import math
import matplotlib.pyplot as plt
import matplotlib
import ipdb
from collections import Counter
from sklearn import decomposition
from MulticoreTSNE import MulticoreTSNE as TSNE
from yellowbrick.cluster import KElbowVisualizer
from matplotlib.colors import ListedColormap

np.random.seed(5)


def generate_colormap(N):
    arr = np.arange(N)/N
    N_up = int(math.ceil(N/7)*7)
    arr.resize(N_up)
    arr = arr.reshape(7,N_up//7).T.reshape(-1)
    ret = matplotlib.cm.hsv(arr)
    n = ret[:,3].size
    a = n//2
    b = n-a
    for i in range(3):
        ret[0:n//2,i] *= np.arange(0.2,1,0.8/a)
    ret[n//2:,3] *= np.arange(1,0.1,-0.9/b)
    return ret


def extracthashtag(text):
  hasht=[]
  for word in text.split():
    if word[0]=='#':
      hasht.append(word[1:].lower())
  return hasht

def extract_user_hashtags(user_ids_list):
    """
    Get top 10 hastags for each user.
    """
    tweets_file =  '/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/preprocessed/bert_all.csv'
    tweets_df = pd.read_csv(tweets_file)
    user_main_hastags = []
    # load user tweets dict:
    with open(objects_path + 'uid2tweetids.pkl', 'rb') as file:
        uid2tweetids = pickle.load(file)
    for uid in tqdm(user_ids_list):
        tweeds = uid2tweetids[uid]
        user_hastags = []
        # keep only tweets from bert_all file (where the hashtags are already extracted)
        filtered_tweeds = set(int(i) for i in set(int(t) for t in tweeds)&set(list(tweets_df['idx'])))
        for tweetid in filtered_tweeds:
            text = tweets_df[tweets_df['idx']==tweetid]['text'].item()
            hasht_list = extracthashtag(text)
            user_hastags.extend(hasht_list)
        if user_hastags:
            user_main_hastags.append(Counter(user_hastags).most_common(5))
        else:
            user_main_hastags.append([("empty",1)])
    pickle.dump((user_ids_list,user_main_hastags),open(objects_path+'userlist_usertophashtags.pkl','wb'))
    return user_main_hastags

def hashtags_to_categories():
    user_ids_list,user_main_hastags = pickle.load(open(objects_path+'userlist_usertophashtags.pkl','rb'))
    main_hastag=[]
    for counter in user_main_hastags:
        # take the first hastag that is informative (exclude 'passsanitaire', 'covid19','vaccination')
        hasht = counter[0][0]
        if len(counter)>1 and hasht in ['passsanitaire', 'covid19','vaccination']:
            hasht = counter[1][0] 
            if len(counter)>2 and hasht in ['passsanitaire', 'covid19','vaccination']:
                hasht = counter[2][0]
        #if hasht[:5]=='manif':
        #    hasht = hasht[:5]
        #if 'honte' in hasht:
        #    hasht = 'passdelahonte'
        main_hastag.append(hasht)
    # keep only most frequent hashtags
    selected_hashtags = [tag for tag, nb in Counter(main_hastag).most_common(19)]
    main_hastag_filtered = [tag if tag in selected_hashtags else 'other' for tag in main_hastag]
    hasht2id = {}
    for i, hasht in enumerate(set(main_hastag_filtered)):
        hasht2id[hasht] = i

    hashtags_ids = [hasht2id[hasht] for hasht in main_hastag_filtered]
    return main_hastag_filtered, hashtags_ids, hasht2id


def create_user_embeddings():
    # loading BERT embeddings:
    tweet_embs = objects_path + 'emb_ids_list_finetuned_camembert.pkl'
    with open(tweet_embs, 'rb') as file:
        embeddings_tensor, ids_tensor = pickle.load(file)
    #embeddings_tensor = embeddings_tensor.numpy()
    # load user tweets dict:
    with open(objects_path + 'uid2tweetids.pkl', 'rb') as file:
        uid2tweetids = pickle.load(file)
    # careful, in uid2tweetids there are the ids of all tweets (including retweets) but not the id of the retweeted tweet.
    # however in tweet-emb there are only original tweet ids. So here we only use the original tweets of the users.

    # Create user embedding as average of all their tweet embeddings:
    embs_list = []
    uids_reduced = []
    nb_tweets_threshold = 20
    twid2idx = {int(twid):i for i, twid in enumerate(ids_tensor)}
    print(len(uid2tweetids))
    for uid, tweeds in uid2tweetids.items():
        if len(tweeds)> nb_tweets_threshold and len(tweeds)<500:
            twid_indexes = torch.LongTensor(list(filter(None,[twid2idx.get(int(twid),None) for twid in tweeds])))
            all_embs= torch.index_select(embeddings_tensor,0, twid_indexes)
            # keep pnly users with more than 20 unique tweets during the period.
            if len(all_embs)>nb_tweets_threshold:
                uids_reduced.append(uid)
                embs_list.append(all_embs.mean(0))

    print(len(uids_reduced))
    pickle.dump((uids_reduced,embs_list), open(objects_path+'userlist_usertweetemb.pkl', 'wb'))
    # 27071 users have more than 20 tweets in total (including retweets). 3513 have more than 20 unique tweets (the ones we focus on)

def get_best_k(X, save_plot=False):
    km = sklearn.cluster.KMeans()
    visualizer = KElbowVisualizer(km, k=(3,15))
    visualizer.fit(np.array(X)) 
    if save_plot:
        visualizer.show(outpath="kelbow_kmeans_userBERT.png")
    k = visualizer.elbow_value_
    return k

def evaluate_clustering(annotations, clusters):
    annotated_clust, annotated_id = [clust for clust, user in zip(clusters, uids_reduced) if int(user) in list(annotations[0])], [user for clust, user in zip(clusters, uids_reduced) if int(user) in list(annotations[0])]
    annotated_categ = [ann[1][1] for ann in annotations.iterrows() if str(ann[1][0]) in annotated_id]
    print(Counter(annotated_categ))
    h, c, v = metrics.homogeneity_completeness_v_measure(annotated_categ, annotated_clust)
    print(h,c,v)
    # (= precision, recall, f1)
    # v not adequate for only 30 samples with very small categories
    #with random ground truth: 0.14766315882650363, 0.15753621559913356, 0.1524399928323517)

    ri = metrics.adjusted_rand_score(annotated_categ, annotated_clust)
    print(ri)
    # proba of two elemnts of a pair of users (of the same ground truth categ) to be in the same clusters, and conversely
    # (= proba que les assignements soient compatibles)
    return h,c,v,ri




###################
# First method: Clustering of BERT embeddings.
objects_path = '/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/'

#create_user_embeddings()
uids_reduced,embs_list = pickle.load(open(objects_path+'userlist_usertweetemb.pkl', 'rb'))
embs_list = np.array([np.array(emb) for emb in embs_list])

#k = get_best_k(embs_list)
# Rmq: we use elbow method with silhouette score but not elbow at all in the plot.
k=7
km = sklearn.cluster.KMeans(k)
clusters = km.fit_predict(embs_list)

# get annotations
#vacccineopinion_hashtags_users
#annotated_user_category
annotations = pd.read_csv('objects/annotated_user_category.txt', header=None)
# issue with annotated users: only 3 of them are in the set with more than 20 unique tweets. Annotate more !!

# with hashtags-based annotation
hashtags_annotation = pd.read_csv('objects/vacccineopinion_hashtags_users.txt', header=None)
h,c,v,ri = evaluate_clustering(hashtags_annotation, clusters)

# with main hastag for each user:
#main_hashtags_list = extract_user_hashtags(uids_reduced)
hashtag_names, hashtags_ids, hasht2id = hashtags_to_categories()
print(len(set(hashtags_ids)))

tsne = TSNE(n_components=2, verbose=1, random_state=123, n_jobs=4, n_iter=500)
z = tsne.fit_transform(embs_list) 
df = pd.DataFrame()
#df["y"] = y
df["comp-1"] = z[:,0]
df["comp-2"] = z[:,1]
plt.figure(figsize=(9, 5))
scatter = plt.scatter(df["comp-1"] , df["comp-2"], c = hashtags_ids, label = hashtag_names, s=1, cmap=ListedColormap(generate_colormap(len(set(hashtags_ids)))))
lgd = plt.legend(handles=scatter.legend_elements(num=len(set(hashtags_ids)))[0], labels=list(hasht2id.keys()), loc="upper left", bbox_to_anchor=(1.05, 1.0))
plt.tight_layout()

#handles, _ = scatter.legend_elements(prop='colors', num=len(set(hashtags_ids)))
#plt.legend(handles, labels=list(hasht2id.keys()),loc='center left', bbox_to_anchor=(1, 0.5))

plt.savefig('tsne_main_hashtags_BERTonly.png', bbox_extra_artists=(lgd,), bbox_inches='tight')
plt.close()
plt.clf()
# todo put the legend outside the plot




"""
# extracting most frequent hashtag of each cluster
users_idx = [int(i) for i in range(len(clusters))]

user_hastags_df = pd.read_csv('/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/preprocessed/user_main_hashtags.csv')
cluster_hashtag_dict = {}
main_h_list = []
for clust in set(clusters):
    clust_users = [u for u,c in zip(users_idx,clusters) if c==clust]
    clust_hasht = list(user_hastags_df[user_hastags_df['idx'].isin(clust_users)]['hashtag'])
    print(Counter(clust_hasht).most_common(10))
    main_hasht = Counter(clust_hasht).most_common()
    if main_hasht[0][0] in ['empty'] and len(main_hasht)>1:
        main_hasht = main_hasht[1][0]
    else:
        main_hasht = main_hasht[0][0]
    print(clust, len(clust_users), len(clust_hasht),main_hasht)
    main_h_list.append(main_hasht)

"""







# get hashtags
#tweets_file = '/home/smontari/scratch/data/twitter-scraping/antipass/preprocessed/bert_all.csv'
# tweets_file =  '/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/preprocessed/bert_all.csv'
# tweets_df = pd.read_csv(tweets_file)
# uid2tweetids = pickle.load(open('objects/uid2tweetids.pkl','rb'))
#uid2tweetids = pickle.load(open('/home/smontari/scratch/data/twitter-scraping/antipass/preprocessed/uid2tweetids.pkl','rb'))
#categories = hashtags_to_categories()
#pred = clusters[annotated_idxs]
#