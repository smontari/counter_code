

import glob
import sys
import codecs
import random
random.seed(123)
import json
from tqdm import tqdm
import pickle
import collections
import matplotlib.pyplot as plt
import ipdb

# select tweets from most frequent users
path = "/home/smontari/code/tweets-analysis/objects/"
#data_path = "/home/smontari/code/data_processing/tweets-analysis/data/search_jerusalem OR israel OR gaza OR palestin"
data_path = "/data/almanach/user/smontari/scratch/scraped_tweets/antivax/"
def load_json_multiple(segments):
  chunk = ""
  for segment in segments:
    chunk += segment
    try:
      yield json.loads(chunk)
      chunk = ""
    except ValueError:
      pass

def extract_users_count(lang_filter=False):
    user_tweets_count = dict()
    day = []
    lang = []
    hashtags = []
    mentions = []
    media = []
    data_files = glob.glob(data_path + '/*.data')
    print("number of days: ", len(data_files))
    for i in tqdm(range(len(data_files))):
        dfile = data_files[i]
        date = dfile.split('/')[-1].split('.')[0]
        user_tweets_count[date]=dict()
        with codecs.open(dfile, 'r', 'utf-8', errors='ignore') as f:
            for jfile in load_json_multiple(f): 
                if lang_filter:
                    if jfile["language"]!='fr':
                        continue   
                user = jfile["user_id"]
                day.append(date)
                lang.append(jfile["language"])
                for hashtag in jfile["hashtags"]:
                    hashtags.append(hashtag)
                mentions.append(len(jfile["mentions"]))
                media.append(len(jfile["photos"]))
                if user not in user_tweets_count[date]:
                    user_tweets_count[date][user]=1
                else:
                    user_tweets_count[date][user]=user_tweets_count[date][user]+1

    #pickle.dump(user_tweets_count, open(path + "user_tweets_count.pkl", 'wb'))
    return user_tweets_count, day, lang, hashtags, media, mentions



def filter_tweets():
    too_short = 0
    not_enough_letters = 0

    data_files = glob.glob(data_path + '/*.data')
    for i in tqdm(range(len(data_files))):
        dfile = data_files[i]
        with codecs.open(dfile, 'r', 'utf-8', errors='ignore') as f:
            for jfile in load_json_multiple(f):    
                text = jfile["tweet"]
                if len(text)<5:
                    continue
    return None


def get_user_tweets():
    uid_counter = {}
    counter = 0
    with codecs.open(source_file_flat, 'r', 'utf-8', errors='ignore') as f:
        for tweet_dict in load_json_multiple(f):
            counter +=1
            if counter%100000==0:
                print(counter)
            line = tweet_dict["text"]
            tweet_id = str(tweet_dict["id"])
            user_id = str(tweet_dict["author_id"])
            if user_id not in uid_counter:
                    uid_counter[user_id] = 0
            if tweet_id!=None and line[:2]!='RT':
                uid_counter[user_id]+=1

    print(len(uid_counter))
    pickle.dump(uid_counter, open(objects_path + "user_tweets_count_total.pkl", 'wb'))
    return uid_counter


if __name__=="__main__":
    print("start")
    objects_path = '/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/'
    source_dir = "/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/"
    source_file_flat = source_dir + 'all_flat.json'

    #uid_counter = get_user_tweets()
    uid_counter = pickle.load(open(objects_path + "user_tweets_count_total.pkl", 'rb'))
    #plt.hist(list(uid_counter.values()), bins = 10)
    #plt.savefig("nb_original_tweets_per_user.png")
    count = list(uid_counter.values())
    ipdb.set_trace()
    print(len([c for c in count if c>10])/len(c))
    '''
    user_tweets_count, day, lang, hashtags, media, mentions = extract_users_count(True)

    hashtags_reduc = {ht: count for ht, count in collections.Counter(hashtags).items() if count >= 1000}

    print("total nb of tweets: ", len(lang))
    for liste in [lang, hashtags_reduc, media, mentions]:
        print(collections.Counter(liste))
    # 'fr': 90290, 'en': 24813
    # passsanitaire': 79527, 'antivax': 17376, 'antivaxxers': 14510, 'covid19': 12525, 'antipasssanitaire': 11096, 'covid19vaccine': 7557, 'passdelahonte': 7049, 'manifs7aout': 5193, 'manif31juillet': 4856
    # images: 0: 104643, 1: 20534, 2: 1533, 4: 870, 3: 540
    # mentions: 18515 have at least one mention.
    #user_tweets_count = pickle.load(open(path + "user_tweets_count.pkl", 'rb'))

    user_all_count = collections.defaultdict(dict)  
    for date, user_count_dict in user_tweets_count.items():
        for user in user_count_dict:
            user_all_count[user][date]=user_tweets_count[date][user]

    print("All users: ", str(len(user_all_count)))


    # keep only users tweeting at least 15 days out of 20
    users_reduc = []
    for user in user_all_count:
        nb_days = len(user_all_count[user])
        if nb_days>=10:
            users_reduc.append(user)

    print("remaining users: ", str(len(users_reduc)))

    # keep only top 100 users from this list
    users_reduc2 = []
    for user in users_reduc:
        nb_tweets = sum(list(user_all_count[user].values()))
        if nb_tweets>=100:
            users_reduc2.append(user)

    print("remaining users: ", str(len(users_reduc2)))

    with open(path + 'antivax_frequent_users_list_fr.txt', 'w+') as f:
        for item in users_reduc2:
            f.write("%s\n" % item)
    '''