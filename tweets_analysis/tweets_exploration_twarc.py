from nltk.tokenize import TweetTokenizer
from emoji import demojize
import re
import random
import json
import codecs

"""
Here, we want to know what proportion of tweet is discarded during preliminary filetring and why.
"""


source_dir = "/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/"
source_file = source_dir + 'all.json'
lang_select = 'fr'


def load_json_multiple(segments):
  chunk = ""
  for segment in segments:
    chunk += segment
    try:
      yield json.loads(chunk)
      chunk = ""
    except ValueError:
      pass


tokenizer = TweetTokenizer()

def normalizeToken(token):
    lowercased_token = token.lower()
    if token.startswith("@"):
        return "@USER"
    elif lowercased_token.startswith("http") or lowercased_token.startswith("www"):
        return "HTTPURL"
    elif len(token) == 1:
        return demojize(token)
    else:
        if token == "’":
            return "'"
        elif token == "…":
            return "..."
        else:
            return token

def normalizeTweet(tokens):
    normTweet = " ".join([normalizeToken(token) for token in tokens])    
    normTweet = re.sub(r",([0-9]{2,4}) , ([0-9]{2,4})", r",\1,\2", normTweet)
    normTweet = re.sub(r"([0-9]{1,3}) / ([0-9]{2,4})", r"\1/\2", normTweet)
    normTweet = re.sub(r"([0-9]{1,3})- ([0-9]{2,4})", r"\1-\2", normTweet)
    
    return " ".join(normTweet.split())

if __name__ == "__main__":
  counter = 0
  too_small = 0
  wrong_lang = 0
  RT = 0
  valid_tweet = 0
  
  print("starting")

  with codecs.open(source_file, 'r', 'utf-8', errors='ignore') as f:
    for jfile in load_json_multiple(f):
        for tweet_dict in jfile['data']:
            counter +=1
            if counter%50000==0:
                print('all tweets: ', counter)
                print('wrong_lang: ', wrong_lang, wrong_lang*100/counter)
                print('retweets: ', RT, RT*100/counter, (RT+wrong_lang)*100/counter)
                print('too_small: ', too_small, too_small*100/counter, (too_small + RT+wrong_lang)*100/counter)
                print('valid tweets: ', valid_tweet, valid_tweet*100/counter)
            line = tweet_dict["text"]
            tokens = tokenizer.tokenize(line.replace("’", "'").replace("…", "..."))
            if tweet_dict["lang"] == lang_select:
                if tokens[0]!='RT':
                    if 10<=len(tokens)<=64:
                        valid_tweet +=1
                    else:
                        too_small+=1
                else: 
                    RT +=1
            else:
                wrong_lang +=1

print('all tweets: ', counter)
print('wrong_lang: ', wrong_lang)
print('retweets: ', RT)
print('too_small: ', too_small)
print('valid tweets: ', valid_tweet)