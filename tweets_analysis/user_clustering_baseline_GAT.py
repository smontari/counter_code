import torch
from sklearn import metrics
import sklearn.cluster
from sklearn.manifold import TSNE
import pandas as pd
import pickle
import codecs
import numpy as np
import ipdb
import json
from collections import Counter
from sklearn import decomposition
from torch_geometric.data import Data


np.random.seed(5)


def load_json_multiple(segments):
  chunk = ""
  for segment in segments:
    chunk += segment
    try:
      yield json.loads(chunk)
      chunk = ""
    except ValueError:
      pass

def create_user_list():
  # make a first run through the dataset to get a list of users and their tweets, but only the original ones !! No RT.
  print("Creating user lists")
  uid2tweetids = {}
  counter = 0
  with codecs.open(source_file_flat, 'r', 'utf-8', errors='ignore') as f:
      for tweet_dict in load_json_multiple(f):
        counter +=1
        if counter%100000==0:
            print(counter)
        line = tweet_dict["text"]
        tweet_id = str(tweet_dict["id"])
        user_id = str(tweet_dict["author_id"])
        if tweet_id!=None and line[:2]!='RT':
            if user_id not in uid2tweetids:
                uid2tweetids[user_id] = []
            uid2tweetids[user_id].append(tweet_id)
  return uid2tweetids
  
def collect_tweets_flattened(uid2tweetids):
  """
  Create a dictionary of user ids and their interaction with associated tweet text.
  """
  print("Loading tweets")
  static_graph_dict = {}
  counter = 0
  with codecs.open(source_file_flat, 'r', 'utf-8', errors='ignore') as f:
    for tweet_dict in load_json_multiple(f):
        destination_ids = []
        destination_texts = []
        counter +=1
        if counter%50000==0:
            print("Processed {} tweets.".format(counter))
            print(len(static_graph_dict))
            #pickle.dump(tweets_dict, open("/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/static_graph_for_gpy.pkl", 'wb'))
        source_id = str(tweet_dict["author_id"])
        if source_id not in static_graph_dict:
            static_graph_dict[source_id]={}
        text = tweet_dict["text"]
        # get mentioned users (but not the one from retweets, that are handled in the next step)
        mentions_list = tweet_dict.get('entities',{}).get('mentions',[])
        if mentions_list and not text[:2]=='RT':
            destination_ids.extend([m['id'] for m in mentions_list])
            destination_texts.extend(len(mentions_list)*[text])
        # get referenced tweets
        for reftwdict in tweet_dict.get('referenced_tweets',[]):
            if 'author_id' in reftwdict: # if not, it is probably a reply_to and the intercation is a mention.
                destination_ids.append(reftwdict['author_id'])
                if reftwdict['type']=='retweeted':
                    destination_texts.append(reftwdict['text'])
                else:
                    destination_texts.append(text)

        # keep only interactions between people inside the graph (the ones who actually tweet, not only RT)
        destination_ids_filtered = [uid for uid in destination_ids if uid in uid2tweetids]
        destination_texts_filtered =  [txt for txt,uid in zip(destination_texts, destination_ids) if uid in uid2tweetids]
        for uid, text in zip(destination_ids_filtered, destination_texts_filtered):
            if uid not in static_graph_dict[source_id]:
                static_graph_dict[source_id][uid] = [text]
            else:
                static_graph_dict[source_id][uid].append(text)

    pickle.dump(static_graph_dict, open(source_dir + 'objects/static_graph_dict_text.pkl', 'wb'))


def process_graph_dict_for_pygeom(static_dict):
    """
    Only count the number of interactions (directed) as feature.
    Output :
    data.x: Node feature matrix with shape [num-nodes, num-node-features]
    Here, we can either use no node feature, only basic metadata, or BERT embeddings of all unique tweets of the user.
    data.edge_index: Graph connectivity in COO format with shape [2, num-edges] and type torch.long
    This is the list of source nodes and the list of target nodes (not a list of index tuples)
    data.edge_attr: Edge feature matrix with shape [num-edges, num-edge-features]
    Edge feature could be simply the number of quote, mentions and retweets; or the average BERT embedding of mention and quote tweet.
    """
    source_ids = []
    dest_ids = []
    edge_attr = []
    n = len(set(source_ids+dest_ids))

    # todo define data.num_nodes

    for sourceid, dest_dict in static_dict.items():
        source_ids.extend([int(sourceid)]*len(dest_dict))
        dest_ids.extend([int(uid) for uid in dest_dict.keys()])
        edge_attr.extend([len(v) for v in dest_dict.values()])

    n = len(set(source_ids+dest_ids))
    edge_index = torch.tensor([source_ids, dest_ids], dtype=torch.long)
    edge_attr = torch.tensor([edge_attr], dtype=torch.float)

    graph = Data(edge_attr=edge_attr, edge_index=edge_index, num_nodes=n)
    return graph


source_dir = "/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/"
source_file_flat = source_dir + 'all_flat.json'

#uid2tweetids = create_user_list()
#pickle.dump(uid2tweetids, open(source_dir + 'objects/uid2tweetids_noRT.pkl', 'wb'))
#uid2tweetids = pickle.load(open(source_dir + 'objects/uid2tweetids_noRT.pkl', 'rb'))
#collect_tweets_flattened(uid2tweetids)
static_graph_dict = pickle.load(open(source_dir + 'objects/static_graph_dict_text.pkl', 'rb'))
graph = process_graph_dict_for_pygeom(static_graph_dict)
ipdb.set_trace()
print(graph.num_nodes)


"""
edge_index = torch.tensor([[0, 1, 1, 2],
                           [1, 0, 2, 1]], dtype=torch.long)
x = torch.tensor([[-1], [0], [1]], dtype=torch.float)

data = Data(x=x, edge_index=edge_index)
"""