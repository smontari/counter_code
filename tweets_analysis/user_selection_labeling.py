import random
import numpy as np
import os
import pandas as pd
import codecs
import pickle
from collections import Counter
import json
from tqdm import tqdm
import ipdb

"""
We want to select the most prominent users to annotate them.
We choose them among the preselected set from small_df.
"""

def load_json_multiple(segments):
  chunk = ""
  for segment in segments:
    chunk += segment
    try:
      yield json.loads(chunk)
      chunk = ""
    except ValueError:
      pass

def select_users():
    print("Selecting users")
    uid2tweetids = {}
    counter = 0
    with codecs.open(source_file_flat, 'r', 'utf-8', errors='ignore') as f:
        for tweet_dict in load_json_multiple(f):
            ipdb.set_trace()
            counter +=1
            if counter%100000==0:
                print(counter)
            line = tweet_dict["text"]
            tweet_id = str(tweet_dict["id"])
            user_id = str(tweet_dict["author_id"])
            if tweet_id!=None and line[:2]!='RT':
                if user_id not in uid2tweetids:
                    uid2tweetids[user_id] = []
                uid2tweetids[user_id].append((tweet_id, line))

    print(len(uid2tweetids))
    pickle.dump(uid2tweetids, open(objects_path+'user_originaltweets_dict.pkl', 'wb'))
    # 3513 have more than 20 unique tweets (the ones we focus on)

def extracthashtag(text):
  hasht=[]
  for word in text.split():
    if word[0]=='#':
      hasht.append(word[1:].lower().rstrip(",."))
  return hasht

def extract_user_hashtags(uid2tweetids, threshold=10, nbUsersPerTag=5):
    """
    Get top hastags for each user.
    """
    uid2tweetids_reduc = {uid: tw for (uid, tw) in uid2tweetids.items() if len(tw) >= threshold }
    print("filtered users: ", len(uid2tweetids_reduc))

    user_main_hastag = []
    for uid, tweets in tqdm(uid2tweetids_reduc.items()):
        user_hastags = []
        # get list of all hasthtags in its original tweets
        hasht_list = [extracthashtag(text) for twid, text in tweets]
        user_hastags =  [h for sublist in hasht_list for h in sublist]
        if user_hastags:
            counter = Counter(user_hastags).most_common(nbUsersPerTag)
            # take the first hastag that is informative (exclude 'passsanitaire', 'covid19','vaccination')
            hasht = counter[0][0]
            if len(counter)>1 and hasht in ['passsanitaire', 'covid19','vaccination', 'vaccin']:
                hasht = counter[1][0] 
                if len(counter)>2 and hasht in ['passsanitaire', 'covid19','vaccination','vaccin']:
                    hasht = counter[2][0]
            # merge similar hashtags
            if hasht[:5]=='manif':
                hasht = hasht[:5]
            if 'honte' in hasht:
                hasht = 'passdelahonte'
            if 'vaccinezvous' in hasht:
                hasht = 'vaccinezvous'
            if ('antipass' in hasht) or ('nonaupass' in hasht):
                hasht = 'antipass'
            if hasht in ['vaccinobligatoire', 'vaccinationobligatoire']:
                hasht = 'antipass'
            if 'antiva' in hasht:
                hasht = 'antivax'
        else:
            hasht = "empty"
        user_main_hastag.append(hasht)

    # keep only most frequent hashtags
    selected_hashtags = [tag for tag, nb in Counter(user_main_hastag).most_common(7)]
    print(Counter(user_main_hastag).most_common(7))
    main_hastag_filtered = [tag for tag in user_main_hastag if tag in selected_hashtags ]
    users_filtered = [usr  for usr, tag in zip(uid2tweetids,user_main_hastag) if tag in selected_hashtags]
    return users_filtered, main_hastag_filtered

def compute_user_popularity():
    static_graph_dict = pickle.load(open(source_dir + 'objects/static_graph_dict_text.pkl', 'rb'))
    # compute popularity level (number of intercation where the user is the destination)
    dest_count_dict = {}
    for source, dest_dict in tqdm(static_graph_dict.items()):
        for dest, tweets in dest_dict.items():
            if dest not in dest_count_dict:
                dest_count_dict[dest]=len(tweets)
            else:
                dest_count_dict[dest]+=len(tweets)
    pickle.dump(dest_count_dict, open(objects_path+'user_popularity_static.pkl', 'wb'))

def filter_users(users_filtered, main_hastag_filtered):
    # select users that are the destination of the most interactions
    dest_count_dict = pickle.load(open(source_dir + 'objects/user_popularity_static.pkl', 'rb'))
    selected_users = []
    for tag in set(main_hastag_filtered):
        tag_users = [usr for usr, t in zip(users_filtered,main_hastag_filtered) if t ==tag ]
        tag_user_popularity = {uid: cnt for (uid, cnt) in dest_count_dict.items() if uid in tag_users}
        top_tag_users = sorted(tag_user_popularity.items(), key=lambda x: x[1], reverse=True)
        selected_users.extend(top_tag_users[:5])
    select_users = [usr for usr, nb in selected_users]
    print(len(set(select_users)))
    return select_users


def create_annotation_dict_user(uid2tweetids, selected_users, nbtweets=10):
    """
    annotate the selected users: 5 most popular users for each of the 7 main hastags.
    """
    print("getting tweet text...")
    uid2tweetids_selected = {uid: tw for (uid, tw) in uid2tweetids.items() if uid in selected_users}
    users_tweets_to_annotate_dict = {}    
    for uid, tweets in uid2tweetids_selected.items():
        texts = [line for tweet_id, line in tweets]
        selected_tweets = random.sample(texts, nbtweets)
        users_tweets_to_annotate_dict[uid]=selected_tweets
    pickle.dump(users_tweets_to_annotate_dict, open(objects_path+'users_tweets_to_annotate_dict.pkl', 'wb'))

def annotate_user(users_tweets_to_annotate_dict):
    nb_done = 0
    if os.path.isfile(path + 'objects/annotations_user.txt'):
        with open(path + 'objects/annotations_user.txt', 'r') as f:
            nb_done = len(f.readlines())
            print("already annotated: ", nb_done)
    print("left to annotate: ", len(users_tweets_to_annotate_dict)-nb_done)
        

    print("""
    Annotation guidelines: 
     0: neutral or do not know
     1: anti pass 
     2: pro pass
     1: anti vax 
     2: pro vax
    """)
    for uid, tweets in list(users_tweets_to_annotate_dict.items())[nb_done:]:
        print('\n')
        print("############## New user: ", uid, " #################")
        print('\n'.join(tweets))
        annotation_pass = input("Anti (1) or Pro (2) pass (neutral or do not know: 0) ? > ") 
        annotation_vax = input("Anti (1) or Pro (2) vax (neutral or do not know: 0) ? > ") 

        with open(path + 'objects/annotations_user.txt', 'a+') as f:
            print(uid, ', ' ,annotation_pass,', ' ,annotation_vax, file = f)



def create_annotation_dataset():
    min_inter = 100
    inward_users = Counter(graph_df['i'])
    outward_users = Counter(graph_df['u'])

    valid_inward = [k for k,v in inward_users.items() if min_inter<v]
    valid_outward = [k for k,v in outward_users.items() if min_inter<v]
    selected_users = list(set(valid_inward+valid_outward))
    
    idx = np.where((graph_df['u'].isin(selected_users)) & (graph_df['i'].isin(selected_users)))
    small_df = graph_df.loc[idx]
    print("new number of users: ", len(set(list(small_df['u']) + list(small_df['i']))))

    # create small_df with actual user ids
    user_ids = [idx2uid[idx] for idx in small_df['u']]
    item_ids = [idx2uid[idx] for idx in small_df['i']]

    small_df_new = pd.DataFrame({'u': user_ids,
                    'i': item_ids,
                    'ts': small_df['ts'],
                    'label': small_df['label'],
                    'feature': small_df['feature']})

    # select top tweeting individual (user_ids) 
    n = 5000
    outward_users = Counter(small_df_new['u'])
    top_out = [id for id, _ in outward_users.most_common(n)]
    #sample n_users from them
    n_users = 30
    sampled_users = random.sample(top_out, n_users)
    # get their tweets ids, sample 30 of them
    nb_sample = 20
    user_dfs = []
    for user in sampled_users:
        sampled_df = small_df_new[small_df_new['u']==user].sample(n=nb_sample, random_state=1) 
        user_dfs.append(sampled_df)
    ids_to_annotate = pd.concat(user_dfs)

    # tweet id to text: use preprocessed list
    print("getting tweet text...")
    all_selected_tweets_df = pd.read_csv(source_dir + 'preprocessed/bert_all.csv', sep=',')
    selected_ids = set(all_selected_tweets_df['idx'])

    tweet_text = []
    for tweetid in ids_to_annotate['feature']:
        tweet_text.append(all_selected_tweets_df.loc[all_selected_tweets_df.idx==tweetid]['text'].item())

    ids_to_annotate['text'] = tweet_text
    ids_to_annotate = ids_to_annotate.sample(frac = 1)
    ids_to_annotate.to_csv(path + 'objects/tweets_to_annotate.csv')


def annotate():
    ids_to_annotate = pd.read_csv(path + 'objects/tweets_to_annotate.csv')
    nb_done = 0
    if os.path.isfile(path + 'objects/annotations.txt'):
        with open(path + 'objects/annotations.txt', 'r') as f:
            nb_done = len(f.readlines())
            print("already annotated: ", nb_done)
    print("left to annotate: ", len(ids_to_annotate)-nb_done)
        

    print("""
    Annotation guidelines: 
     0: neutral or do not know
     1: anti pass 
     2: pro pass
     1: anti vax 
     2: pro vax
    """)
    for tweetid, text in zip(list(ids_to_annotate['feature'])[nb_done:], list(ids_to_annotate['text'])[nb_done:]):
        print(text)
        annotation_pass = input("Anti (1) or Pro (2) pass (neutral or do not know: 0) ? > ") 
        annotation_vax = input("Anti (1) or Pro (2) vax (neutral or do not know: 0) ? > ") 

        with open(path + 'objects/annotations.txt', 'a+') as f:
            print(tweetid, ', ' ,annotation_pass,', ' ,annotation_vax, file = f)


def process_annotations():
    # load annotations:
    min_nb_annotated_tweets = 1
    users_ok = []
    category_ok = []

    annotations = pd.read_csv(path + 'objects/annotations.txt', header=None)
    annotated_tweets = list(annotations[0])
    ids_to_annotate = pd.read_csv(path + 'objects/tweets_to_annotate.csv')
    users_list = set(ids_to_annotate['u'])
    for user in users_list:
        user_opinion=None
        user_tweets = ids_to_annotate[ids_to_annotate['u']==user]['feature']
        user_annotated_tweets = set(user_tweets)&set(annotated_tweets)
        if len(user_annotated_tweets)>=min_nb_annotated_tweets:

            # Filter zeros and take the majority between 1 and 2. Maybe use sorted instead ? TODO
            vax_opinion = Counter(annotations[annotations[0].isin(user_annotated_tweets)][2]).most_common(2)
            pass_opinion = Counter(annotations[annotations[0].isin(user_annotated_tweets)][1]).most_common(2)
            # if the most common is neutral, and there is a second opinion, pick the second one.
            if int(vax_opinion[0][0]) == 0 and len(vax_opinion)>1:
                vax_opinion = vax_opinion[1][0]
            else:
                vax_opinion = vax_opinion[0][0]
            if int(pass_opinion[0][0]) == 0 and len(pass_opinion)>1:
                pass_opinion = pass_opinion[1][0]
            else:
                pass_opinion = pass_opinion[0][0]

            user_opinion = int(vax_opinion)*3 + int(pass_opinion)
            users_ok.append(user)
            category_ok.append(user_opinion)

    with open(path+'objects/annotated_user_category.txt','w') as f:
        for u, categ in zip (users_ok, category_ok):
            print(u, ',', categ, file=f)

path = '/home/smontari/code/tweets-analysis/code/analysis/'
source_dir = "/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/"
source_file = source_dir + 'all.json'
source_file_flat = source_dir + 'all_flat.json'
objects_path = '/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/'


#uid2tweetids = pickle.load(open(path + 'objects/uid2tweetids.pkl', 'rb'))
uid2idx = pickle.load(open(path + 'objects/uid2idx.pkl', 'rb'))
#graph_df = pd.read_csv(path + 'objects/twitter_twarc.csv')

#idx2uid = {v:k for k,v in uid2idx.items()}
#user_ids = [idx2uid[idx] for idx in graph_df['u']]
#item_ids = [idx2uid[idx] for idx in graph_df['i']]


if __name__ == "__main__":  
    print("starting")
    select_users()
    #create_annotation_dataset()

    #annotate()
    #resume_annotation_pipeline()

    # new roadmap
    # Preparation steps: filter users who tweet a lot, extract their most frequent hashtags, select a few users to annotate for each hashtag.
    #select_users()
    #uid2tweetids = pickle.load(open(objects_path+'user_originaltweets_dict.pkl', 'rb'))
    #users_filtered, main_hastag_filtered = extract_user_hashtags(uid2tweetids, threshold=10, nbUsersPerTag=5)
    #compute_user_popularity()
    #selected_users = filter_users(users_filtered, main_hastag_filtered)
    #create_annotation_dict_user(uid2tweetids, selected_users, nbtweets=10)
    users_tweets_to_annotate_dict=pickle.load(open(objects_path+'users_tweets_to_annotate_dict.pkl', 'rb'))
    print("starting annotation")
    annotate_user(users_tweets_to_annotate_dict)


    """
    user_hastags_df=pd.read_csv('/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/preprocessed/user_main_hashtags.csv')
    print(Counter(user_hastags_df['hashtag']).most_common(50))
    pro = list(user_hastags_df[user_hastags_df['hashtag'].isin(['vaccinezvousbordel'])]['twuid'])
    #'vaccinezvous',
    anti = list(user_hastags_df[user_hastags_df['hashtag'].isin(['nonauvaccinobligatoire'])]['twuid'])
    print(len(pro), len(anti))
    with open(path+'objects/vacccineopinion_hashtags_users.txt','w') as f:
        for u in pro:
            print(u, ',', '0', file=f)
        for u in anti:
            print(u, ',', '1', file=f)
    """

