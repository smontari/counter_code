import os
import sys
import re
import glob
import json
import codecs
import time
from datetime import datetime
import pickle
import numpy as np
import pandas as pd
from pprint import pprint
import pickle
#import ipdb
import requests
import shutil
from urllib.parse import urlsplit

def load_json_multiple(segments):
  chunk = ""
  for segment in segments:
    chunk += segment
    try:
      yield json.loads(chunk)
      chunk = ""
    except ValueError:
      pass


def get_urls(status):
    """
    Returns a list of URLs found in the tweet
    """
    urls = []
    # everything is in 'entities'
    if "entities" in status:
        entities = status["entities"]
        # get all urls
        for item in entities.get("urls", []):
            ent_urls = [item['expanded_url'] for item in entities["urls"] if "expanded_url" in item]
            urls.extend(ent_urls)
            # sometimes urls have associated images, get this image too.
            for field in ["media", "images"]:
                im_urls = [im['url'] for im in item.get(field, []) if 'url' in im]
                urls.extend(im_urls)
        #if some entities are images and media, get them
        for field in ["media", "images"]:
            im_urls = [item['url'] for item in entities.get(field, []) if 'url' in item]
            urls.extend(im_urls)

    return urls

    

def collect_tweets_raw():
  """
  We create a csv file with the following columns: user_id,item_id,timestamp,state_label,comma_separated_list_of_features
  """
  # second run: create the graph, keeping the interactions only when the mentioned user is in the graph
  # AND when the corresponding tweet is in the selected list (tweet whose embedding is extracted)
  print("Loading tweets")
  tweets_dict = {}
  counter = 0
  with codecs.open(source_file, 'r', 'utf-8', errors='ignore') as f:
    for jfile in load_json_multiple(f):
      for tweet_dict in jfile['data']:
        counter +=1
        if counter%100000==0:
            print(counter)
            pickle.dump(tweets_dict, open("/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/tweet_dict_noRT_urls.pkl", 'wb'))
        twid = str(tweet_dict["id"])
        text = tweet_dict["text"]

        if text[:2]=='RT':
            id2 = [referenced['id'] for referenced in tweet_dict.get('referenced_tweets',[]) if referenced['type'] == 'retweeted'][0]
            reftweetdict = [twdict for twdict in jfile['includes']['tweets'] if twdict['id']==id2][0]
            text = reftweetdict["text"]

        # get url of all medias
        urls = get_urls(tweet_dict)
        if 'attachments' in tweet_dict:
            media_keys = tweet_dict['attachments'].get('media_keys',[])
            for mk in media_keys:
                media_url = [media_dict.get('url',media_dict.get('preview_image_url', []))  for media_dict in jfile['includes']['media'] if media_dict['media_key']==mk][0]
                urls.append(media_url)


        tweets_dict[twid]={}
        tweets_dict[twid]["author_id"] = str(tweet_dict["author_id"])
        tweets_dict[twid]["created_at"] = tweet_dict["created_at"]#datetime.fromisoformat(tweet_dict["created_at"][:-1])
        tweets_dict[twid]["text"] = text
        if urls:
            tweets_dict[twid]["urls"] = urls
            tweets_dict[twid]["images"] = []            
            for url in urls:
                m = re.search("^https:\/\/pbs\.twimg\.com\/media\/(.+)$", url)
                if m is not None:
                    filename = m.group(1)
                    save_path = os.path.join(pictures_dir, filename)
                    tweets_dict[twid]["images"].append(save_path)
                    if not os.path.exists(save_path):
                        response = requests.get(url, stream=True)
                        with open(save_path, 'wb') as out_file:
                            shutil.copyfileobj(response.raw, out_file)
                        del response
    return tweets_dict


def collect_tweets_flattened():
  """
  Create a dictionary of tweet ids and associated image (url + local path)
  """
  print("Loading tweets")
  tweets_dict = {}
  counter = 0
  nb_images = 0
  with codecs.open(source_file_flat, 'r', 'utf-8', errors='ignore') as f:
    for tweet_dict in load_json_multiple(f):
        tweet_media_dicts = []
        counter +=1
        if counter%50000==0:
            print("Processed {} tweets with {} media.".format(counter, nb_images))
            pickle.dump(tweets_dict, open("/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/twid_dict_mediaonly_path_url.pkl", 'wb'))
        twid = str(tweet_dict["id"])
        # get tweet attached media
        if 'attachments' in tweet_dict:
            tweet_media_dicts.extend(tweet_dict['attachments'].get('media',[]))
        # get media from referenced tweets too
        for reftwdict in tweet_dict.get('referenced_tweets',[]):
            if 'attachments' in reftwdict:
                tweet_media_dicts.extend(reftwdict['attachments'].get('media',[]))

        # get url of all medias
        media_urls = filter(None, [media_dict.get('url',media_dict.get('preview_image_url', []))  for media_dict in tweet_media_dicts])
        media_urls = list(set(media_urls))

        tweets_dict[twid]=[]
        if media_urls:
            for url in media_urls:
                filename = None
                m = re.search("^https:\/\/pbs\.twimg\.com\/media\/(.+)$", url)
                if m is not None:
                    filename = m.group(1)
                else:
                    m = re.search("^https:\/\/pbs\.twimg\.com\/ext_tw_video_thumb\/(.+)\/pu\/img\/(.+)$", url)
                    if m is not None:
                        filename = m.group(2)
                if filename:
                    save_path = os.path.join(pictures_dir, filename)
                    tweets_dict[twid].append((url,save_path))
                    nb_images +=1
                    if not os.path.exists(save_path):
                        response = requests.get(url, stream=True)
                        with open(save_path, 'wb') as out_file:
                            shutil.copyfileobj(response.raw, out_file)
                        del response
    return tweets_dict, nb_images, counter
    


path = '/home/smontari/code/tweets-analysis/code/analysis/'
source_dir = "/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/"
source_file = source_dir + 'all.json'
source_file_flat = source_dir + 'all_flat.json'

pictures_dir = "/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/media"
if not os.path.exists(pictures_dir):
    os.makedirs(pictures_dir)

#tweets_dict = collect_tweets()

tweets_dict, nb_images, counter = collect_tweets_flattened()
pickle.dump(tweets_dict, open("/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/twid_dict_mediaonly_path_url.pkl", 'wb'))

print("Processed {} tweets with {} media in total.".format(counter, nb_images))
print("Check that this many images are in /data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/twid_dict_mediaonly_path_url.pkl.")



#all_selected_tweets_df = pd.read_csv(source_dir + 'preprocessed/bert_all.csv', sep=',')
#selected_ids = set(all_selected_tweets_df['idx'])


