# works with python <=2.7
from nltk.tokenize import TweetTokenizer
from emoji import demojize
import re
import random
import json
import codecs

source_dir = "/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/"
source_file = source_dir + 'all.json'
objects_path = "/home/smontari/code/tweets-analysis/objects/"
test_proportion = 0.2
lang_select = 'fr'


def load_json_multiple(segments):
  chunk = ""
  for segment in segments:
    chunk += segment
    try:
      yield json.loads(chunk)
      chunk = ""
    except ValueError:
      pass


tokenizer = TweetTokenizer()

def normalizeToken(token):
    lowercased_token = token.lower()
    if token.startswith("@"):
        return "@USER"
    elif lowercased_token.startswith("http") or lowercased_token.startswith("www"):
        return "HTTPURL"
    elif len(token) == 1:
        return demojize(token)
    else:
        if token == "’":
            return "'"
        elif token == "…":
            return "..."
        else:
            return token

def testTweet(tokens):
    test = False
    if 10<=len(tokens)<=64 and tokens[0]!='RT':
        test = True
    return test

def normalizeTweet(tokens):
    normTweet = " ".join([normalizeToken(token) for token in tokens])    
    normTweet = re.sub(r",([0-9]{2,4}) , ([0-9]{2,4})", r",\1,\2", normTweet)
    normTweet = re.sub(r"([0-9]{1,3}) / ([0-9]{2,4})", r"\1/\2", normTweet)
    normTweet = re.sub(r"([0-9]{1,3})- ([0-9]{2,4})", r"\1-\2", normTweet)
    
    return " ".join(normTweet.split())

if __name__ == "__main__":
  counter = 0
  tweets_file = source_dir+  "preprocessed/bert_all.csv"

  w_file = codecs.open(tweets_file, 'w', 'utf-8')
  w_file.write('%s\t%s\n'%('idx', 'text'))
  # todo encoding
  with codecs.open(source_file, 'r', 'utf-8', errors='ignore') as f:
    for jfile in load_json_multiple(f):
        for tweet_dict in jfile['data']:
            counter +=1
            if counter%10000==0:
                print(counter)
            line = tweet_dict["text"]
            tokens = tokenizer.tokenize(line.replace("’", "'").replace("…", "..."))
            tweet_id = str(tweet_dict["id"])
            if tweet_dict["lang"] == lang_select and tweet_id!=None and line!=None and testTweet(tokens):
                text = normalizeTweet(tokens)
                w_file.write('%s\t%s\n'%(tweet_id, text))