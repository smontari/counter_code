
import glob
import sys
import random
random.seed(123)
import json
from tqdm import tqdm
import pickle
import collections
from pprint import pprint
import nltk
#nltk.download('wordnet')
from nltk.stem.wordnet import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()

from gensim.models import LdaModel, LdaMulticore
from gensim.models import Phrases
from gensim.corpora import Dictionary
from gensim.test.utils import datapath

# issue: BOW topic model such as LDA does not work for multilingual data. 
# keep only english tweets for now
# next step: use multilingual topic model.

source_dir = "/home/smontari/code/data_processing/tweets-analysis/data/preprocessed_israel/"
data_files = glob.glob(source_dir + '/*.data')

tweets = []

for i in tqdm(range(len(data_files))):
    dfile = data_files[i]
    with open(dfile, 'r') as f:
        tweets_list = f.readlines()
    for elem in tweets_list:
        # Split into words
        text = elem.split('\t')[1].split()
        # Remove words that are only one character and lemmatize
        text = [lemmatizer.lemmatize(token) for token in text if len(token) > 1]
        if len(text)>1:
            tweets.append(text)

# Add bigrams and trigrams to docs (only ones that appear 20 times or more).
bigram = Phrases(tweets, min_count=20)
for idx in range(len(tweets)):
    for token in bigram[tweets[idx]]:
        if '_' in token:
            # Token is a bigram, add to document.
            tweets[idx].append(token)

# Remove rare and common tokens.
dictionary = Dictionary(tweets)
# Filter out words that occur less than 20 documents, or more than 50% of the documents.
dictionary.filter_extremes(no_below=20, no_above=0.5)
# Bag-of-words representation of the documents.
corpus = [dictionary.doc2bow(tw) for tw in tweets]

print('Number of unique tokens: %d' % len(dictionary))
print('Number of tweets: %d' % len(corpus))

pickle.dump(corpus, open("isreal_corpus_processed_for_gensim_lda.pkl", 'wb'))

print("start training")
# Set training parameters.
num_topics = 10
chunksize = 2000
passes = 20
iterations = 400
eval_every = None  # Don't evaluate model perplexity, takes too much time.

# Make a index to word dictionary.
temp = dictionary[0]  # This is only to "load" the dictionary.
id2word = dictionary.id2token

model = LdaModel(
    corpus=corpus,
    id2word=id2word,
    chunksize=chunksize,
    alpha='auto',
    eta='auto',
    iterations=iterations,
    num_topics=num_topics,
    passes=passes,
    eval_every=eval_every,
    #workers=5
)

top_topics = model.top_topics(corpus) #, num_words=20)

# Average topic coherence is the sum of topic coherences of all topics, divided by the number of topics.
avg_topic_coherence = sum([t[1] for t in top_topics]) / num_topics
print('Average topic coherence: %.4f.' % avg_topic_coherence)

pprint(top_topics)


# Save model to disk.
model.save("lda_israel.gensim_model")