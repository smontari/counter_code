# works with python <=2.7

import nltk
from nltk.tokenize import word_tokenize
import glob
import sys
import codecs
import twokenize
from mosestokenizer import *
moses = MosesTokenizer(lang='fr')
import random
random.seed(123)
import itertools
import json
from tqdm import tqdm
import pickle
import glob
import os
from emoji import UNICODE_EMOJI

#source_dir = "/home/smontari/code/data_processing/tweets-analysis/data/search_jerusalem OR israel OR gaza OR palestin"
source_dir = "/data/almanach/user/smontari/scratch/scraped_tweets/antivax"
source_files = glob.glob(source_dir + '/*.data')
target_dir = "/data/almanach/user/smontari/scratch/scraped_tweets/preprocessed_antivax/"
objects_path = "/home/smontari/code/tweets-analysis/objects/"
user_filtering = True
english_only = True
if user_filtering:
    with open(objects_path + 'antivax_frequent_users_list_fr.txt', 'r') as f: #israel_frequent_users_list.txt
        users_list = f.readlines()

def checkSpecial(word):
  for i in range(len(word)):
    if word[i].isdigit():
      return True
    if isAlpha(word[i]):
      return False
  return True

def isAlpha(word):
  for i in range(len(word)):
    if ('a'<=word[i] and word[i]<='z') == False:
      return False
  return True

def removeSomeWords(line):
  newStr = ''
  numWords = 0
  for word in twokenize.tokenizeRawTweetText(line):
    if word[0]!='@' and word.startswith('http')==False and word.startswith('#')==False and checkSpecial(word)==False:
      if numWords == 0 and word=='rt':
        continue
      newStr = newStr + word +' '
      numWords = numWords + 1
  return newStr

def postProcess(line):
  cur_words = line.split()
  new_words = []
  i = 0
  while True:
    if i >= len(cur_words):
      break
    if (i+1) < len(cur_words) and (cur_words[i+1] == u"\u0027" or cur_words[i+1] == u"\u2019"):
      new_words.append(cur_words[i]+"'")
      i = i + 2
    else:
      new_words.append(cur_words[i])
      i = i + 1
  resStr = ''
  for word in new_words:
    if word != ':p' and "..." not in word and checkSpecial(word)==False:
      resStr = resStr + word + ' '
  return resStr.strip()

def readStopWords():
  words = []
  with codecs.open('stopwords-fr.txt', 'r', 'utf-8', errors='ignore') as f:
    for line in f:
      words.append(line.strip())
  return words
#STOPWORDS = readStopWords()

def load_json_multiple(segments):
  chunk = ""
  for segment in segments:
    chunk += segment
    try:
      yield json.loads(chunk)
      chunk = ""
    except ValueError:
      pass

def is_emoji(s):
  count = 0
  for emoji in UNICODE_EMOJI:
    count += s.count(emoji)
    if count > 1:
      return False
  return bool(count)
def proper_word(word):
  num_alpha = 0
  for c in word:
    if not c.isalpha() and c!='-' and c!="'" and c!='.':
      return False
    if c.isalpha():
      num_alpha += 1
  return num_alpha!=0
def remove_special_words(tweet):
  words = tweet.split()
  new_words = []
  for word in words:
    if word[0] == '@' or word[0] == '#' or 'http' in word or 'www' in word or 't.co' in word:
      continue
    new_words.append(word)
  return ' '.join(new_words)
def new_tokenize(tweet):
  words = tweet.lower().split()
  if len(words)==0 or words[0] == 'rt':
    return None
  tweet = remove_special_words(tweet)
  new_words = []
  for word in moses(tweet.lower()):
    if word[0] == '@' or word[0] == '#' or 'http' in word:
      continue
    if word.isdigit():
      new_words.append('$$$DIGIT$$$')
      continue
    if proper_word(word):
      new_words.append(word)
      continue
    if is_emoji(word):
      new_words.append(word)
  if len(new_words)==0:
    return None
  return ' '.join(new_words)

def bert_tokenize(tweet):
  words = tweet.split()
  if len(words)==0 or words[0] == 'rt':
    return None
  tweet = remove_special_words(tweet)
  new_words = []
  for word in moses(tweet.lower()):
    if word[0] == '@' or word[0] == '#' or 'http' in word:
      continue
    if word.isdigit():
      new_words.append('$$$DIGIT$$$')
      continue
    if proper_word(word):
      new_words.append(word)
      continue
    if is_emoji(word):
      new_words.append(word)
  if len(new_words)==0:
    return None
  return new_words

def tokenize_all(bert=False)
# for BERT:
# todo create vocabulary to extract only these embeddings
if bert: 
  words_dict = {}

#print(source_files)
for i in tqdm(range(len(source_files))):
  source_file = source_files[i]
  dest_file = os.path.join(target_dir, source_file.split("/")[-1])
  w = codecs.open(dest_file, 'w', 'utf-8')
  with codecs.open(source_file, 'r', 'utf-8', errors='ignore') as f:
    for jfile in load_json_multiple(f):    
      line = jfile["tweet"]
      tweet_id = str(jfile["id"])
      # filtering to keep only most frequent users
      if user_filtering:
          if jfile["user_id"] not in users_list:
              next
      if english_only:
          if jfile["language"] != "en":
              next
      if tweet_id!=None and line!=None:
        if ':' in tweet_id:
          tweet_id = tweet_id.split(':')[-1]


        # for DBE:  
        #tokens = new_tokenize(line.strip())
        #if tokens:
        #  w.write('%s\t%s\n'%(tweet_id, tokens))
        
        
        # for BERT:
        tokens_list = bert_tokenize(line.strip())
        if tokens_list:
          for word in tokens_list:
            if word not in words_dict:
              words_dict[word]=1
            else:
              words_dict[word]+=1
          tokens =' '.join(tokens_list)
          w.write('%s\t%s\n'%(tweet_id, tokens))

        '''
        source = line
        line = removeSomeWords(line.lower())
        string = ''
        for word in moses.tokenize(line, escape=False):
          string += word + ' '
        string = postProcess(string.strip())
        string = string.strip()
        if len(string)>1:
          w.write(tweet_id + "\t")
          #w.write(line.strip() + '\n')
          w.write(string + "\n")
          i = i + 1
        '''
      #if i == 10000:
      #  break
  #if i == 10000:
  #  break
  w.close()
  print('Done with '+dest_file)


pickle.dump(words_dict, open(objects_path + "words_freq_antivax.pkl", 'wb')) 


