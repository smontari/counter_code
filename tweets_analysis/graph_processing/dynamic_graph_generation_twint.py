import os
import sys
from tqdm import tqdm
import glob
import json
import codecs
import twokenize
import time
from datetime import datetime
import pickle
import numpy as np
import pandas as pd

def load_json_multiple(segments):
  chunk = ""
  for segment in segments:
    chunk += segment
    try:
      yield json.loads(chunk)
      chunk = ""
    except ValueError:
      pass

def extractMentions(text):
  mentions=[]
  for word in twokenize.tokenizeRawTweetText(text):
    if word[0]=='@':
      mentions.append(word[1:].lower())
  return mentions

source_dir = "/data/almanach/user/smontari/scratch/scraped_tweets/antivax/"
source_files = glob.glob(source_dir + '/*.data')[:2]
user_filtering = False
french_only = True
tweet_sampling = False # If there are too many nodewise events, sample a certain amount of tweets per user (without mentions)
if user_filtering:
    with open('objects/israel_frequent_users_list.txt', 'r') as f:
        users_list = f.readlines()

def create_dyn_graph():
  """
  We create a csv file with the following columns: user_id,item_id,timestamp,state_label,comma_separated_list_of_features
  """
  # make a first run through the dataset to get a list of users and their tweets.
  print("Creating user lists")
  uid2name, name2uid, uid2tweetids = {}, {}, {}
  for i in tqdm(range(len(source_files))):
    source_file = source_files[i]
    with codecs.open(source_file, 'r', 'utf-8', errors='ignore') as f:
      for jfile in load_json_multiple(f):    
        tweet_id = str(jfile["id"])
        line = jfile["tweet"]
        # filetring to keep only most frequent users
        if user_filtering:
            if jfile["user_id"] not in users_list:
                next
        if french_only:
            if jfile["language"] != "fr":
                next
        user_id = str(jfile["user_id"])
        user_screen_name = jfile["username"]
        if tweet_id!=None and line!=None and user_id!=None and user_screen_name!=None:
          user_screen_name = user_screen_name.lower()
          if user_id not in uid2name:
            uid2name[user_id] = {}
            uid2tweetids[user_id] = []
          if user_screen_name not in uid2name[user_id]:
            uid2name[user_id][user_screen_name] = 1
          if user_screen_name not in name2uid:
            name2uid[user_screen_name] = user_id
          uid2tweetids[user_id].append(tweet_id)

  # second run: create the graph, keeping the interactions only when the mentioned user is in the graph.
  print("Creating dynamic graph")
  user_ids = []
  item_ids= []
  timestamps= []
  state_labels= []
  features= []

  for i in tqdm(range(len(source_files))):
    source_file = source_files[i]
    with codecs.open(source_file, 'r', 'utf-8', errors='ignore') as f:
      for jfile in load_json_multiple(f):    
        tweet_id = str(jfile["id"])
        line = jfile["tweet"]
        # filetring to keep only most frequent users
        if user_filtering:
            if jfile["user_id"] not in users_list:
                next
        if french_only:
            if jfile["language"] != "fr":
                next
        user_id = str(jfile["user_id"])
        user_screen_name = jfile["username"]
        created_at = datetime.strptime(jfile["created_at"][:-5], '%Y-%m-%d %H:%M:%S')
        timecode = int(created_at.timestamp())
        if tweet_id!=None and line!=None and user_id!=None and user_screen_name!=None:
          user_screen_name = user_screen_name.lower()
          mentions = extractMentions(line)
          if mentions: 
            for mention in mentions:
              if mention in name2uid:
                user_ids.append(user_id)
                item_ids.append(name2uid[mention])
                timestamps.append(timecode)
                state_labels.append(1)
                features.append(tweet_id)
          else:
            user_ids.append(user_id)
            item_ids.append(user_id)
            timestamps.append(timecode)
            state_labels.append(0)
            features.append(tweet_id)

  # create dictionary of user twitter ids and indexes starting at 0.
  uid2idx={}
  unique_users = list(set((user_ids + item_ids)))
  for idx, uid in enumerate(unique_users):
    uid2idx[uid] = idx
  print("number of unique users in the graph: ", len(unique_users))

  user_idxs = [uid2idx[uid] for uid in user_ids]
  item_idxs = [uid2idx[uid] for uid in item_ids]
  graph_df = pd.DataFrame({'u': user_idxs,
                'i': item_idxs,
                'ts': timestamps,
                'label': state_labels,
                'feature': features})
  return uid2name, uid2idx, name2uid, uid2tweetids, graph_df, np.array(features)

uid2name, uid2idx, name2uid, uid2tweetids, graph_df, features = create_dyn_graph()
save_obj = [uid2name, uid2idx, name2uid, uid2tweetids]
pickle.dump(save_obj, open('objects/data_dict.pkl', 'wb'))
graph_df.to_csv('objects/twitter.csv', index=False)
print(graph_df.describe())


#print("number of mentions: ", len(name2friends_mention), mentions_nb)

#save_obj = pickle.load(open('objects/net_maps.pkl', 'rb'))
#uid2name, name2uid, uid2tweetids, name2friends = save_obj[0], save_obj[1], save_obj[2], save_obj[3]

