from nltk.tokenize import TweetTokenizer
import re
import random
import json
import codecs
import numpy as np
import pandas as pd
import pickle
from collections import Counter
import ipdb

"""
We  want to select a smaller graph (not bigger than their dataset: 9000 nodes, 120.000 edges)
What they do: select only top 5000 inward and top 5000 outward users
"""

path = '/home/smontari/code/tweets-analysis/code/analysis/'
source_dir = "/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/"
source_file = source_dir + 'all.json'

#all_selected_tweets_df = pd.read_csv(source_dir + 'preprocessed/bert_all.csv', sep=',')
#selected_ids = set(all_selected_tweets_df['idx'])

#uid2tweetids = pickle.load(open(path + 'objects/uid2tweetids.pkl', 'rb'))
uid2idx = pickle.load(open(path + 'objects/uid2idx.pkl', 'rb'))
graph_df = pd.read_csv(path + 'objects/twitter_twarc.csv')

if __name__ == "__main__":  
    print("starting")
    print("number of events: ", len(graph_df))
    print("number of user: ", len(list(set(list(graph_df['u']) + list(graph_df['i'])))))
    #print("proportion of interaction: ", sum(list(graph_df['label']))*100/len(graph_df))
    n = 5000
    min_inter = 100

    # idx2uid = {v:k for k,v in uid2idx.items()}
    # user_ids = [idx2uid[idx] for idx in graph_df['u']]
    # item_ids = [idx2uid[idx] for idx in graph_df['i']]

    inward_users = Counter(graph_df['i'])
    outward_users = Counter(graph_df['u'])
    # top_in = [id for id, _ in inward_users.most_common(n)]
    # top_out = [id for id, _ in outward_users.most_common(n)]
    # print("selected top users: ", len(set(top_in+top_out)))
    # selected_users = list(set(top_in+top_out))
    
    # idx = np.where((graph_df['u'].isin(selected_users)) & (graph_df['i'].isin(selected_users)))

    # small_df = graph_df.loc[idx]
    # print("new number of events: ", len(small_df))
    # print("new number of users: ", len(set(list(small_df['u']) + list(small_df['i']))))

    valid_inward = [k for k,v in inward_users.items() if min_inter<v]
    valid_outward = [k for k,v in outward_users.items() if min_inter<v]
    print("selected frequent users: ", len(set(valid_inward+valid_outward)))

    selected_users = list(set(valid_inward+valid_outward))
    
    idx = np.where((graph_df['u'].isin(selected_users)) & (graph_df['i'].isin(selected_users)))

    small_df = graph_df.loc[idx]
    print("new number of events: ", len(small_df))
    print("new number of users: ", len(set(list(small_df['u']) + list(small_df['i']))))

    print("proportion of interaction: ", sum(small_df['label'])*100/len(small_df))

    # dump graph with only selected users and edge feature !
    uid2idx={}
    for idx, uid in enumerate(selected_users):
        uid2idx[uid] = idx

    user_idxs = [uid2idx[uid] for uid in small_df['u']]
    item_idxs = [uid2idx[uid] for uid in small_df['i']]
    graph_df_new = pd.DataFrame({'u': user_idxs,
                    'i': item_idxs,
                    'ts': small_df['ts'],
                    'label': small_df['label'],
                    'feature': small_df['feature']})

    graph_df_new.to_csv(path + 'objects/twitter_twarc_small.csv', index=False)


    ############################

    selected_users = list(set(valid_inward+valid_outward))[:100]
    
    idx = np.where((graph_df['u'].isin(selected_users)) & (graph_df['i'].isin(selected_users)))

    small_df = graph_df.loc[idx]
    print("new number of events: ", len(small_df))
    print("new number of users: ", len(set(list(small_df['u']) + list(small_df['i']))))

    print("proportion of interaction: ", sum(small_df['label'])*100/len(small_df))

    # dump graph with only selected users and edge feature !
    uid2idx={}
    for idx, uid in enumerate(selected_users):
        uid2idx[uid] = idx

    user_idxs = [uid2idx[uid] for uid in small_df['u']]
    item_idxs = [uid2idx[uid] for uid in small_df['i']]
    graph_df_new = pd.DataFrame({'u': user_idxs,
                    'i': item_idxs,
                    'ts': small_df['ts'],
                    'label': small_df['label'],
                    'feature': small_df['feature']})


    
    
    graph_df_new.to_csv(path + 'objects/twitter_twarc_micro.csv', index=False)

