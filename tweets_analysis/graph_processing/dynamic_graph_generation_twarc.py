import os
import sys
from tqdm import tqdm
import glob
import json
import codecs
import twokenize
import time
from datetime import datetime
import pickle
import numpy as np
import pandas as pd
from pprint import pprint
import ipdb

def load_json_multiple(segments):
  chunk = ""
  for segment in segments:
    chunk += segment
    try:
      yield json.loads(chunk)
      chunk = ""
    except ValueError:
      pass

def extractMentions(tweet_dict):
  """
  Extract mentions and retweets.
  If it is a retweet, extract the id of the corresponding tweet.
  In both cases, check if the id is in the list of preprocessed tweets (whose embedding has been extracted)
  Keep only mentioned users that are already in the graph.
  """
  mentions=[]
  if 'mentions' in tweet_dict.get('entities',[]):
    for mention_dict in tweet_dict['entities']['mentions']:
      mentions.append(mention_dict['id'])  
  if 'in_reply_to_user_id' in tweet_dict:
    mentions.append(tweet_dict['in_reply_to_user_id'])
  # get the id of the tweet that is retweet (if RT), otherwise take the current tweet id.
  if 'referenced_tweets' in tweet_dict:
    interaction_tweet_id = int(tweet_dict['referenced_tweets'][0]['id'])
  else:
    interaction_tweet_id = int(tweet_dict['id'])
  mentions_filtered = [id for id in set(mentions) if id in uid2tweetids]
  if interaction_tweet_id in selected_ids:
    return mentions_filtered, interaction_tweet_id
  else:
    return None, None

def create_user_list():
  # make a first run through the dataset to get a list of users and their tweets.
  print("Creating user lists")
  uid2tweetids = {}
  counter = 0
  with codecs.open(source_file, 'r', 'utf-8', errors='ignore') as f:
      for jfile in load_json_multiple(f):
        for tweet_dict in jfile['data']:
          counter +=1
          if counter%100000==0:
            print(counter)
          line = tweet_dict["text"]
          tweet_id = str(tweet_dict["id"])
          user_id = str(tweet_dict["author_id"])
          if tweet_id!=None and line!=None and user_id!=None:
            if user_id not in uid2tweetids:
              uid2tweetids[user_id] = []
            uid2tweetids[user_id].append(tweet_id)
  return uid2tweetids
  
def create_dyn_graph(uid2tweetids):
  """
  We create a csv file with the following columns: user_id,item_id,timestamp,state_label,comma_separated_list_of_features
  """
  # second run: create the graph, keeping the interactions only when the mentioned user is in the graph
  # AND when the corresponding tweet is in the selected list (tweet whose embedding is extracted)
  print("Creating dynamic graph")
  user_ids = []
  item_ids= []
  timestamps= []
  state_labels= []
  features= []
  counter = 0
  with codecs.open(source_file, 'r', 'utf-8', errors='ignore') as f:
    for jfile in load_json_multiple(f):
      for tweet_dict in jfile['data']:
        counter +=1
        if counter%100000==0:
          print(counter, "proportion of interaction: ", sum(state_labels)*100/counter)
        line = tweet_dict["text"]
        tweet_id = str(tweet_dict["id"])
        user_id = str(tweet_dict["author_id"])
        created_at = datetime.fromisoformat(tweet_dict["created_at"][:-1])
        timecode = int(created_at.timestamp())
        if tweet_id!=None and line!=None and user_id!=None:
          mentions, interaction_tweet_id = extractMentions(tweet_dict)
          if interaction_tweet_id:
            if mentions: 
              for mention in mentions:
                user_ids.append(user_id)
                item_ids.append(mention)
                timestamps.append(timecode)
                state_labels.append(1)
                features.append(interaction_tweet_id)
          # if there is no interaction but the tweet id is in the list of selected tweets:
            else:
                user_ids.append(user_id)
                item_ids.append(user_id)
                timestamps.append(timecode)
                state_labels.append(0)
                features.append(interaction_tweet_id)

  # create dictionary of user twitter ids and indexes starting at 0.
  uid2idx={}
  unique_users = list(set((user_ids + item_ids)))
  for idx, uid in enumerate(unique_users):
    uid2idx[uid] = idx
  print("number of unique users in the graph: ", len(unique_users))

  user_idxs = [uid2idx[uid] for uid in user_ids]
  item_idxs = [uid2idx[uid] for uid in item_ids]
  graph_df = pd.DataFrame({'u': user_idxs,
                'i': item_idxs,
                'ts': timestamps,
                'label': state_labels,
                'feature': features})
  return uid2idx, graph_df, np.array(features)


path = '/home/smontari/code/tweets-analysis/code/analysis/'
source_dir = "/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/"
source_file = source_dir + 'all.json'
tweet_sampling = False # If there are too many nodewise events, sample a certain amount of tweets per user (without mentions)
all_selected_tweets_df = pd.read_csv(source_dir + 'preprocessed/bert_all.csv', sep=',')
selected_ids = set(all_selected_tweets_df['idx'])


#uid2tweetids = create_user_list()
#pickle.dump(uid2tweetids, open(path + 'objects/uid2tweetids.pkl', 'wb'))
uid2tweetids = pickle.load(open(path + 'objects/uid2tweetids.pkl', 'rb'))

uid2idx, graph_df, features = create_dyn_graph(uid2tweetids)
pickle.dump(uid2idx, open(path + 'objects/uid2idx.pkl', 'wb'))
graph_df.to_csv(path + 'objects/twitter_twarc.csv', index=False)
print("number of events: ", len(graph_df))
print("proportion of interaction: ", sum(list(graph_df['label']))*100/len(graph_df))
