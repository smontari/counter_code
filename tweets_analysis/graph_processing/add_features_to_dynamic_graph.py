from nltk.tokenize import TweetTokenizer
import re
import random
import json
import codecs
import numpy as np
import pandas as pd
#import torch
import pickle
from collections import Counter
import ipdb

"""
We want to add bert features to the graph dataset
"""

path = '/home/smontari/code/tweets-analysis/code/analysis/'
source_dir = "/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/"

#path = '/home/smontari/code/data_processing/tweets-analysis/code/analysis/'

all_selected_tweets_df = pd.read_csv(source_dir + 'preprocessed/bert_all.csv', sep=',')
selected_ids = set(all_selected_tweets_df['idx'])

small_df = pd.read_csv(path + 'objects/twitter_twarc_small.csv')

if __name__ == "__main__":  
    print("starting")
    #print("proportion of interaction: ", sum(list(graph_df['label']))*100/len(graph_df))
    #dump_path = '/home/smontari/scratch/data/twitter-scraping/antipass/emb_ids_list_finetuned_camembert.pkl'
    dump_path = '/home/smontari/code/tweets-analysis/code/analysis/objects/emb_ids_list_finetuned_camembert.pkl'

    with open(dump_path, 'rb') as file:
        embeddings_tensor, ids_tensor = pickle.load(file)
    #emb_dict = {i:emb for i, emb in zip(embeddings_tensor, ids_tensor)}

    # dimensionality reduction
    dim = 10
    #proj_mat = torch.randn(768, dim)
    #embeddings_tensor_small = torch.einsum("nb,bd->nd", embeddings_tensor, proj_mat).numpy()
    embeddings_tensor_small = embeddings_tensor.numpy()

    # create matrix of all embeddings in the graph
    twid2idx = {int(twid):i for i, twid in enumerate(ids_tensor)}

    event_id_list = list(small_df['feature'])
    print(len(set(int(i) for i in event_id_list)&set(twid2idx.keys())))

    all_embs=embeddings_tensor_small[[twid2idx[int(i)] for i in event_id_list]]
    print(np.array(all_embs).shape)


    with open(source_dir + 'objects/twitter_twarc_small_edgefeatures_768_only.npy', 'wb') as f:
        np.save(f, np.array(all_embs))

    #all_embs = all_embs[:n]
    # n=100
    #embs_df = pd.DataFrame(all_embs)
    #del all_embs

    #small_df = pd.concat([small_df, embs_df], axis=1)
    #small_df.drop('feature', inplace=True, axis=1)
    #print(len(small_df.columns))

    #small_df.to_csv(source_dir + 'objects/twitter_twarc_small_edgefeatures_768.csv', index=False)