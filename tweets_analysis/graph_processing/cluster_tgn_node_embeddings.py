import torch
from sklearn import metrics
import sklearn.cluster
from sklearn.manifold import TSNE
import pandas as pd
import pickle
import numpy as np
#import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib
import ipdb
from collections import Counter
from sklearn import decomposition
from MulticoreTSNE import MulticoreTSNE as TSNE
from yellowbrick.cluster import KElbowVisualizer


np.random.seed(5)

def calculate_WSS(points, kmax=20):
  sse = []
  for k in range(1, kmax+1):
    kmeans = KMeans(n_clusters = k).fit(points)
    centroids = kmeans.cluster_centers_
    pred_clusters = kmeans.predict(points)
    curr_sse = 0
    
    # calculate square of Euclidean distance of each point from its cluster center and add to current WSS
    for i in range(len(points)):
      curr_center = centroids[pred_clusters[i]]
      curr_sse += (points[i, 0] - curr_center[0]) ** 2 + (points[i, 1] - curr_center[1]) ** 2
      
    sse.append(curr_sse)
  return sse


# Clustering
def cluster_nodes(node_embs, categories, k):
    km = sklearn.cluster.KMeans(k)
    km.fit(node_embs)
    clusters = km.predict(node_embs)
    pred = clusters[annotated_idxs]
    #categories=np.random.randint()

    h, c, v = metrics.homogeneity_completeness_v_measure(categories, pred)
    print(h,c,v)
    # (= precision, recall, f1)
    # v not adequate for only 30 samples with very small categories
    #with random ground truth: 0.14766315882650363, 0.15753621559913356, 0.1524399928323517)

    ri = metrics.adjusted_rand_score(categories, pred)
    print(ri)
    # proba of two elemnts of a pair of users (of the same ground truth categ) to be in the same clusters, and conversely
    # (= proba que les assignements soient compatibles)
    return h,c,v,ri

def plot_tsne_groundtruth(model_name, node_embs):
    categories_full = [9]*len(node_embs)
    for user, categ in zip(annotated_idxs, categories):
        categories_full[user]=categ
    tsne = TSNE(n_components=2, verbose=1, random_state=123, n_jobs=4, n_iter=500)
    z = tsne.fit_transform(node_embs) 
    df = pd.DataFrame()
    #df["y"] = y
    df["comp-1"] = z[:,0]
    df["comp-2"] = z[:,1]
    catnames = ["neutral neutral", "neutral anti-pass", "neutral pro-pass", "anti-vax neutral", "anti-vax anti-pass", "anti-vax pro-pass", "pro-vax neutral", " pro-vax anti-pass", "pro-vax pro-pass", "unlabelled"]
    formatter = plt.FuncFormatter(lambda val, loc: catnames[val])
    plt.scatter(df["comp-1"] , df["comp-2"], c = categories_full, s=1.0+(np.array(categories_full)!=9)*20, cmap="tab10")
    plt.colorbar(ticks=list(range(10)), format=formatter)
    plt.savefig('tsne_annotated_' + model_name + '.png')
    plt.close()
    plt.clf()

def plot_tsne_quickhastgas(model_name, node_embs):
    categories_full = [2]*len(node_embs)
    for user, categ in zip(annotated_idxs, categories):
        categories_full[user]=categ
    tsne = TSNE(n_components=2, verbose=1, random_state=123, n_jobs=4, n_iter=500)
    z = tsne.fit_transform(node_embs) 
    df = pd.DataFrame()
    #df["y"] = y
    df["comp-1"] = z[:,0]
    df["comp-2"] = z[:,1]
    catnames = ["pro-vax", "anti-vax", "unlabelled"]
    formatter = plt.FuncFormatter(lambda val, loc: catnames[val])
    coloursmap = (matplotlib.colors.ListedColormap(['red', 'green', 'blue']).with_extremes(over='0.25', under='0.75'))
    plt.scatter(df["comp-1"] , df["comp-2"], c = categories_full, s=1.0+(np.array(categories_full)!=2)*20, cmap = coloursmap)
    plt.colorbar(ticks=list(range(2)), format=formatter)
    plt.savefig('tsne_annotated_' + model_name + '.png')
    plt.close()
    plt.clf()

def extracthashtag(text):
  hasht=[]
  for word in text.split():
    if word[0]=='#':
      hasht.append(word[1:].lower())
  return hasht

def create_hashtag_dataset():
    print("creating hashtags dict")
    tweet_hasht = []
    for i, tweet in enumerate(tweets_df['text']):
        if i%100000==0:
            print(i)
        tweet_hasht.append(extracthashtag(tweet))
    hashtags_dict = {i: hst for i, hst in zip(tweets_df['idx'],tweet_hasht)}
    return hashtags_dict

def extract_user_hashtags():
    hashtags_dict = create_hashtag_dataset()
    user_main_hastag = []
    graph_df = pd.read_csv('/home/smontari/code/tweets-analysis/code/analysis/objects/twitter_twarc_small.csv')
    users_ids_list = set(list(graph_df['u']))
    user_twid_list = [idx2uid[u] for u in users_ids_list]
    print(len(users_ids_list), len(user_twid_list))
    print("extracting user hashtags")
    for i, user_idx in enumerate(users_ids_list):
        if i%1000==0:
            print(i)
        user_hastags = []
        user_twid = idx2uid[user_idx]
        # keep only tweets from bert_all file (where the hashtags are already extracted)
        user_filtered_tweets = set(int(i) for i in uid2tweetids[user_twid])&set(list(tweets_df['idx']))
        for tweetid in user_filtered_tweets:
            hasht_list = hashtags_dict[int(tweetid)]
            user_hastags.extend(hasht_list)
        if user_hastags:
            user_main_hastag.append(Counter(user_hastags).most_common(1)[0][0])
        else:
            user_main_hastag.append("empty")
    user_hastags_df = pd.DataFrame(list(zip(users_ids_list,user_twid_list,user_main_hastag)), columns=['idx','twuid','hashtag'])
    user_hastags_df.to_csv('/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/preprocessed/user_main_hashtags.csv')


def hashtags_to_categories():
    user_hastags_df = pd.read_csv('/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/preprocessed/user_main_hashtags.csv')
    hashtags_full = [9]*len(node_embs)
    for user, hasht in zip(user_hastags_df['idx'], user_hastags_df['hashtag']):
        hashtags_full[int(user)]=hasht
    print(len(set(hashtags_full)))

    hasht2id = {}
    for i, hasht in enumerate((set(hashtags_full))):
        hasht2id[hasht] = i

    hashtags_full_ids = [hasht2id[hasht] for hasht in hashtags_full]
    return hashtags_full_ids

def tsne_hashtags():
    hashtags_full_ids = hashtags_to_categories()
    tsne = TSNE(n_components=2, verbose=1, random_state=123)
    z = tsne.fit_transform(node_embs) 
    df = pd.DataFrame()
    #df["y"] = y
    df["comp-1"] = z[:,0]
    df["comp-2"] = z[:,1]

    plt.scatter(df["comp-1"] , df["comp-2"], c = hashtags_full_ids, cmap="tab10", s=1 )
    plt.colorbar()
    plt.savefig('tsne_hashtags.png')
    plt.clf()


def tsne_clusters(clusters):
    tsne = TSNE(n_components=2, verbose=1, random_state=123)
    z = tsne.fit_transform(node_embs) 
    df = pd.DataFrame()
    #df["y"] = y
    df["comp-1"] = z[:,0]
    df["comp-2"] = z[:,1]

    plt.scatter(df["comp-1"] , df["comp-2"], c = clusters, cmap="tab10", s=1 )
    plt.colorbar()
    plt.savefig('tsne_clusters.png')

def pca_clusters():

    pca = decomposition.PCA(n_components=2)
    pca_data = pd.DataFrame(pca.fit_transform(model['memory.memory']),columns=['PC1','PC2']) 

    print("explained variance ratio: ", pca.explained_variance_ratio_)

    plt.figure(figsize=(6, 5))
    fig,ax = plt.subplots()
    scatter = ax.scatter(pca_data['PC1'], pca_data['PC2'],c=clusters,alpha=0.7)

    plt.savefig('pca_clusters.png')
    plt.clf()

def compute_tsne_clust_metrics():
    metrics_dict = {}
    for model_name in models_dict.keys():
        print(model_name)
        model_path = models_dict[model_name]
        model = torch.load('/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/'+model_path, map_location=torch.device('cpu'))
        #models_path = "/home/smontari/scratch/data/twitter-scraping/antipass/objects/"
        node_embs = model['memory.memory']
        plot_tsne_quickhastgas(model_name, node_embs)
        metrics_list = cluster_nodes(node_embs, k=3)
        metrics_dict[model_name]=metrics_list
    print(metrics_dict)


models_dict = {
    'nodewise':'objects_twitter_twarc_small/saved_models/tgn-attn-nodewisetwitter_twarc_small.pth',
    'vanilla':'objects_twitter_twarc_small/saved_models/tgn-attntwitter_twarc_small.pth',
    'nodewise300':'objects_twitter_twarc_small_300/saved_models/tgn-attn-nodewisetwitter_twarc_small_300.pth',
    'vanilla300':'objects_twitter_twarc_small_300/saved_models/tgn-attntwitter_twarc_small_300.pth'
}

uid2idx = pickle.load(open('objects/uid2idx_new.pkl', 'rb'))
idx2uid = {v:k for k,v in uid2idx.items()}
# get annotations 

#vacccineopinion_hashtags_users
#annotated_user_category
annotations = pd.read_csv('objects/annotated_user_category.txt', header=None)
annotated_users = list(annotations[0])
annotated_idxs= [uid2idx[str(user)] for user in annotated_users]
categories = list(annotations[1])
print(Counter(categories))

metrics_dict = {}
for model_name in models_dict.keys():
    print(model_name)
    model_path = models_dict[model_name]
    model = torch.load('/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/'+model_path, map_location=torch.device('cpu'))
    #models_path = "/home/smontari/scratch/data/twitter-scraping/antipass/objects/"
    node_embs = model['memory.memory']
    #plot_tsne_quickhastgas(model_name, node_embs)
    metrics_list = cluster_nodes(node_embs, categories, k=9)
    metrics_dict[model_name]=metrics_list
print(metrics_dict)



#compute_tsne_clust_metrics()

model_name = 'vanilla300'
print(model_name)
model_path = models_dict[model_name]
model = torch.load('/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/objects/'+model_path, map_location=torch.device('cpu'))
#models_path = "/home/smontari/scratch/data/twitter-scraping/antipass/objects/"
node_embs = model['memory.memory']

print("Computing silhouette score")
# doing some clustering
#km = sklearn.cluster.KMeans()
#visualizer = KElbowVisualizer(km, k=(3,15))
#visualizer.fit(node_embs.numpy()) 
#k = visualizer.elbow_value_
#print(k) # k=7 for nodewise300
k=8
km = sklearn.cluster.KMeans(k)
clusters = km.fit_predict(node_embs)

# extracting most frequent hashtag of each cluster
users_idx = [int(i) for i in range(len(clusters))]

user_hastags_df = pd.read_csv('/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/preprocessed/user_main_hashtags.csv')
cluster_hashtag_dict = {}
main_h_list = []
for clust in set(clusters):
    clust_users = [u for u,c in zip(users_idx,clusters) if c==clust]
    clust_hasht = list(user_hastags_df[user_hastags_df['idx'].isin(clust_users)]['hashtag'])
    print(Counter(clust_hasht).most_common(10))
    main_hasht = Counter(clust_hasht).most_common()
    if main_hasht[0][0] in ['empty'] and len(main_hasht)>1:
        main_hasht = main_hasht[1][0]
    else:
        main_hasht = main_hasht[0][0]
    print(clust, len(clust_users), len(clust_hasht),main_hasht)
    main_h_list.append(main_hasht)




tsne = TSNE(n_components=2, verbose=1, random_state=123, n_jobs=4, n_iter=500)
z = tsne.fit_transform(node_embs) 
df = pd.DataFrame()
#df["y"] = y
df["comp-1"] = z[:,0]
df["comp-2"] = z[:,1]
cluster_names = ['passdelahonte', 'manif7aout', 'antipasssanitaire','nonaupassdelahonte','vaccinezvous','vaccination','antivax','manif31juillet']
formatter = plt.FuncFormatter(lambda val, loc: cluster_names[val])
plt.scatter(df["comp-1"] , df["comp-2"], c = clusters, s=1, cmap="Dark2")
plt.colorbar(ticks=list(range(7)), format=formatter)
plt.savefig('tsne_selected_hashtags' + model_name + '8.png')
plt.close()
plt.clf()








# get hashtags
#tweets_file = '/home/smontari/scratch/data/twitter-scraping/antipass/preprocessed/bert_all.csv'
# tweets_file =  '/data/almanach/user/smontari/scratch/scraped_tweets/twarc_antipass/preprocessed/bert_all.csv'
# tweets_df = pd.read_csv(tweets_file)
# uid2tweetids = pickle.load(open('objects/uid2tweetids.pkl','rb'))
#uid2tweetids = pickle.load(open('/home/smontari/scratch/data/twitter-scraping/antipass/preprocessed/uid2tweetids.pkl','rb'))
#categories = hashtags_to_categories()
#pred = clusters[annotated_idxs]
#h, c, v = metrics.homogeneity_completeness_v_measure(categories, clusters)

#h, c, v = metrics.homogeneity_completeness_v_measure(categories, clusters)
#print(h,c,v)
# (= precision, recall, f1)
# v not adequate for only 30 samples with very small categories

# with random ground truth: 0.14766315882650363, 0.15753621559913356, 0.1524399928323517)

#ri = metrics.adjusted_rand_score(categories, clusters)
#print(ri)
# proba of two elemnts of a pair of users (of the same ground truth categ) to be in the same clusters, and conversely
# (= proba que les assignements soient compatibles)
