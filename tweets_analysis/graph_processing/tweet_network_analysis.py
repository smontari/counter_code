import os
import sys
from tqdm import tqdm
import glob
import json
import codecs
import twokenize
import pickle
import networkx as nx
from node2vec import Node2Vec
from sklearn.cluster import KMeans
import numpy as np



def load_json_multiple(segments):
  chunk = ""
  for segment in segments:
    chunk += segment
    try:
      yield json.loads(chunk)
      chunk = ""
    except ValueError:
      pass

def extractMentions(text):
  mentions=[]
  for word in twokenize.tokenizeRawTweetText(text):
    if word[0]=='@':
      mentions.append(word[1:].lower())
  return mentions

source_dir = "/home/smontari/scratch/data/twitter-scraping/search_jerusalem OR israel OR gaza OR palestin"
source_files = glob.glob(source_dir + '/*.data')
user_filtering = True
english_only = True
if user_filtering:
    with open('objects/israel_frequent_users_list.txt', 'r') as f:
        users_list = f.readlines()

def create_mentions_dict():
  # run through the dataset
  uid2name, name2uid, uid2tweetids, name2friends_mention = {}, {}, {}, {}
  mentions_nb = 0

  for i in tqdm(range(len(source_files))):
    source_file = source_files[i]
    with codecs.open(source_file, 'r', 'utf-8', errors='ignore') as f:
      for jfile in load_json_multiple(f):    
        #print(jfile)
        line = jfile["tweet"]
        tweet_id = str(jfile["id"])
        # filetring to keep only most frequent users
        if user_filtering:
            if jfile["user_id"] not in users_list:
                next
        if english_only:
            if jfile["language"] != "en":
                next
        user_id = str(jfile["user_id"])
        user_screen_name = jfile["username"]
        created_at_year = int(jfile["created_at"].split("-")[0])
        if tweet_id!=None and line!=None and user_id!=None and user_screen_name!=None:
          if ':' in tweet_id:
            tweet_id = tweet_id.split(':')[-1]
          if ':' in user_id:
            user_id = user_id.split(":")[-1]
          user_screen_name = user_screen_name.lower()
          if user_id not in uid2name:
            uid2name[user_id] = {}
            uid2tweetids[user_id] = []
            name2friends_mention[user_id] = {}
          if user_screen_name not in uid2name[user_id]:
            uid2name[user_id][user_screen_name] = 1
          if user_screen_name not in name2uid:
            name2uid[user_screen_name] = user_id
          uid2tweetids[user_id].append(tweet_id)
          mentions = extractMentions(line)
          for mention in mentions:
            mentions_nb +=1
            if mention not in name2friends_mention[user_id]:
              name2friends_mention[user_id][mention] = 1
            else:
              name2friends_mention[user_id][mention] +=1
  return uid2name, name2uid, uid2tweetids, name2friends_mention

#uid2name, name2uid, uid2tweetids, name2friends_mention = create_mentions_dict()
#save_obj = [uid2name, name2uid, uid2tweetids, name2friends_mention]
#pickle.dump(save_obj, open('objects/net_maps.pkl', 'wb'))
#print("number of mentions: ", len(name2friends_mention), mentions_nb)

save_obj = pickle.load(open('objects/net_maps.pkl', 'rb'))
uid2name, name2uid, uid2tweetids, name2friends = save_obj[0], save_obj[1], save_obj[2], save_obj[3]

"""
name2friends_uid = {}
for name_id in name2friends_mention:
  if name_id not in valid_user_ids:
    continue
  name2friends_uid[name_id] = {}
  for friend_mention in name2friends_mention[name_id]:
    if friend_mention in name2uid and name2uid[friend_mention] in valid_user_ids:
      name2friends_uid[name_id][name2uid[friend_mention]] = 1

print('writing outs...')
TARDIR = "../../network/step4/v2_full/"
u2net_f = open(TARDIR+'uids_to_friends', 'w')
u2tweet_f = open(TARDIR+'uids_to_tweets', 'w')
u2screen_f = open(TARDIR+'uids_to_names', 'w')
num_users, num_links = 0, 0
i = 0
for uid in name2friends_uid:
  print(str(i)+"\t"+str(uid))
  i = i + 1
  friends = []
  for friend_id in name2friends_uid[uid]:
    if uid in name2friends_uid[friend_id]:
      friends.append(str(friend_id))
  if len(friends)>0:
    friends_str = ','.join(friends)
    u2net_f.write(str(uid)+"\t"+friends_str+"\n")
    tweets_str = ','.join([str(item) for item in uid2tweetids[uid]])
    u2tweet_f.write(str(uid)+"\t"+tweets_str+"\n")
    screen_names = ','.join([str(item) for item in uid2name[uid]])
    u2screen_f.write(str(uid)+"\t"+screen_names+"\n")
    num_users+=1
    num_links+=len(friends)
u2net_f.close()
u2tweet_f.close()
u2screen_f.close()
num_links=num_links/2
print('# users = '+str(num_users))
print('# links = '+str(num_links))
"""
def create_graph(G_dict):
    '''
        returns
        -------
        G : networkx.DiGraph
            corresponding graph
    '''
    G = nx.DiGraph()
    G.add_nodes_from(G_dict.keys())

    for (u1,v) in G_dict.items():
        for (u2, count) in v['quoted'].items():
            G.add_edge(u1,u2,label='quoted',weight=count)
        for (u2, count) in v['replied_to'].items():
            G.add_edge(u1,u2,label='replied_to',weight=count)
        for (u2, count) in v['retweeted'].items():
            G.add_edge(u1,u2,label='retweeted',weight=count)

    return G

G = create_graph(name2friends)


# help to draw https://networkx.github.io/documentation/networkx-1.9/examples/drawing/labels_and_colors.html
nx.draw(G,with_labels = True,node_color='b',node_size=500)

# Generate walks
node2vec = Node2Vec(G, dimensions=2, walk_length=20, num_walks=10,workers=4)
# Learn embeddings 
model = node2vec.fit(window=10, min_count=1)
#model.wv.most_similar('1')
model.wv.save_word2vec_format("objects/network_embedding.emb") #save the embedding in file embedding.emb


# X = np.loadtxt("embedding.emb", skiprows=1) # load the embedding of the nodes of the graph
# #print(X)
# # sort the embedding based on node index in the first column in X
# X=X[X[:,0].argsort()]; 
# #print(X)
# Z=X[0:X.shape[0],1:X.shape[1]]; # remove the node index from X and save in Z

# kmeans = KMeans(n_clusters=2, random_state=0).fit(Z) # apply kmeans on Z
# labels=kmeans.labels_  # get the cluster labels of the nodes.
# print(labels)
