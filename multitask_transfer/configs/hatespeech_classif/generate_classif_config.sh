#!/bin/bash

DATAPATH="/data/almanach/user/smontari/scratch/machamp/data/hate/comparable_clean_training_data"

for topic in immigrants women
do 
    for lang in en es it
        do 
            train_file="$DATAPATH/$topic/$lang/train.txt"
            echo $train_file
            valid_file="$DATAPATH/$topic/$lang/dev.txt"
            cat template_hate_classif.json | sed -e "s#LANG#$lang#"  -e "s#TRAIN_DATA#$train_file#" -e "s#VALID_DATA#$valid_file#" > ${topic}_$lang.json
        done

    for langpair in "en es" "it es" "en it"
        do 
            lang=( $langpair )
            echo $langpair
            train_a="$DATAPATH/$topic/${lang[0]}/train.txt"
            valid_a="$DATAPATH/$topic/${lang[0]}/dev.txt"
            train_b="$DATAPATH/$topic/${lang[1]}/train.txt"
            valid_b="$DATAPATH/$topic/${lang[1]}/dev.txt"
            cat template_hate_classif_bilingual.json | sed -e "s#LANG1#${lang[0]}#"  -e "s#TRAIN_A#${train_a}#" -e "s#VALID_A#${valid_a}#" \
                                                            -e "s#LANG2#${lang[1]}#"  -e "s#TRAIN_B#${train_b}#" -e "s#VALID_B#${valid_b}#" \
                                                             > ${topic}_${lang[0]}+${lang[1]}.json
        done
done

