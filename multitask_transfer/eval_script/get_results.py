import pandas as pd
from glob import glob
import os
import ipdb
import numpy as np
import ast

logs_path = '/data/almanach/user/smontari/scratch/machamp/logs/'
langs = ['en', 'es', 'it']
topics = ['immigrants', 'women']
column_names = ['model', 'tasks','joint','langtrain']
accuracy_table = pd.DataFrame(columns=pd.Index(column_names + [t + '-' + l for t in topics for l in langs]))

for model in ['xlmr-WITHsmooth_sampling']: #os.listdir(logs_path): #['mbert', 'mbert-WITHsmooth_sampling]: 
    model_path = os.path.join(logs_path, model)
    for task in os.listdir(model_path):
        task_path = os.path.join(model_path, task)
        for topic_lang in os.listdir(task_path):
            res_path = os.path.join(task_path, topic_lang)
            topic = topic_lang.split('_')[0]
            lang = topic_lang.split('_')[1]
            system_name = model + '-' + task + '-' +  lang
            accuracy_table.append(pd.Series(name='system_name'))
            metrics_dict = {}
            # check if all five seeds have been used for training this model
            if '5' in os.listdir(res_path):
                accuracy_table.loc[system_name, 'model'] = model
                accuracy_table.loc[system_name, 'tasks'] = task
                accuracy_table.loc[system_name, 'langtrain'] = lang
                accuracy_table.loc[system_name, 'joint'] = False
                if 'joint' in task:
                    accuracy_table.loc[system_name, 'joint'] = True
                for testlang in langs:
                    
                    metrics_dict[testlang] = []
                    for seed in range(5):
                        seed = str(seed+1)
                        res_file = os.path.join(res_path, seed, 'test_'+testlang+'.out.eval')
                        if os.path.exists(res_file):
                            with open(res_file, "r") as f:
                                res_string = f.read()
                            res_dict = ast.literal_eval(res_string[:-1])
                            metric = res_dict['.run/hate/macro-f1']
                            metrics_dict[testlang].append(metric)
                    avg_metric = np.mean(metrics_dict[testlang])
                    metrics_dict[testlang] = avg_metric
                    accuracy_table.loc[system_name, topic + '-' + testlang] = '{:0.04}'.format(avg_metric)

print(accuracy_table)
accuracy_table.to_csv("eval_script/accuracy_table.csv")
