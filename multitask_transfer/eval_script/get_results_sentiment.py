import pandas as pd
from glob import glob
import os
import ipdb
import numpy as np
import ast

logs_path = '/data/almanach/user/smontari/scratch/machamp/logs/'
langs = ['en', 'es', 'it']
column_names = ['model', 'task', 'langtrain']
accuracy_table = pd.DataFrame(columns=pd.Index(column_names + langs))

for model in ['xlmr-WITHsmooth_sampling']: #os.listdir(logs_path): #['mbert', 'mbert-WITHsmooth_sampling]: 
    model_path = os.path.join(logs_path, model)
    for task in ['sentiment_analysis-only', 'UD_sentiment_joint', 'UD-sentiment-sequential']:
        task_path = os.path.join(model_path, task)
        for sent_lang in os.listdir(task_path):
            res_path = os.path.join(task_path, sent_lang)
            lang = sent_lang.split('_')[1]
            system_name = model + '-' + task + '-' +  lang
            accuracy_table.append(pd.Series(name='system_name'))
            metrics_dict = {}
            # check if all five seeds have been used for training this model
            if '5' in os.listdir(res_path):
                accuracy_table.loc[system_name, 'model'] = model
                accuracy_table.loc[system_name, 'task'] = task
                accuracy_table.loc[system_name, 'langtrain'] = lang
                for testlang in langs:
                    metrics_dict[testlang] = []
                    for seed in range(5):
                        seed = str(seed+1)
                        res_file = os.path.join(res_path, seed, 'test_'+testlang+'.out.eval')
                        if os.path.exists(res_file):
                            with open(res_file, "r") as f:
                                res_string = f.read()
                            res_dict = ast.literal_eval(res_string[:-1])
                            metric = res_dict['.run/sentimentAnalysis/macro-f1']
                            metrics_dict[testlang].append(metric)
                    avg_metric = np.mean(metrics_dict[testlang])
                    metrics_dict[testlang] = avg_metric
                    accuracy_table.loc[system_name, testlang] = '{:0.04}'.format(avg_metric)

print(accuracy_table)
accuracy_table.to_csv("eval_script/accuracy_table_sentiment.csv")
