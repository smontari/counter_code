import pandas as pd
import numpy as np
import pickle
import torch
from transformers import BertTokenizerFast, BertForSequenceClassification, Trainer, TrainingArguments
from sklearn.metrics import accuracy_score, precision_recall_fscore_support, classification_report

from transformers import AutoTokenizer
from transformers import AutoModelForSequenceClassification, AutoModel

import os, sys
import glob
import tarfile
import json, ipdb
from allennlp.models import load_archive
from allennlp.data.tokenizers import Tokenizer, PretrainedTransformerTokenizer
from allennlp.models import load_archive
from allennlp.modules.token_embedders import PretrainedTransformerEmbedder
from allennlp.common import Params
from allennlp.common.plugins import import_plugins

from ./machamp.dataset_readers.machamp_universal_reader import MachampUniversalReader
from ./machamp.modules import pretrained_transformer_mixmatched_indexer
from ./machamp.models.machamp_model import MachampModel
from ./machamp.models.sentence_decoder import MachampClassifier
from ./machamp.models.tag_decoder import MachampTagger
from ./machamp.models.dependency_decoder import MachampBiaffineDependencyParser
from ./machamp.models.mlm_decoder import MachampMaskedLanguageModel
from ./machamp.models.multiseq_decoder import MachampMultiTagger
from ./machamp.models.crf_decoder import MachampCrfTagger
from ./machamp.models.seq2seq_decoder import MachampSeq2SeqDecoder

import_plugins()
path = '/data/almanach/user/smontari/scratch/machamp/'
machamp_path = path + '/logs/xlmr-WITHsmooth_sampling'
topic_langs = ['immigrants_en', 'women_en']

def machamp_to_transformers(model_path):
    print(model_path)
    dir_path = glob.glob(model_path+'*/')[-1]
    # Load the archive from the serialization directory, which contains the trained model and the params.
    archive = load_archive(dir_path + "model.tar.gz")
    # Pull out just the PretrainedTransformerEmbedder part of the model.
    transformer_embedder: PretrainedTransformerEmbedder = archive.model._text_field_embedder.token_embedder_tokens._matched_embedder
    tokenizer: PretrainedTransformerTokenizer = Tokenizer.from_params(Params(archive.config.as_dict()["dataset_reader"]["tokenizer"]))
    transformer_dir = model_path.replace("logs", "eval_script/transformers_logs")
    transformer_embedder.transformer_model.save_pretrained(transformer_dir)
    tokenizer.tokenizer.save_pretrained(transformer_dir)
    #model = AutoModelForSequenceClassification.from_pretrained(transformer_dir)
    return transformer_dir

def hatecheck_eval(task, topic_lang):
    task_path = os.path.join(model_path, task)
    model_path = os.path.join(task_path, topic_lang, '1', 'model')
    modelname = task + '-' + topic_lang
    transformer_dir = machamp_to_transformers(model_path)
    models[modelname] = BertForSequenceClassification.from_pretrained(transformer_dir)

    # Instantiate trainer objects for each model (already fine-tuned so no longer necessary to specify training and eval data)
    # output directory is redundant because there is no further training but needs to be specified anyway
    trainer[modelname] = Trainer(
        model=models[modelname],         
        args=TrainingArguments(
            output_dir=transformer_dir + "/Test",
            per_device_eval_batch_size = 64)
        )
    results[modelname] = trainer[modelname].predict(hatecheck_dataset)

    preds=[]
    for row in results[model][0]:
        preds.append(int(np.argmax(row)))
    
    label_pred[modelname] = pd.Series(preds)
    label_pred[modelname].replace({1: 'hateful', 0: 'non-hateful'}, inplace=True)
    label_pred[modelname].name = 'pred_BERT_{}'.format(modelname)


modelzero_path = machamp_path + "hate-only/immigrants_en/1/model"
transformer_dir = machamp_to_transformers(modelzero_path)
# load test suite
hatecheck_df = pd.read_csv('hatecheck-experiments/Data/Test Suite/hatecheck_final_ACL.csv', index_col=0)
# separate test suite into text and label series series
hatecheck_texts = hatecheck_df.test_case.astype("string").tolist()
hatecheck_labels = hatecheck_df.label_gold.replace({'hateful': 1, 'non-hateful': 0}).tolist()

# import tokenizer (which includes custom special tokens for URLs, mentions and emojis and is the same across training datasets)
tokenizer = BertTokenizerFast.from_pretrained(transformer_dir)
# tokenize text series
hatecheck_encodings = tokenizer(hatecheck_texts, truncation=True, padding=True)

# write test suite as PyTorch dataset
class HateDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)

hatecheck_dataset = HateDataset(hatecheck_encodings, hatecheck_labels)

models = {}
trainer = {}
results = {}
label_pred={}

tasks = [t for t in os.listdir(machamp_path) if "hate" in t]
for task in tasks:
    for topic_lang in topic_langs:
        hatecheck_eval(task, topic_lang)


export_df = hatecheck_df['case_id']
for model in trainer:
    export_df = pd.concat([export_df, label_pred[model]], axis= 1)
    
export_df.to_pickle('hatecheck-experiments/Data/Test Suite/results.pkl')