
Code changes:

In machamp/utils:
Remove the date ("now") from the save-directory serialization_dir, replace it by "model" dir.
To deal with several different seeds:
Create output_path arg in train.py, update utils.py. Path is: ${CODEPATH}logs/${MODEL}_hate/${topic}_${lang}/$seed/model/

For prediction with each seed and each language: change predict.py

In util:
When finetuning, save the pytorch model to iutput path instead of finetune path (in my scratch instead of arij's scratch)


For weighted binary classification:
-See machamp files modified to support class weights for the sentence classification task. 
-Define class_weights for your task (say, MYTASK-LABELS ) in your dataset config file:

"tasks": {
    "MYTASK-LABELS": {
        "task_type": "classification",
        // ...
        "class_weights": "balanced"
        "metric": "macro-f1" 
    }
}

Possible values for class_weights are:

"balanced": a string; the class weights are calculated automatically by machamp based on training distribution, so for each label you will have a weight = num_samples / num_labels*label_count, that is the standard way in literature to set class weights

{"class1": weight1, ..., "classN": weightN}: a dictionary str->float to set your custom class weights "offline". I didn't thoroughly tested it yet, but it should work if you define all the classes and not a subset of them
if you don't specify class_weights in your config, they won't simply be used or computed


- If you plan to use macro-f1 score as metric, you will have an additional thing to do, i.e., add this line at the end of merge_configs() function in machamp/util.py file, just before the return statement

# TODO: This is hardcoded to avoid issues in the macro f1 metric!
params_config['vocabulary'] = {'non_padded_namespaces': ["dataset_embeds", "MYTASK-LABELS"]}

where MYTASK-LABELS is your task name defined as indicated in your config file (as above) 



RESULTS:
mbert_hate_10epochs_1seed
(mbert with the 6 hate datasets separately)

Jobs:
oarsub -p "gpu='YES'" -l /nodes=1/gpunum=1,walltime=20:00:00 ./scripts-oar/script_hate_only.sh 

oarsub -p "gpu='YES'" -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-tasks.txt ./scripts-oar/param_script_all_multilingual_models_hate_JOINT.sh 

oarsub -p "gpu='YES'" -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-tasks.txt ./scripts-oar/param_script_all_trained_models+hate.sh

oarsub -p "gpu='YES' and gpucapability>='6.1' and host='nefgpu50.inria.fr' "  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-tasks.txt -t besteffort ./scripts-oar/param_script_all_trained_models_ssampling+hate.sh 

oarsub -p "gpu='YES' and gpucapability>='6.1' and host='nefgpu48.inria.fr' "  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-tasks.txt -t besteffort ./scripts-oar/param_script_all_multilingual_models_ssampling_hate_JOINT.sh 

oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-tasks+model.txt -t besteffort ./scripts-oar/param2_script_all_multilingual_models_hate_JOINT.sh 

oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-tasks+model.txt -t besteffort ./scripts-oar/param2_script_all_multilingual_models_ssampling_hate_JOINT.sh 

oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-tasks+model.txt -t besteffort ./scripts-oar/param2_script_all_trained_models_ssampling+hate.sh 

oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-tasks+model.txt ./scripts-oar/param2_script_all_trained_models+hate.sh 



oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-tasks+model+topic.txt -t besteffort ./scripts-oar/param3_script_all_multilingual_models_ssampling_hate_JOINT.sh 

oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-tasks+model+topic.txt -t besteffort ./scripts-oar/param3_script_all_trained_models_ssampling+hate.sh 

oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-model+topic.txt -t besteffort ./scripts-oar/param2_script_hate_only_smoothsampling.sh 

oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-tasks+topic.txt -t besteffort ./scripts-oar/param2_script_charbert_trained_model_ssampling+hate.sh 

oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-lang-topic.txt -t besteffort ./scripts-oar/param2_script_charbert_hate_only_smoothsampling.sh

***
oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-model-lang-topic.txt -t besteffort ./scripts-oar/param3_script_hate_only_smoothsampling.sh

oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-tasks+topic.txt -t besteffort ./scripts-oar/param3_script_all_multilingual_models_ssampling_hate_JOINT.sh

oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-tasks+topic.txt -t besteffort ./scripts-oar/param3_script_all_trained_models_ssampling+hate.sh 



*************
Experiments en sentiment analysis:

oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-lang.txt -t besteffort ./scripts-oar/script_ssampling_UD_finetune_sentiment.sh
OAR_JOB_ID=12040067
OAR_JOB_ID=12040068
OAR_JOB_ID=12040069
OAR_ARRAY_ID=12040067
(re-run for evaluation)

oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-lang.txt -t besteffort ./scripts-oar/

oarsub -p "gpu='YES'"  -l /nodes=1/gpunum=1,walltime=20:00:00 --array-param-file scripts-oar/array-file-param-lang.txt -t besteffort ./scripts-oar/script_sentiment-UD-joint_train_smoothsampling.sh
OAR_JOB_ID=12039811
OAR_JOB_ID=12039812
OAR_JOB_ID=12039813
OAR_ARRAY_ID=12039811
(re-run for evaluation)


