#!/bin/bash

#SBATCH --job-name=mbert-hate    # create a short name for your job
#SBATCH --nodes=1                # node count
#SBATCH --ntasks=1               # total number of tasks across all nodes
#SBATCH --cpus-per-task=10       # cpu-cores per task (>1 if multi-threaded tasks)
#SBATCH --partition=gpu          # Name of the partition
#SBATCH --gres=gpu:rtx6000:1     # GPU nodes are only available in gpu partition
#SBATCH --mem=16G                # Total memory allocated
#SBATCH --hint=multithread       # we get physical cores not logical
#SBATCH --time=48:00:00          # total run time limit (HH:MM:SS)
#SBATCH --output=mbert-hate%j.out   # output file name
#SBATCH --error=mbert-hate%j.err    # error file name

echo "### Running $SLURM_JOB_NAME ###"

#set -x
#cd ${SLURM_SUBMIT_DIR}

# Set your conda environment
#source /home/$USER/.bashrc
source activate machtest


MODEL=mbert
DATAPATH="/home/smontari/scratch/data/hate_speech/Data/comparable_clean_training_data/"
CODEPATH="/home/smontari/scratch/code/machamp/"
MODELCONFIG=${CODEPATH}configs/params.mbert.json

for topic in immigrants women
do 
    for lang in en es it
    do 
        config_hate="config_hate_classif_${topic}_$lang.json"
        echo $config_hate
        TESTDATA="$DATAPATH/$topic/$lang/test.txt"

        EXP=${MODEL}_hate_${topic}_${lang}
        # todo use the --seed argument and loop over a list of 5 seeds.
        cd $CODEPATH &&
        for seed in 1 2 3 4 5
        do 

            python3 -u ${CODEPATH}train.py --name $EXP --device 0 --parameters_config $MODELCONFIG \
                --dataset_config ${CODEPATH}configs/configs_hatespeech_classif/$config_hate \
                --seed $seed\
                --output_path ${CODEPATH}logs/${MODEL}_hate/${topic}_${lang}/$seed/model/
            #train.py --name mbert_hate_women_es --device 0 --parameters_config /home/smontari/scratch/code/machamp/configs/params.mbert.json --dataset_config /home/smontari/scratch/code/machamp/configs/configs_hatespeech_classif/config_hate_classif_women_es.json
            python3 -u ${CODEPATH}predict.py  ${CODEPATH}logs/${MODEL}_hate/${topic}_${lang}/$seed/model/model.tar.gz $TESTDATA ${CODEPATH}logs/${MODEL}_hate/${topic}_${lang}/$seed/test.out --device 0
            
            #predict.py /home/smontari/scratch/code/machamp/logs/mbert_hate_women_es/model/model.tar.gz /home/smontari/scratch/data/hate_speech/Data/comparable_clean_training_data/women/es/test.txt /home/smontari/scratch/code/machamp/logs/mbert_hate_women_es/test.out --device 0
        done
    done
done
