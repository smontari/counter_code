#!/bin/bash
#SBATCH --time=24:00:00    # Each task takes max 30 minutes
#SBATCH --array=1-36%36         # total of 36 jobs , run all concurrently

param_store=/gpfswork/rech/lco/url46ht/machamp/slurm-scripts/full_array-file-param-tasks+model+topic.txt     
### args.txt contains 36 lines with 3 arguments per line.
###    Line <i> contains arguments for run <i>
# Get first argument
model=$(cat $param_store | awk -v var=$SLURM_ARRAY_TASK_ID 'NR==var {print $1}')
task=$(cat $param_store | awk -v var=$SLURM_ARRAY_TASK_ID 'NR==var {print $2}')
topic=$(cat $param_store | awk -v var=$SLURM_ARRAY_TASK_ID 'NR==var {print $3}')

srun ./param3_script_all_multilingual_models_ssampling_hate_JOINT.sh  $model $task $topic
