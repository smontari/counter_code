#!/bin/bash
#SBATCH --job-name=mbert-hate    # create a short name for your job
#SBATCH --nodes=1                # node count
#SBATCH --ntasks=1               # total number of tasks across all nodes
#SBATCH --cpus-per-task=10       # cpu-cores per task (>1 if multi-threaded tasks)
#SBATCH --partition=gpu          # Name of the partition
#SBATCH --gres=gpu:rtx6000:1     # GPU nodes are only available in gpu partition
#SBATCH --mem=16G                # Total memory allocated
#SBATCH --hint=multithread       # we get physical cores not logical
#SBATCH --time=48:00:00          # total run time limit (HH:MM:SS)
#SBATCH --output=mbert-hate%j.out   # output file name
#SBATCH --error=mbert-hate%j.err    # error file name

echo "### Running $SLURM_JOB_NAME ###"

set -x
#cd ${SLURM_SUBMIT_DIR}

# Set your conda environmentmodule purge
conda activate machamp

MODEL=$1
TASK=$2
echo $MODEL
echo $TASK

export DATAPATH="/gpfswork/rech/lco/url46ht/machamp/data/hate/comparable_clean_training_data/"
export CODEPATH="/gpfswork/rech/lco/url46ht/machamp/"

# for TASK in sentiment_analysis UD UD_twitter sentiment_analysis+UD sentiment_analysis+UD_twitter sentiment_analysis+UD+UD_twitter

for MODEL_LANG in en+es+it
do
    #for MODEL in mbert xlmr xlmt 
    CONFIG=configs/$TASK/$MODEL_LANG.json
    CONFICPARAMS=configs/models/params.$MODEL.json
    MODELCONFIG=configs/models/params.$MODEL.json

    for topic in immigrants women
    do 
        for lang in en es it en+es en+it it+es
        do 
            config_hate=configs/hatespeech_classif/${topic}_$lang.json

            EXP=${TASK}_${MODEL}_hate-joint_${topic}_${lang}
            # use the --seed argument and loop over a list of 5 seeds.
            for seed in 1 2 3 4 5
            do
                # check if the 20 epochs have already been done
                if  [ ! -f "${CODEPATH}logs/${MODEL}/${TASK}-hate-joint/${topic}_${lang}/$seed/model/metrics_epoch_19.json" ]
                then
                    python3 -u ${CODEPATH}train.py --name $EXP --device 0 --parameters_config $MODELCONFIG \
                        --dataset_configs $config_hate $CONFIG \
                        --seed $seed\
                        --output_path ${CODEPATH}logs/${MODEL}/${TASK}-hate-joint/${topic}_${lang}/$seed/model/                       
                fi
                # After training, evaluate on each language
                for testlang in en es it
                do 
                    TESTDATA="$DATAPATH/$topic/$testlang/test.txt"
                    if  [ ! -f "${CODEPATH}logs/${MODEL}/${TASK}-hate-joint/${topic}_${lang}/$seed/test_${testlang}.out.eval" ]
                    then
                        # archive, input_file, pred_file
                        python3 -u ${CODEPATH}predict.py  \
                            ${CODEPATH}logs/${MODEL}/${TASK}-hate-joint/${topic}_${lang}/$seed/model/model.tar.gz \
                            $TESTDATA \
                            ${CODEPATH}logs/${MODEL}/${TASK}-hate-joint/${topic}_${lang}/$seed/test_${testlang}.out \
                            --device 0 --dataset HATE_${lang:0:2}
                    fi
                done
            
            done
        done
    done
done
