#!/bin/bash

#SBATCH --job-name=mbert-hate    # create a short name for your job
#SBATCH --nodes=1                # node count
#SBATCH --ntasks=1               # total number of tasks across all nodes
#SBATCH --cpus-per-task=10       # cpu-cores per task (>1 if multi-threaded tasks)
#SBATCH --partition=gpu          # Name of the partition
#SBATCH --gres=gpu:rtx6000:1     # GPU nodes are only available in gpu partition
#SBATCH --mem=16G                # Total memory allocated
#SBATCH --hint=multithread       # we get physical cores not logical
#SBATCH --time=48:00:00          # total run time limit (HH:MM:SS)
#SBATCH --output=mbert-hate%j.out   # output file name
#SBATCH --error=mbert-hate%j.err    # error file name

echo "### Running $SLURM_JOB_NAME ###"

#set -x
#cd ${SLURM_SUBMIT_DIR}

# Set your conda environment
#source /home/$USER/.bashrc
source activate machtest

DATAPATH="/home/smontari/scratch/data/hate_speech/Data/comparable_clean_training_data/"
CODEPATH="/home/smontari/scratch/code/machamp/"

for model in mbert mbert-es_gsd+ewt+it_isdr
    do
    for topic in immigrants women
    do 
        for lang in en es it
        do 
            TESTDATA="$DATAPATH/$topic/$lang/test.txt"

            # todo use the --seed argument and loop over a list of 5 seeds.
            cd $CODEPATH &&
            for seed in 1 2 3 4 5
            do 

            echo python3 -u ${CODEPATH}predict.py  \
                ${CODEPATH}logs/${model}_hate/${topic}_${lang}/$seed/model/model.tar.gz \
                $TESTDATA \
                ${CODEPATH}logs/${model}_hate/${topic}_${lang}/$seed/test_${lang}.out \
                --device 0
                # archive, input_file, pred_file
                
                python3 -u ${CODEPATH}predict.py  \
                    ${CODEPATH}logs/${model}_hate/${topic}_${lang}/$seed/model/model.tar.gz \
                    $TESTDATA \
                    ${CODEPATH}logs/${model}_hate/${topic}_${lang}/$seed/test_${lang}.out \
                    --device 0            
            done
        done
    done             
done