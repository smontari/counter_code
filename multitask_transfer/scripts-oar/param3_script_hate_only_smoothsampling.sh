#!/bin/bash
#
#
# Comments starting with #OAR are used by the resource manager if using "oarsub -S"
#
# Note : quoting style of parameters matters, follow the example
#
# The job is submitted to the default queue
#OAR -q default
# Path to the binary to run

module purge
module load conda
source activate allennlp
module load cuda/10.2
module load gcc/7.3.0
module load cudnn/7.6-cuda-10.2

MODEL=$1
lang=$2
topic=$3
echo $MODEL
echo $topic
echo $lang

export DATAPATH="/data/almanach/user/smontari/scratch/machamp/data/hate/comparable_clean_training_data/"
export CODEPATH="/data/almanach/user/smontari/scratch/machamp/"

#for MODEL in mbert xlmr xlmt
MODELCONFIG=configs/models-smooth_sampling/params.$MODEL.json
MODELEXP=$MODEL-WITHsmooth_sampling/hate/$MODEL-hate-$MODEL_LANG
MODELOUTPUT=${CODEPATH}logs/${MODEL}-WITHsmooth_sampling/hate-only/
#for topic in immigrants women
#for lang in en es it en+es en+it it+es
config_hate="${topic}_$lang.json"
echo $config_hate

EXP=${MODEL}_hate_${topic}_${lang}
# use the --seed argument and loop over a list of 5 seeds.
for seed in 1 2 3 4 5
do
    # check if the 20 epochs have already been done
    if  [ ! -f "$MODELOUTPUT/${topic}_${lang}/$seed/model/model.tar.gz" ]
    then
        echo python3 -u ${CODEPATH}train.py --name $EXP --device 0 --parameters_config $MODELCONFIG  --dataset_config ${CODEPATH}configs/hatespeech_classif/$config_hate --seed $seed --output_path $MODELOUTPUT/${topic}_${lang}/$seed/model/
        python3 -u ${CODEPATH}train.py --name $EXP --device 0 --parameters_config $MODELCONFIG \
            --dataset_config ${CODEPATH}configs/hatespeech_classif/$config_hate \
            --seed $seed\
            --output_path $MODELOUTPUT/${topic}_${lang}/$seed/model/
    fi
    # After training, evaluate on each language
    for testlang in en es it
    do
        TESTDATA="$DATAPATH/$topic/$testlang/test.txt"
        if  [ ! -f "$MODELOUTPUT/${topic}_${lang}/$seed/test_${testlang}.out.eval" ]
        then
            echo python3 -u ${CODEPATH}predict.py  $MODELOUTPUT/${topic}_${lang}/$seed/model/model.tar.gz $TESTDATA $MODELOUTPUT/${topic}_${lang}/$seed/test_${testlang}.out --device 0 --dataset HATE_${lang:0:2}
            # archive, input_file, pred_file
            python3 -u ${CODEPATH}predict.py  \
                $MODELOUTPUT/${topic}_${lang}/$seed/model/model.tar.gz \
                $TESTDATA \
                $MODELOUTPUT/${topic}_${lang}/$seed/test_${testlang}.out \
                --device 0 --dataset HATE_${lang:0:2}
        fi
    done

done

#oarsub -p "gpu='YES' and gpucapability>='6.1' and host='nefgpu51.inria.fr' "  -l /nodes=1/gpunum=1,walltime=20:00:00 -t besteffort ./scripts-oar/script_all_multilingual_models2.sh


#python -m IPython --pdb -c "%run /data/almanach/user/smontari/scratch/machamp/train.py --name sentiment_analysis_mbert_hate_immigrants_en --device 0 --parameters_config configs/models/params.mbert.json --dataset_config /data/almanach/user/smontari/scratch/machamp/configs/hatespeech_classif/immigrants_en.json --seed 1 --output_path /data/almanach/user/smontari/scratch/machamp/logs/mbert/sentiment_analysis+hate/immigrants_en/1/model/ --finetune /data/almanach/user/ariabi/scratch/machamp/logs/mbert/sentiment_analysis/mbert-sentiment_analysis-en+es+it/model"

#python -m IPython --pdb -c "%run /data/almanach/user/smontari/scratch/machamp/predict.py /data/almanach/user/smontari/scratch/machamp/logs/mbert/sentiment_analysis+hate/immigrants_en/1/model/model.tar.gz /data/almanach/user/smontari/scratch/machamp/data/hate/comparable_clean_training_data//immigrants/en/test.txt /data/almanach/user/smontari/scratch/machamp/logs/mbert/sentiment_analysis+hate/immigrants_en/1/test_en.out --device 0"

