#!/bin/bash
#
#
# Comments starting with #OAR are used by the resource manager if using "oarsub -S"
#
# Note : quoting style of parameters matters, follow the example
#
# The job is submitted to the default queue
#OAR -q default
# Path to the binary to run

module purge
module load conda
source activate machtest
module load cuda/10.2
module load gcc/7.3.0
module load cudnn/7.6-cuda-10.2

CONFIG=ewt
CONFICPARAMS=configs/params.bert-base.json
MODEL=bert-base-uncased-ewt
TESTDATA=data/UD_English-EWT/en_ewt-ud-test.conllu
EXP=bert-base-uncased-ewt

python3 -u train.py --dataset_config configs/$CONFIG.json --device 0 --name $MODEL --parameters_config $CONFICPARAMS

python3 -u predict.py logs/$MODEL/model/model.tar.gz $TESTDATA logs/$MODEL/${EXP}_test.out --device 0

python3  $EVAL_CONLL -v $TESTDATA logs/$MODEL/${EXP}.test.out

#oarsub -p "gpu='YES' and gpucapability>='6.1' and host='nefgpu38.inria.fr' "  -l /nodes=1/gpunum=2,walltime=10:00:00 -t besteffort ./script.sh
#oarsub -p "gpu='YES' and gpucapability>='6.1' "  -l /nodes=1/gpunum=1,walltime=10:00:00 -t besteffort ./script.sh