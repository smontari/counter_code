#!/bin/bash
#
#
# Comments starting with #OAR are used by the resource manager if using "oarsub -S"
#
# Note : quoting style of parameters matters, follow the example
#
# The job is submitted to the default queue
#OAR -q default
# Path to the binary to run

module purge
module load conda
source activate allennlp
module load cuda/10.2
module load gcc/7.3.0
module load cudnn/7.6-cuda-10.2

MODEL=xlmr
MODEL_LANG=en+es+it
export DATAPATH="/data/almanach/user/smontari/scratch/machamp/data/sentiment_twitter/"
export CODEPATH="/data/almanach/user/smontari/scratch/machamp/"
export TRAINEDMODELSPATH="/data/almanach/user/ariabi/scratch/machamp/"

# for TASK in sentiment_analysis UD UD_twitter sentiment_analysis+UD sentiment_analysis+UD_twitter sentiment_analysis+UD+UD_twitter

#for MODEL in mbert xlmr xlmt 
lang=$1
TASK=UD
CONFIG=configs/$TASK/$MODEL_LANG.json
MODELCONFIG=configs/models-smooth_sampling/params.$MODEL.json
MODELOUTPUT=${CODEPATH}logs/${MODEL}-WITHsmooth_sampling/${TASK}_sentiment_joint/
#for lang in en es it
config_sent=configs/sentiment_analysis/$lang.json

EXP=${TASK}_sentiment_joint_${MODEL}_${lang}
# use the --seed argument and loop over a list of 5 seeds.
for seed in 1 2 3 4 5
do
    # check if the 20 epochs have already been done
    if  [ ! -f "$MODELOUTPUT/sentiment_${lang}/$seed/model/model.tar.gz" ]
    then
        echo python3 -u ${CODEPATH}train.py --name $EXP --device 0 --parameters_config $MODELCONFIG  --dataset_configs $CONFIG $config_sent --seed $seed --output_path $MODELOUTPUT/sentiment_${lang}/${seed}/model
        python3 -u ${CODEPATH}train.py --name $EXP --device 0 --parameters_config $MODELCONFIG \
            --dataset_configs $CONFIG $config_sent \
            --seed $seed\
            --output_path $MODELOUTPUT/sentiment_${lang}/${seed}/model
    fi
    # After training, evaluate on each language
    for testlang in en es it
    do 
        TESTDATA="$DATAPATH/$testlang/test.txt"
        if  [ ! -f "$MODELOUTPUT/sentiment_${lang}/$seed/test_${testlang}.out.eval" ]
        then
            echo python3 -u ${CODEPATH}predict.py  $MODELOUTPUT/sentiment_${lang}/$seed/model/model.tar.gz $TESTDATA $MODELOUTPUT/sentiment_${lang}/$seed/test_${testlang}.out --device 0 --dataset sentimentAnalysis
            # archive, input_file, pred_file
            python3 -u ${CODEPATH}predict.py  \
                $MODELOUTPUT/sentiment_${lang}/$seed/model/model.tar.gz \
                $TESTDATA \
                $MODELOUTPUT/sentiment_${lang}/$seed/test_${testlang}.out \
                --device 0 --dataset sentimentAnalysis
        fi
    done
done
