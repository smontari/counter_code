#!/bin/bash
#
#
# Comments starting with #OAR are used by the resource manager if using "oarsub -S"
#
# Note : quoting style of parameters matters, follow the example
#
# The job is submitted to the default queue
#OAR -q default
# Path to the binary to run

module purge
module load conda
source activate allennlp
module load cuda/10.2
module load gcc/7.3.0
module load cudnn/7.6-cuda-10.2

MODEL=xlmr
export DATAPATH="/data/almanach/user/smontari/scratch/machamp/data/sentiment_twitter/"
export CODEPATH="/data/almanach/user/smontari/scratch/machamp/"
export TRAINEDMODELSPATH="/data/almanach/user/ariabi/scratch/machamp/"

# for TASK in sentiment_analysis UD UD_twitter sentiment_analysis+UD sentiment_analysis+UD_twitter sentiment_analysis+UD+UD_twitter

#for MODEL in mbert xlmr xlmt 
lang=$1
TASK=sentiment_analysis
MODELCONFIG=configs/models-smooth_sampling/params.$MODEL.json
MODELOUTPUT=${CODEPATH}logs/${MODEL}-WITHsmooth_sampling/${TASK}-only/
#for lang in en es it
config_sent=configs/sentiment_analysis/$lang.json

EXP=${TASK}_${MODEL}_${lang}
# use the --seed argument and loop over a list of 5 seeds.
for seed in 1 2 3 4 5
do
    # check if the 20 epochs have already been done
    if  [ ! -f "$MODELOUTPUT/sentiment_${lang}/$seed/model/model.tar.gz" ]
    then
        echo python3 -u ${CODEPATH}train.py --name $EXP --device 0 --parameters_config $MODELCONFIG  --dataset_config $config_sent --seed $seed --output_path $MODELOUTPUT/sentiment_${lang}/${seed}/model
        python3 -u ${CODEPATH}train.py --name $EXP --device 0 --parameters_config $MODELCONFIG \
            --dataset_config $config_sent \
            --seed $seed\
            --output_path $MODELOUTPUT/sentiment_${lang}/${seed}/model
    fi
    # After training, evaluate on each language
    for testlang in en es it
    do 
        TESTDATA="$DATAPATH/$testlang/test.txt"
        if  [ ! -f "$MODELOUTPUT/sentiment_${lang}/$seed/test_${testlang}.out.eval" ]
        then
            echo python3 -u ${CODEPATH}predict.py  $MODELOUTPUT/sentiment_${lang}/$seed/model/model.tar.gz $TESTDATA $MODELOUTPUT/sentiment_${lang}/$seed/test_${testlang}.out --device 0
            # archive, input_file, pred_file
            python3 -u ${CODEPATH}predict.py  \
                $MODELOUTPUT/sentiment_${lang}/$seed/model/model.tar.gz \
                $TESTDATA \
                $MODELOUTPUT/sentiment_${lang}/$seed/test_${testlang}.out \
                --device 0
        fi
    done
done
