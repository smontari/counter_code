#!/bin/bash
#
#
# Comments starting with #OAR are used by the resource manager if using "oarsub -S"
#
# Note : quoting style of parameters matters, follow the example
#
# The job is submitted to the default queue
#OAR -q default
# Path to the binary to run

module purge
module load conda
source activate machtest
module load cuda/10.2
module load gcc/7.3.0
module load cudnn/7.6-cuda-10.2

# for TASK in sentiment_analysis UD UD_twitter sentiment_analysis+UD sentiment_analysis+UD_twitter sentiment_analysis+UD+UD_twitter
for TASK in sentiment_analysis+UD sentiment_analysis+UD_twitter sentiment_analysis+UD+UD_twitter
do
    for LANG in en es it
    do
        for MODEL in mbert xlmr xlmt 
        do
            echo " experiment $MODEL $TASK $LANG"
            CONFIG=configs/$TASK/$LANG.json
            CONFICPARAMS=configs/models/params.$MODEL.json
            MODELEXP=$MODEL/$TASK/$MODEL-$TASK-$LANG
            python3 -u train.py --dataset_config $CONFIG --device 0 --name $MODELEXP --parameters_config $CONFICPARAMS
        done
    done
done

#oarsub -p "gpu='YES' and gpucapability>='6.1' and host='nefgpu48.inria.fr' "  -l /nodes=1/gpunum=1,walltime=20:00:00 -t besteffort ./scripts-oar/script_all_multilingual_models.sh
